<?php
include_once 'common/common.php';
if($_POST['page'] == "requestReset"){
  $userName = isset($_POST['userName']);
  $table = "tbl_reset_password";
  $field = array("userName","resetPasswordStatus","resetPasswordDateTime");
  $data = array($userName,1,$currentTimeDate);
  $msg = "Request Already Sent!";
  $result = insertAllData($table,$field,$data,$msg);
  $returndata = $result;
}else{
  if($token != "success"){
    $returndata['status'] = "ERROR";
    $returndata['message'] = "Access Denied!";
  }else{
    if($_POST['page'] == "resetDefault"){
      if(isset($_POST['userID']) && isset($_POST['userPassword'])){
          $ID=isset($_POST['userID']);
          $password=sha1($_POST['userPassword']);
          $sql = "UPDATE tbl_users SET userPassword='".$password."' WHERE userID=$ID";
          if ($conn->query($sql) === TRUE) {
              $returndata['status'] = "SUCCESS";
              $returndata['message'] = "Password Successfully Changed";
          } else {
              $returndata['status'] = "ERROR";
          }
      }else{
          $returndata['status'] = "ERROR";
      }
    }
    if($_POST['page'] == "requestList"){
      $sql = "SELECT * FROM tbl_reset_password";
      if ($result=mysqli_query($conn,$sql)){
        while ($row=mysqli_fetch_array($result))
        {
          $list[]= array(
            "resetPasswordID" => $row['resetPasswordID'],
            "userName" => $row['userName'],
            "resetPasswordStatus" => $row['resetPasswordStatus'],
            "resetPasswordDateTime" => $row['resetPasswordDateTime'],
            "updatedBy" => $row['updatedBy'],
            "updatedTime" => $row['updatedTime']
          );
        }
        $returndata['requestList'] = $list;
        $returndata['status'] = "SUCCESS";
      }else{
        $returndata['status'] = "ERROR";
        $returndata['message'] = "ERROR: 500 Status Codes";
      }
    }

    if($_POST['page'] == "resetPassword"){
      $resetPasswordStatus = isset($_POST['resetPasswordStatus']);

      $table1 = "tbl_reset_password";
      $id1 = isset($_POST['resetPasswordID']);
      $idName1 = "resetPasswordID";
      $field1 = array("resetPasswordStatus","updatedBy","updatedTime");
      $data1 = array($resetPasswordStatus,$userID,$currentTimeDate);
      $msg = "Password already reset";
      $result1 = updateData($table1,$field1,$data1,$idName1,$id1,$msg);

      if($resetPasswordStatus == 2){
        $password=sha1('12345');
        $table2 = "tbl_users";
        $id2 = $_POST['userName'];
        $idName2 = "userName";
        $field2 = array("userPassword");
        $data2 = array($password);
        $result2 = updateData($table2,$field2,$data2,$idName2,$id2,$msg);
      }
      $returndata = $result1;
    }
  }
}

/********Compose Your Json Data Here*************/
createJsonData('userInfoUpdate', $returndata);
mysqli_close($conn);
