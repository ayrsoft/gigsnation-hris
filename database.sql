-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 18, 2018 at 03:53 PM
-- Server version: 5.6.38
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `db_gigsnation_hris`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_attendance`
--

CREATE TABLE `tbl_attendance` (
  `attendanceID` int(100) NOT NULL,
  `userID` int(100) NOT NULL,
  `attendanceDate` date NOT NULL,
  `attendanceDay` enum('1','2','3','4','5') NOT NULL COMMENT '[Monday = 1, Tuesday = 2, Wednesday = 3, Thursday = 4, Friday = 5]',
  `timeIn` time(6) NOT NULL,
  `timeOut` time(6) NOT NULL,
  `overtime` text NOT NULL,
  `overtimeStatus` enum('1','2') NOT NULL COMMENT '[Approved = 1, Declined = 2]',
  `holidayID` int(11) NOT NULL COMMENT '[tbl_holidays]',
  `attendanceHoliday` enum('1','2') NOT NULL COMMENT '[Regular = 1, Special = 1]'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department`
--

CREATE TABLE `tbl_department` (
  `departmentID` int(100) NOT NULL,
  `departmentName` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_salary`
--

CREATE TABLE `tbl_employee_salary` (
  `employeeSalaryID` int(100) NOT NULL,
  `userID` int(100) NOT NULL,
  `employeeMonthlySalary` int(100) NOT NULL COMMENT '[if empty get the salary from the positionSalaryID]',
  `positionSalaryID` int(100) NOT NULL COMMENT '[tbl_position_salary]',
  `salaryStatus` enum('1','2') NOT NULL,
  `updatedBy` text NOT NULL,
  `updatedAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employment_type`
--

CREATE TABLE `tbl_employment_type` (
  `employmentTypeID` int(100) NOT NULL,
  `employmentTypeName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_holidays`
--

CREATE TABLE `tbl_holidays` (
  `holidayID` int(100) NOT NULL,
  `holidayName` text NOT NULL,
  `holidayDate` date NOT NULL,
  `holidayDay` enum('1','2','3','4','5') NOT NULL COMMENT 'Monday = 1, Tuesday = 2, Wednesday = 3, Thursday = 4, Friday = 5',
  `holidayType` enum('1','2') NOT NULL COMMENT '[Regular = 1, Special = 2]'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_identification_number`
--

CREATE TABLE `tbl_identification_number` (
  `idNumberID` int(100) NOT NULL,
  `idNumberName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_position`
--

CREATE TABLE `tbl_position` (
  `positionID` int(100) NOT NULL,
  `positionName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_position_salary`
--

CREATE TABLE `tbl_position_salary` (
  `positionSalaryID` int(100) NOT NULL,
  `positionID` int(100) NOT NULL COMMENT ' [tbl_position]',
  `positionMonthlySalary` int(100) NOT NULL,
  `updatedBy` varchar(100) NOT NULL,
  `updatedAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `userID` int(100) NOT NULL,
  `empID` int(100) NOT NULL,
  `userName` varchar(100) NOT NULL,
  `userPassword` varchar(100) NOT NULL,
  `firstName` text NOT NULL,
  `lastName` text NOT NULL,
  `middleName` text NOT NULL,
  `extName` text NOT NULL,
  `birthday` date NOT NULL,
  `gender` enum('1','2') NOT NULL COMMENT '[male = 1, female = 2]',
  `civilStatus` enum('1','2','3','4','5') NOT NULL COMMENT '[married = 1, single = 2, widowed = 3, separated = 4, divorced = 5]',
  `nationality` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `contactNumber` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`userID`, `empID`, `userName`, `userPassword`, `firstName`, `lastName`, `middleName`, `extName`, `birthday`, `gender`, `civilStatus`, `nationality`, `email`, `contactNumber`) VALUES
(1, 1, 'patricia.otero', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Patricia Ayrah', 'Otero', 'Reyes', '', '1995-04-21', '2', '2', 'Filipino', 'ayrah0421.pao@gmail.com', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users_address`
--

CREATE TABLE `tbl_users_address` (
  `userID` int(100) NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `city` text COLLATE utf8_unicode_ci NOT NULL,
  `provinceState` text COLLATE utf8_unicode_ci NOT NULL,
  `country` text COLLATE utf8_unicode_ci NOT NULL,
  `zipCode` int(100) NOT NULL,
  `addressType` enum('1','2') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users_educational`
--

CREATE TABLE `tbl_users_educational` (
  `userID` int(100) NOT NULL,
  `educationLevel` text NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `majorSpecialization` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users_emergency_contact`
--

CREATE TABLE `tbl_users_emergency_contact` (
  `userID` int(11) NOT NULL,
  `contactName` text NOT NULL,
  `emergencyContactNum` int(11) NOT NULL,
  `relationship` text NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users_employment_profile`
--

CREATE TABLE `tbl_users_employment_profile` (
  `userID` int(100) NOT NULL,
  `departmentID` int(11) NOT NULL,
  `positionID` int(100) NOT NULL COMMENT '[tbl_position]',
  `employmentOriginalDateHired` date NOT NULL,
  `employmentStartDate` date NOT NULL,
  `employmentStatus` enum('1','2') NOT NULL,
  `employmentTypeID` int(100) NOT NULL COMMENT '[tbl_employment_type]',
  `paymentType` enum('1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users_id_number`
--

CREATE TABLE `tbl_users_id_number` (
  `userID` int(100) NOT NULL,
  `idType` int(100) NOT NULL COMMENT 'idNumberID [tbl_identification_number]',
  `idNumber` int(100) NOT NULL,
  `deductionStatus` enum('0','1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  ADD PRIMARY KEY (`attendanceID`);

--
-- Indexes for table `tbl_department`
--
ALTER TABLE `tbl_department`
  ADD PRIMARY KEY (`departmentID`);

--
-- Indexes for table `tbl_employee_salary`
--
ALTER TABLE `tbl_employee_salary`
  ADD PRIMARY KEY (`employeeSalaryID`);

--
-- Indexes for table `tbl_employment_type`
--
ALTER TABLE `tbl_employment_type`
  ADD PRIMARY KEY (`employmentTypeID`);

--
-- Indexes for table `tbl_holidays`
--
ALTER TABLE `tbl_holidays`
  ADD PRIMARY KEY (`holidayID`);

--
-- Indexes for table `tbl_identification_number`
--
ALTER TABLE `tbl_identification_number`
  ADD PRIMARY KEY (`idNumberID`);

--
-- Indexes for table `tbl_position`
--
ALTER TABLE `tbl_position`
  ADD PRIMARY KEY (`positionID`);

--
-- Indexes for table `tbl_position_salary`
--
ALTER TABLE `tbl_position_salary`
  ADD PRIMARY KEY (`positionSalaryID`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `empID` (`empID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  MODIFY `attendanceID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_department`
--
ALTER TABLE `tbl_department`
  MODIFY `departmentID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_employee_salary`
--
ALTER TABLE `tbl_employee_salary`
  MODIFY `employeeSalaryID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_employment_type`
--
ALTER TABLE `tbl_employment_type`
  MODIFY `employmentTypeID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_holidays`
--
ALTER TABLE `tbl_holidays`
  MODIFY `holidayID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_identification_number`
--
ALTER TABLE `tbl_identification_number`
  MODIFY `idNumberID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_position`
--
ALTER TABLE `tbl_position`
  MODIFY `positionID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_position_salary`
--
ALTER TABLE `tbl_position_salary`
  MODIFY `positionSalaryID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `userID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
