-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 13, 2018 at 03:19 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_gigsnation_hris`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_allowance_type`
--

CREATE TABLE `tbl_allowance_type` (
  `allowanceTypeID` int(100) NOT NULL,
  `allowanceName` text NOT NULL,
  `allowanceTaxable` enum('Y','N') NOT NULL COMMENT 'Y = yes, N = no',
  `allowanceStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_attendance`
--

CREATE TABLE `tbl_attendance` (
  `attendanceID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `attendanceDate` date NOT NULL,
  `timeIn` time(6) NOT NULL,
  `timeOut` time(6) NOT NULL,
  `overtime` int(100) NOT NULL,
  `overtimeStatus` int(100) NOT NULL COMMENT 'Approved = 1, Declined = 2',
  `holidayTypeID` int(100) NOT NULL COMMENT 'tbl_holiday_type',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_audit_logs`
--

CREATE TABLE `tbl_audit_logs` (
  `auditLogsID` int(100) NOT NULL,
  `auditLogsDate` datetime(6) NOT NULL,
  `userID` int(100) NOT NULL,
  `auditLogsType` text NOT NULL COMMENT 'Add, Edit',
  `auditLogsDescription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company_settings`
--

CREATE TABLE `tbl_company_settings` (
  `companySettingsID` int(100) NOT NULL,
  `companyName` text NOT NULL,
  `companyContactPerson` text NOT NULL,
  `companyAddress` text NOT NULL,
  `companyCountry` text NOT NULL,
  `companyCity` text NOT NULL,
  `companyState` text NOT NULL,
  `companyPostalCode` int(100) NOT NULL,
  `companyEmail` text NOT NULL,
  `companyPhoneNumber` int(100) NOT NULL,
  `companyMobileNumber` int(100) NOT NULL,
  `companyFaxNumber` text NOT NULL,
  `companyWebsite` text NOT NULL,
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deduction_type`
--

CREATE TABLE `tbl_deduction_type` (
  `deductionTypeID` int(100) NOT NULL,
  `deductionTypeName` text NOT NULL,
  `deductionTypeStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_deduction_type`
--

INSERT INTO `tbl_deduction_type` (`deductionTypeID`, `deductionTypeName`, `deductionTypeStatus`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, 'Human Resources', 'Y', 1, '2018-12-13 06:18:09.000000', 0, '0000-00-00 00:00:00.000000'),
(2, 'Software Development', 'Y', 1, '2018-12-13 04:19:15.000000', 0, '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department_type`
--

CREATE TABLE `tbl_department_type` (
  `departmentTypeID` int(100) NOT NULL,
  `departmentTypeName` text NOT NULL,
  `departmentTypeStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee`
--

CREATE TABLE `tbl_employee` (
  `employeeID` varchar(100) NOT NULL,
  `firstName` text NOT NULL,
  `lastName` text NOT NULL,
  `middleName` text NOT NULL,
  `extName` text NOT NULL,
  `birthday` date NOT NULL,
  `gender` enum('M','F') NOT NULL COMMENT 'M = male, F = female',
  `civilStatus` int(100) NOT NULL COMMENT 'married = 1, single = 2, widowed = 3, separated = 4, divorced = 5',
  `nationality` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `contactNumber` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee`
--

INSERT INTO `tbl_employee` (`employeeID`, `firstName`, `lastName`, `middleName`, `extName`, `birthday`, `gender`, `civilStatus`, `nationality`, `email`, `contactNumber`) VALUES
('001', 'Patricia Ayrah', 'Otero', 'Reyes', '', '1995-04-21', 'F', 2, 'Filipino', 'ayrah0421.pao@gmail.com', '09771003619'),
('002', 'Davecar', 'Grave', 'Mabasa', 'Jr', '1994-07-21', 'M', 2, 'Filipino', 'davecar.grave@gmail.com', '09361600304');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_address`
--

CREATE TABLE `tbl_employee_address` (
  `employeeID` varchar(100) NOT NULL,
  `employeeAddress` text NOT NULL,
  `employeeCity` text NOT NULL,
  `employeeProvince` text NOT NULL,
  `employeeCountry` text NOT NULL,
  `employeeZipcode` int(100) NOT NULL,
  `employeeAddressType` int(100) NOT NULL COMMENT 'present address = 1, residential = 2'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee_address`
--

INSERT INTO `tbl_employee_address` (`employeeID`, `employeeAddress`, `employeeCity`, `employeeProvince`, `employeeCountry`, `employeeZipcode`, `employeeAddressType`) VALUES
('001', '096 Stuazon St. Brgy. Poblacion', 'Pateros', 'Metro Manila', 'Philippines', 1620, 1),
('002', '2 FC Street brgy. San Pedro', 'Pateros', 'Metro Manila', 'Philippines', 1622, 1),
('002', '059 Makisig St. Brgy. 101', 'Sta. Ana Manila', 'Metro Manila', 'Philippines', 1620, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_allowance`
--

CREATE TABLE `tbl_employee_allowance` (
  `employeeAllowanceID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `allowanceTypeID` int(100) NOT NULL COMMENT 'tbl_allowance_type',
  `employeeAllowanceAmount` int(100) NOT NULL,
  `employeeAllowanceStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_basic_adj`
--

CREATE TABLE `tbl_employee_basic_adj` (
  `employeeBasicSalaryAdjID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `employmentBasicSalary` int(100) NOT NULL,
  `employeeBasicSalaryAdjIDate` date NOT NULL,
  `employeeBasicSalaryAdjReason` text NOT NULL,
  `employeeBasicSalaryStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee_basic_adj`
--

INSERT INTO `tbl_employee_basic_adj` (`employeeBasicSalaryAdjID`, `employeeID`, `employmentBasicSalary`, `employeeBasicSalaryAdjIDate`, `employeeBasicSalaryAdjReason`, `employeeBasicSalaryStatus`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, '001', 180000, '2018-12-13', 'Initial Salary', 'Y', 1, '2018-12-13 04:19:15.000000', 0, '0000-00-00 00:00:00.000000'),
(2, '002', 18000, '2018-12-13', 'Initial Salary', 'Y', 1, '2018-12-13 00:00:00.000000', 0, '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_deduction`
--

CREATE TABLE `tbl_employee_deduction` (
  `employeeDeductionID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `deductionTypeID` int(100) NOT NULL COMMENT 'tbl_deduction_type',
  `deductionAmount` int(100) NOT NULL,
  `deductionValidFrom` date NOT NULL,
  `deductionValidTo` date NOT NULL,
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_educational`
--

CREATE TABLE `tbl_employee_educational` (
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `educationalLevelName` text NOT NULL,
  `educationalStartDate` date NOT NULL,
  `educationalEndDate` date NOT NULL,
  `educationalMajorSpecialization` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee_educational`
--

INSERT INTO `tbl_employee_educational` (`employeeID`, `educationalLevelName`, `educationalStartDate`, `educationalEndDate`, `educationalMajorSpecialization`) VALUES
('001', 'College', '2011-04-21', '2016-04-21', 'Bachelor of Science in Information and Communication Technology'),
('002', 'College', '2010-04-21', '2015-04-21', 'Bachelor of Science in Information and Communication Technology');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_emergency_contact`
--

CREATE TABLE `tbl_employee_emergency_contact` (
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `emergencyContactName` text NOT NULL,
  `emergencyContactNum` text NOT NULL,
  `emergencyRelationship` text NOT NULL,
  `emergencyRemarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee_emergency_contact`
--

INSERT INTO `tbl_employee_emergency_contact` (`employeeID`, `emergencyContactName`, `emergencyContactNum`, `emergencyRelationship`, `emergencyRemarks`) VALUES
('001', 'Alma Otero', '', 'Sister', 'This is for testing purposes'),
('002', 'Encarnation Grave', '09171003612', 'Mother', 'This is for testing purposes');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_employment`
--

CREATE TABLE `tbl_employee_employment` (
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `departmentTypeID` int(100) NOT NULL COMMENT 'tbl_department_type',
  `positionTypeID` int(100) NOT NULL COMMENT 'tbl_position_type',
  `employmentOriginalDateHIred` date NOT NULL,
  `employmentStartDate` date NOT NULL,
  `employmentTypeID` int(100) NOT NULL COMMENT 'tbl_employment_type',
  `employmentPaymentGroup` int(100) NOT NULL COMMENT 'weekly = 1, semi = 2, monthly = 3',
  `employmentPaymentType` int(100) NOT NULL COMMENT 'remittance = 1, bank = 2',
  `employmentStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee_employment`
--

INSERT INTO `tbl_employee_employment` (`employeeID`, `departmentTypeID`, `positionTypeID`, `employmentOriginalDateHIred`, `employmentStartDate`, `employmentTypeID`, `employmentPaymentGroup`, `employmentPaymentType`, `employmentStatus`) VALUES
('001', 1, 1, '2018-12-13', '2018-12-17', 1, 1, 1, 'Y'),
('002', 2, 1, '2018-12-13', '2018-12-17', 1, 1, 1, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_gov`
--

CREATE TABLE `tbl_employee_gov` (
  `employeeGovID` int(11) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `governmentType` int(100) NOT NULL COMMENT 'HDMF = 1, TIN = 2, SSS = 3, PHILHEALTH = 4',
  `governmentNumber` varchar(100) NOT NULL,
  `governmentStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee_gov`
--

INSERT INTO `tbl_employee_gov` (`employeeGovID`, `employeeID`, `governmentType`, `governmentNumber`, `governmentStatus`) VALUES
(1, '001', 1, '564548', 'Y'),
(2, '001', 2, '66666', 'Y'),
(3, '001', 3, '33333', 'Y'),
(4, '001', 4, '777777', 'Y'),
(5, '002', 4, '777777', 'Y'),
(6, '002', 1, '8888', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_leave`
--

CREATE TABLE `tbl_employee_leave` (
  `leaveID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `leaveTypeID` int(100) NOT NULL COMMENT 'tbl_leave_type',
  `employeeLeaveApplyDate` date NOT NULL,
  `employeeLeaveStartDate` date NOT NULL,
  `employeeLeaveEndDate` date NOT NULL,
  `employeeLeaveRemarks` text NOT NULL,
  `employeeLeaveStatus` int(100) NOT NULL COMMENT 'Approved = 1, Declined = 2',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_loan`
--

CREATE TABLE `tbl_employee_loan` (
  `employeeLoanID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `loanTypeID` int(100) NOT NULL COMMENT 'tbl_loan_type',
  `employeePrincipalAmount` int(100) NOT NULL,
  `employeeLoanAmount` int(100) NOT NULL,
  `employeeLoanRemainingBalance` int(100) NOT NULL,
  `employeeLoanTerms` int(100) NOT NULL COMMENT 'months',
  `employeeLoanScheme` int(100) NOT NULL COMMENT 'weekly = 1, semi = 2, monthly = 3',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_payroll_record`
--

CREATE TABLE `tbl_employee_payroll_record` (
  `employeePayrollRecordID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `employeeGrossIncome` int(100) NOT NULL,
  `employeeDaysWorked` int(100) NOT NULL,
  `employeeNetIncome` int(100) NOT NULL,
  `employeeDaysDeduction` int(100) NOT NULL COMMENT 'Absent',
  `employeeLateDeduction` int(100) NOT NULL,
  `employeeOvertimeTotalAmount` int(100) NOT NULL,
  `employeeHolidayTotalAmount` int(100) NOT NULL,
  `employeeTaxTotalAmount` int(100) NOT NULL,
  `employeeSSSLoan` int(100) NOT NULL,
  `employeePagibigLoan` int(100) NOT NULL,
  `employeeOtherDeductionTotal` int(100) NOT NULL,
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_termination`
--

CREATE TABLE `tbl_employee_termination` (
  `employeeTerminationID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `terminationDate` date NOT NULL,
  `TerminationReason` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employment_type`
--

CREATE TABLE `tbl_employment_type` (
  `employmentTypeID` int(100) NOT NULL,
  `employmentTypeName` text NOT NULL,
  `employmentTypeStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_holiday_type`
--

CREATE TABLE `tbl_holiday_type` (
  `holidayTypeID` int(100) NOT NULL,
  `holidayName` text NOT NULL,
  `holidayDate` date NOT NULL,
  `holidayDay` int(100) NOT NULL COMMENT 'Monday = 1, Tuesday = 2, Wednesday = 3, Thursday = 4, Friday = 5',
  `holidayType` int(100) NOT NULL COMMENT 'Regular = 1, Special = 2',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_leave_type`
--

CREATE TABLE `tbl_leave_type` (
  `leaveTypeID` int(100) NOT NULL,
  `leaveName` text NOT NULL,
  `leaveTypeStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_loan_type`
--

CREATE TABLE `tbl_loan_type` (
  `loanTypeID` int(100) NOT NULL,
  `loanTypeName` text NOT NULL,
  `loanTypeStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_position_type`
--

CREATE TABLE `tbl_position_type` (
  `positionTypeID` int(100) NOT NULL,
  `departmentTypeID` int(100) NOT NULL COMMENT 'tbl_department_type',
  `positionTypeName` text NOT NULL,
  `positionTypeSalary` int(100) NOT NULL,
  `positionTypeStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_position_type`
--

INSERT INTO `tbl_position_type` (`positionTypeID`, `departmentTypeID`, `positionTypeName`, `positionTypeSalary`, `positionTypeStatus`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, 1, 'Human Resources Manager', 100000, 'Y', 1, '2018-12-13 06:21:06.000000', 0, '0000-00-00 00:00:00.000000'),
(2, 1, 'Human Resources Assistant', 10000, 'Y', 1, '2018-12-13 06:21:06.000000', 0, '0000-00-00 00:00:00.000000'),
(3, 2, 'Information Technology', 40000, 'Y', 1, '2018-12-13 06:21:06.000000', 0, '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_salary_settings`
--

CREATE TABLE `tbl_salary_settings` (
  `salarySettingID` int(100) NOT NULL,
  `salarySettingName` text NOT NULL,
  `salarySettingValueRate` int(100) NOT NULL,
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `userID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `userName` varchar(100) NOT NULL,
  `userPassword` varchar(100) NOT NULL,
  `userLevel` int(100) NOT NULL COMMENT 'Admin = 1, HR Manager = 2, HR Assistant = 3',
  `userStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`userID`, `employeeID`, `userName`, `userPassword`, `userLevel`, `userStatus`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, '001', 'patricia.otero', '12345', 1, 'Y', 1, '2018-12-13 08:16:11.000000', 0, '0000-00-00 00:00:00.000000'),
(2, '002', 'davecar.grave', '12345', 1, 'Y', 1, '2018-12-13 08:16:11.000000', 1, '2018-12-13 08:16:11.000000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_allowance_type`
--
ALTER TABLE `tbl_allowance_type`
  ADD PRIMARY KEY (`allowanceTypeID`);

--
-- Indexes for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  ADD PRIMARY KEY (`attendanceID`),
  ADD UNIQUE KEY `employeeID` (`employeeID`);

--
-- Indexes for table `tbl_audit_logs`
--
ALTER TABLE `tbl_audit_logs`
  ADD PRIMARY KEY (`auditLogsID`);

--
-- Indexes for table `tbl_company_settings`
--
ALTER TABLE `tbl_company_settings`
  ADD PRIMARY KEY (`companySettingsID`);

--
-- Indexes for table `tbl_deduction_type`
--
ALTER TABLE `tbl_deduction_type`
  ADD PRIMARY KEY (`deductionTypeID`);

--
-- Indexes for table `tbl_department_type`
--
ALTER TABLE `tbl_department_type`
  ADD PRIMARY KEY (`departmentTypeID`);

--
-- Indexes for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  ADD PRIMARY KEY (`employeeID`);

--
-- Indexes for table `tbl_employee_allowance`
--
ALTER TABLE `tbl_employee_allowance`
  ADD PRIMARY KEY (`employeeAllowanceID`),
  ADD UNIQUE KEY `employeeID` (`employeeID`);

--
-- Indexes for table `tbl_employee_basic_adj`
--
ALTER TABLE `tbl_employee_basic_adj`
  ADD PRIMARY KEY (`employeeBasicSalaryAdjID`),
  ADD UNIQUE KEY `employeeID` (`employeeID`);

--
-- Indexes for table `tbl_employee_deduction`
--
ALTER TABLE `tbl_employee_deduction`
  ADD PRIMARY KEY (`employeeDeductionID`),
  ADD UNIQUE KEY `employeeID` (`employeeID`);

--
-- Indexes for table `tbl_employee_employment`
--
ALTER TABLE `tbl_employee_employment`
  ADD PRIMARY KEY (`employeeID`);

--
-- Indexes for table `tbl_employee_gov`
--
ALTER TABLE `tbl_employee_gov`
  ADD PRIMARY KEY (`employeeGovID`);

--
-- Indexes for table `tbl_employee_leave`
--
ALTER TABLE `tbl_employee_leave`
  ADD PRIMARY KEY (`leaveID`),
  ADD UNIQUE KEY `employeeID` (`employeeID`);

--
-- Indexes for table `tbl_employee_loan`
--
ALTER TABLE `tbl_employee_loan`
  ADD PRIMARY KEY (`employeeLoanID`),
  ADD UNIQUE KEY `employeeID` (`employeeID`);

--
-- Indexes for table `tbl_employee_payroll_record`
--
ALTER TABLE `tbl_employee_payroll_record`
  ADD PRIMARY KEY (`employeePayrollRecordID`),
  ADD UNIQUE KEY `employeeID` (`employeeID`);

--
-- Indexes for table `tbl_employee_termination`
--
ALTER TABLE `tbl_employee_termination`
  ADD PRIMARY KEY (`employeeTerminationID`);

--
-- Indexes for table `tbl_holiday_type`
--
ALTER TABLE `tbl_holiday_type`
  ADD PRIMARY KEY (`holidayTypeID`);

--
-- Indexes for table `tbl_leave_type`
--
ALTER TABLE `tbl_leave_type`
  ADD PRIMARY KEY (`leaveTypeID`);

--
-- Indexes for table `tbl_loan_type`
--
ALTER TABLE `tbl_loan_type`
  ADD PRIMARY KEY (`loanTypeID`);

--
-- Indexes for table `tbl_position_type`
--
ALTER TABLE `tbl_position_type`
  ADD PRIMARY KEY (`positionTypeID`);

--
-- Indexes for table `tbl_salary_settings`
--
ALTER TABLE `tbl_salary_settings`
  ADD PRIMARY KEY (`salarySettingID`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_allowance_type`
--
ALTER TABLE `tbl_allowance_type`
  MODIFY `allowanceTypeID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  MODIFY `attendanceID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_audit_logs`
--
ALTER TABLE `tbl_audit_logs`
  MODIFY `auditLogsID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_company_settings`
--
ALTER TABLE `tbl_company_settings`
  MODIFY `companySettingsID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_deduction_type`
--
ALTER TABLE `tbl_deduction_type`
  MODIFY `deductionTypeID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_department_type`
--
ALTER TABLE `tbl_department_type`
  MODIFY `departmentTypeID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_employee_allowance`
--
ALTER TABLE `tbl_employee_allowance`
  MODIFY `employeeAllowanceID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_employee_basic_adj`
--
ALTER TABLE `tbl_employee_basic_adj`
  MODIFY `employeeBasicSalaryAdjID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_employee_deduction`
--
ALTER TABLE `tbl_employee_deduction`
  MODIFY `employeeDeductionID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_employee_gov`
--
ALTER TABLE `tbl_employee_gov`
  MODIFY `employeeGovID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_employee_leave`
--
ALTER TABLE `tbl_employee_leave`
  MODIFY `leaveID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_employee_loan`
--
ALTER TABLE `tbl_employee_loan`
  MODIFY `employeeLoanID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_employee_payroll_record`
--
ALTER TABLE `tbl_employee_payroll_record`
  MODIFY `employeePayrollRecordID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_employee_termination`
--
ALTER TABLE `tbl_employee_termination`
  MODIFY `employeeTerminationID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_holiday_type`
--
ALTER TABLE `tbl_holiday_type`
  MODIFY `holidayTypeID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_leave_type`
--
ALTER TABLE `tbl_leave_type`
  MODIFY `leaveTypeID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_loan_type`
--
ALTER TABLE `tbl_loan_type`
  MODIFY `loanTypeID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_position_type`
--
ALTER TABLE `tbl_position_type`
  MODIFY `positionTypeID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_salary_settings`
--
ALTER TABLE `tbl_salary_settings`
  MODIFY `salarySettingID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `userID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
