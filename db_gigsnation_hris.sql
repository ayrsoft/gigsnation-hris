-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 20, 2019 at 08:05 PM
-- Server version: 5.6.38
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `db_gigsnation_hris`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_allowance_type`
--

CREATE TABLE `tbl_allowance_type` (
  `allowanceTypeID` int(100) NOT NULL,
  `allowanceTypeName` text NOT NULL,
  `allowanceTypeTaxable` enum('Y','N') NOT NULL COMMENT 'Y = yes, N = no',
  `allowanceTypeStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_allowance_type`
--

INSERT INTO `tbl_allowance_type` (`allowanceTypeID`, `allowanceTypeName`, `allowanceTypeTaxable`, `allowanceTypeStatus`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, 'Allowance 2', 'Y', 'N', 1, '2018-12-16 14:46:45.000000', 0, '0000-00-00 00:00:00.000000'),
(2, '1', 'Y', 'Y', 1, '2019-01-21 03:09:16.000000', 0, '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_attendance`
--

CREATE TABLE `tbl_attendance` (
  `attendanceID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `attendanceDate` date NOT NULL,
  `timeIn` time(6) NOT NULL,
  `timeOut` time(6) NOT NULL,
  `overtime` int(100) NOT NULL,
  `overtimeStatus` int(100) NOT NULL COMMENT 'Approved = 1, Declined = 2',
  `holidayTypeID` int(100) NOT NULL COMMENT 'tbl_holiday_type',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_attendance`
--

INSERT INTO `tbl_attendance` (`attendanceID`, `employeeID`, `attendanceDate`, `timeIn`, `timeOut`, `overtime`, `overtimeStatus`, `holidayTypeID`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, '001', '2018-08-11', '08:30:00.000000', '17:14:00.000000', 0, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(2, '001', '2018-08-13', '08:05:59.000000', '17:47:00.000000', 0, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(3, '001', '2018-08-14', '07:59:00.000000', '17:18:00.000000', 0, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(4, '001', '2018-08-16', '08:13:00.000000', '19:00:00.000000', 2, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(5, '001', '2018-08-17', '08:22:00.000000', '17:25:00.000000', 0, 2, 2, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(6, '001', '2018-08-18', '08:24:00.000000', '17:15:00.000000', 0, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(7, '001', '2018-08-20', '08:13:00.000000', '17:22:00.000000', 0, 2, 5, 2, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(8, '001', '2018-08-22', '08:09:00.000000', '17:15:00.000000', 0, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(9, '001', '2018-08-23', '08:03:59.000000', '19:00:00.000000', 2, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(10, '001', '2018-08-24', '08:07:00.000000', '17:15:00.000000', 0, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(11, '001', '2018-08-25', '07:58:00.000000', '17:37:00.000000', 0, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(12, '2', '2018-08-11', '07:52:00.000000', '17:12:59.000000', 0, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(13, '2', '2018-08-13', '07:55:00.000000', '17:47:00.000000', 0, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(14, '2', '2018-08-14', '07:54:00.000000', '17:18:00.000000', 0, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(15, '2', '2018-08-15', '07:52:00.000000', '17:38:59.000000', 0, 2, 4, 2, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(16, '2', '2018-08-16', '07:58:00.000000', '17:10:00.000000', 0, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(17, '2', '2018-08-17', '08:00:00.000000', '17:20:00.000000', 0, 2, 2, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(18, '2', '2018-08-18', '08:03:00.000000', '19:00:00.000000', 2, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(19, '2', '2018-08-20', '07:28:00.000000', '17:04:59.000000', 0, 2, 5, 2, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(20, '2', '2018-08-22', '07:49:00.000000', '19:00:00.000000', 2, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(21, '2', '2018-08-23', '07:39:00.000000', '17:03:00.000000', 0, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(22, '2', '2018-08-24', '07:55:00.000000', '17:15:00.000000', 0, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000'),
(23, '2', '2018-08-25', '07:44:00.000000', '17:40:00.000000', 0, 2, 0, 1, '2018-08-25 08:30:00.000000', 0, '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_audit_logs`
--

CREATE TABLE `tbl_audit_logs` (
  `auditLogsID` int(100) NOT NULL,
  `auditLogsDate` datetime(6) NOT NULL,
  `userID` int(100) NOT NULL,
  `auditLogsType` text NOT NULL COMMENT 'Add, Edit',
  `auditLogsDescription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_audit_logs`
--

INSERT INTO `tbl_audit_logs` (`auditLogsID`, `auditLogsDate`, `userID`, `auditLogsType`, `auditLogsDescription`) VALUES
(1, '0000-00-00 00:00:00.000000', 1, 'View', 'View Salary Settings'),
(2, '0000-00-00 00:00:00.000000', 1, 'View', 'View Salary Settings'),
(3, '0000-00-00 00:00:00.000000', 1, 'View', 'View Salary Settings'),
(4, '2018-12-16 20:04:35.000000', 1, 'View', 'View Salary Settings'),
(5, '2018-12-16 20:05:12.000000', 1, 'Search', 'Search Salary Settings'),
(6, '2018-12-16 20:33:06.000000', 1, 'Search', 'Search Salary Settings'),
(7, '2018-12-16 20:33:28.000000', 1, 'View', 'View Salary Settings'),
(8, '2018-12-16 22:07:36.000000', 1, 'View', 'View Salary Settings');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company_settings`
--

CREATE TABLE `tbl_company_settings` (
  `companySettingsID` int(100) NOT NULL,
  `companyName` text NOT NULL,
  `companyContactPerson` text NOT NULL,
  `companyAddress` text NOT NULL,
  `companyCountry` text NOT NULL,
  `companyCity` text NOT NULL,
  `companyState` text NOT NULL,
  `companyPostalCode` int(100) NOT NULL,
  `companyEmail` text NOT NULL,
  `companyPhoneNumber` text NOT NULL,
  `companyMobileNumber` text NOT NULL,
  `companyFaxNumber` text NOT NULL,
  `companyWebsite` text NOT NULL,
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_company_settings`
--

INSERT INTO `tbl_company_settings` (`companySettingsID`, `companyName`, `companyContactPerson`, `companyAddress`, `companyCountry`, `companyCity`, `companyState`, `companyPostalCode`, `companyEmail`, `companyPhoneNumber`, `companyMobileNumber`, `companyFaxNumber`, `companyWebsite`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, 'Testin', 'Ayrah', 'IBM Plaza', 'Philippines', 'Pateros ', 'Metro Manila', 1620, 'comapany@gmail.com', '12321', '2147483647', '11111', 'www.wala.com', 1, '2018-12-16 08:35:23.000000', 1, '2018-12-16 22:03:06.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deduction_type`
--

CREATE TABLE `tbl_deduction_type` (
  `deductionTypeID` int(100) NOT NULL,
  `deductionTypeName` text NOT NULL,
  `deductionTypeStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_deduction_type`
--

INSERT INTO `tbl_deduction_type` (`deductionTypeID`, `deductionTypeName`, `deductionTypeStatus`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, 'Deduction', 'N', 1, '2018-12-13 06:18:09.000000', 1, '2018-12-16 13:32:16.000000'),
(2, 'Deduction 2', 'Y', 1, '2018-12-13 04:19:15.000000', 0, '0000-00-00 00:00:00.000000'),
(3, 'Deduction 3', 'Y', 1, '2018-12-16 13:28:40.000000', 0, '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department_type`
--

CREATE TABLE `tbl_department_type` (
  `departmentTypeID` int(100) NOT NULL,
  `departmentTypeName` text NOT NULL,
  `departmentTypeStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_department_type`
--

INSERT INTO `tbl_department_type` (`departmentTypeID`, `departmentTypeName`, `departmentTypeStatus`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, 'Mobiles', 'Y', 1, '2018-12-16 02:40:10.000000', 1, '2018-12-16 02:49:48.000000'),
(2, 'Information Techonology', 'N', 1, '2018-12-16 02:41:04.000000', 0, '0000-00-00 00:00:00.000000'),
(3, 'Mobile', 'Y', 1, '2018-12-16 02:43:08.000000', 0, '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee`
--

CREATE TABLE `tbl_employee` (
  `employeeID` varchar(100) NOT NULL,
  `firstName` text NOT NULL,
  `lastName` text NOT NULL,
  `middleName` text NOT NULL,
  `extName` text NOT NULL,
  `birthday` date NOT NULL,
  `gender` enum('M','F') NOT NULL COMMENT 'M = male, F = female',
  `civilStatus` int(100) NOT NULL COMMENT 'married = 1, single = 2, widowed = 3, separated = 4, divorced = 5',
  `nationality` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `contactNumber` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee`
--

INSERT INTO `tbl_employee` (`employeeID`, `firstName`, `lastName`, `middleName`, `extName`, `birthday`, `gender`, `civilStatus`, `nationality`, `email`, `contactNumber`) VALUES
('', '1', '1', '1', '1', '0000-00-00', 'M', 1, '1', '1', '1'),
('001', 'Patricia Ayrah', 'Otero', 'Reyes', '', '1995-04-21', 'F', 2, 'Filipino', 'ayrah0421.pao@gmail.com', '09771003619'),
('002', 'Davecar', 'Grave', 'Mabasa', 'Jr', '1994-07-21', 'M', 2, 'Filipino', 'davecar.grave@gmail.com', '09361600304'),
('003', 'eireenaaaaaa1111', 'Estudillo', 'Aguirre', '', '1995-04-21', 'F', 2, 'Filipino', 'ayrah0421.pao@gmail.com', '09771003619'),
('010', 'eireenaaaaaa1111', 'Estudillo', 'Aguirre', '', '1995-04-21', 'F', 2, 'Filipino', 'ayrah0421.pao@gmail.com', '09771003619');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_address`
--

CREATE TABLE `tbl_employee_address` (
  `employeeID` varchar(100) NOT NULL,
  `employeeAddress` text NOT NULL,
  `employeeCity` text NOT NULL,
  `employeeProvince` text NOT NULL,
  `employeeCountry` text NOT NULL,
  `employeeZipcode` int(100) NOT NULL,
  `employeeAddressType` int(100) NOT NULL COMMENT 'present address = 1, residential = 2'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee_address`
--

INSERT INTO `tbl_employee_address` (`employeeID`, `employeeAddress`, `employeeCity`, `employeeProvince`, `employeeCountry`, `employeeZipcode`, `employeeAddressType`) VALUES
('001', '096 Stuazon St. Brgy. Poblacion', 'Pateros', 'Metro Manila', 'Philippines', 1620, 1),
('002', '2 FC Street brgy. San Pedro', 'Pateros', 'Metro Manila', 'Philippines', 1622, 1),
('002', '059 Makisig St. Brgy. 101', 'Sta. Ana Manila', 'Metro Manila', 'Philippines', 1620, 2),
('', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('2011111211', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('2011111211', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('003', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('003', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('003', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('003', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('005', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('005', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('006', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('006', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('007', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('007', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('008', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('008', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('009', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('009', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('010', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('010', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('001', '096 Stuazon St. Brgy. Poblacion', 'Pateros', 'Metro Manila', 'Philippines', 1620, 1),
('002', '2 FC Street brgy. San Pedro', 'Pateros', 'Metro Manila', 'Philippines', 1622, 1),
('002', '059 Makisig St. Brgy. 101', 'Sta. Ana Manila', 'Metro Manila', 'Philippines', 1620, 2),
('', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('2011111211', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('2011111211', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('003', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('003', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('003', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('003', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('005', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('005', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('006', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('006', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('007', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('007', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('008', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('008', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('009', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('009', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('010', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('010', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('001', '096 Stuazon St. Brgy. Poblacion', 'Pateros', 'Metro Manila', 'Philippines', 1620, 1),
('002', '2 FC Street brgy. San Pedro', 'Pateros', 'Metro Manila', 'Philippines', 1622, 1),
('002', '059 Makisig St. Brgy. 101', 'Sta. Ana Manila', 'Metro Manila', 'Philippines', 1620, 2),
('', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('2011111211', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('2011111211', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('003', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('003', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('003', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('003', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('004', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('005', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('005', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('006', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('006', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('007', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('007', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('008', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('008', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('009', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('009', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('010', '096 Stuazon St.', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('010', '0977 M.Almeda St. Brgy. Poblacion', 'Pateros', 'Metro, Manila', 'Philippines', 1620, 0),
('1', '1', '1', '1', '1', 1, 1),
('1', '1', '1', '1', '1', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_allowance`
--

CREATE TABLE `tbl_employee_allowance` (
  `employeeAllowanceID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `allowanceTypeID` int(100) NOT NULL COMMENT 'tbl_allowance_type',
  `employeeAllowanceAmount` int(100) NOT NULL,
  `employeeAllowanceStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee_allowance`
--

INSERT INTO `tbl_employee_allowance` (`employeeAllowanceID`, `employeeID`, `allowanceTypeID`, `employeeAllowanceAmount`, `employeeAllowanceStatus`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, '001', 2, 1, 'Y', 1, '2019-01-21 03:08:59.000000', 1, '2019-01-21 03:17:32.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_basic_adj`
--

CREATE TABLE `tbl_employee_basic_adj` (
  `employeeBasicSalaryAdjID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `employmentBasicSalary` int(100) NOT NULL,
  `employeeBasicSalaryAdjIDate` date NOT NULL,
  `employeeBasicSalaryAdjReason` text NOT NULL,
  `employeeBasicSalaryStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee_basic_adj`
--

INSERT INTO `tbl_employee_basic_adj` (`employeeBasicSalaryAdjID`, `employeeID`, `employmentBasicSalary`, `employeeBasicSalaryAdjIDate`, `employeeBasicSalaryAdjReason`, `employeeBasicSalaryStatus`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, '001', 180000, '2018-12-13', 'Initial Salary', 'Y', 1, '2018-12-13 04:19:15.000000', 0, '0000-00-00 00:00:00.000000'),
(2, '002', 18000, '2018-12-13', 'Initial Salary', 'Y', 1, '2018-12-13 00:00:00.000000', 0, '0000-00-00 00:00:00.000000'),
(3, '006', 10000, '2018-12-18', 'Initial Salary', 'Y', 1, '2018-12-18 03:58:36.000000', 0, '0000-00-00 00:00:00.000000'),
(4, '007', 10000, '2018-12-18', 'Initial Salary', 'Y', 1, '2018-12-18 04:58:49.000000', 0, '0000-00-00 00:00:00.000000'),
(5, '008', 10000, '2018-12-18', 'Initial Salary', 'Y', 1, '2018-12-18 05:01:47.000000', 0, '0000-00-00 00:00:00.000000'),
(6, '009', 10000, '2018-12-18', 'Initial Salary', 'Y', 1, '2018-12-18 05:02:39.000000', 0, '0000-00-00 00:00:00.000000'),
(7, '010', 10000, '2018-12-18', 'Initial Salary', 'Y', 1, '2018-12-18 05:04:26.000000', 0, '0000-00-00 00:00:00.000000'),
(8, '1', 1, '2019-01-20', 'Initial Salary', 'Y', 1, '2019-01-20 21:58:41.000000', 0, '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_deduction`
--

CREATE TABLE `tbl_employee_deduction` (
  `employeeDeductionID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `deductionTypeID` int(100) NOT NULL COMMENT 'tbl_deduction_type',
  `deductionAmount` int(100) NOT NULL,
  `deductionValidFrom` date NOT NULL,
  `deductionValidTo` date NOT NULL,
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_educational`
--

CREATE TABLE `tbl_employee_educational` (
  `educationalID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `educationalLevelName` text NOT NULL,
  `educationalStartDate` date NOT NULL,
  `educationalEndDate` date NOT NULL,
  `educationalMajorSpecialization` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee_educational`
--

INSERT INTO `tbl_employee_educational` (`educationalID`, `employeeID`, `educationalLevelName`, `educationalStartDate`, `educationalEndDate`, `educationalMajorSpecialization`) VALUES
(1, '001', 'College', '2011-04-21', '2016-04-21', 'Bachelor of Science in Information and Communication Technology'),
(2, '002', 'College', '2010-04-21', '2015-04-21', 'Bachelor of Science in Information and Communication Technology'),
(3, '010', 'Seconday', '2018-01-01', '2018-01-02', 'Nothing'),
(4, '010', 'Seconday', '2018-02-01', '2018-02-02', 'Nothing');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_emergency_contact`
--

CREATE TABLE `tbl_employee_emergency_contact` (
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `emergencyContactName` text NOT NULL,
  `emergencyContactNum` text NOT NULL,
  `emergencyRelationship` text NOT NULL,
  `emergencyRemarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee_emergency_contact`
--

INSERT INTO `tbl_employee_emergency_contact` (`employeeID`, `emergencyContactName`, `emergencyContactNum`, `emergencyRelationship`, `emergencyRemarks`) VALUES
('001', 'Alma Otero', '', 'Sister', 'This is for testing purposes'),
('002', 'Encarnation Grave', '09171003612', 'Mother', 'This is for testing purposes'),
('', '', '0977788822', '', ''),
('', '', '0977788822', '', ''),
('2011111211', '', '0977788822', '', ''),
('003', '', '0977788822', '', ''),
('003', '', '0977788822', '', ''),
('004', '', '0977788822', '', ''),
('004', '', '0977788822', '', ''),
('004', '', '0977788822', '', ''),
('005', '', '0977788822', '', ''),
('006', '', '0977788822', '', ''),
('007', '', '0977788822', '', ''),
('008', '', '0977788822', '', ''),
('009', '', '0977788822', '', ''),
('010', '', '0977788822', '', ''),
('001', 'Alma Otero', '', 'Sister', 'This is for testing purposes'),
('002', 'Encarnation Grave', '09171003612', 'Mother', 'This is for testing purposes'),
('', '', '0977788822', '', ''),
('', '', '0977788822', '', ''),
('2011111211', '', '0977788822', '', ''),
('003', '', '0977788822', '', ''),
('003', '', '0977788822', '', ''),
('004', '', '0977788822', '', ''),
('004', '', '0977788822', '', ''),
('004', '', '0977788822', '', ''),
('005', '', '0977788822', '', ''),
('006', '', '0977788822', '', ''),
('007', '', '0977788822', '', ''),
('008', '', '0977788822', '', ''),
('009', '', '0977788822', '', ''),
('010', '', '0977788822', '', ''),
('001', 'Alma Otero', '', 'Sister', 'This is for testing purposes'),
('002', 'Encarnation Grave', '09171003612', 'Mother', 'This is for testing purposes'),
('', '', '0977788822', '', ''),
('', '', '0977788822', '', ''),
('2011111211', '', '0977788822', '', ''),
('003', '', '0977788822', '', ''),
('003', '', '0977788822', '', ''),
('004', '', '0977788822', '', ''),
('004', '', '0977788822', '', ''),
('004', '', '0977788822', '', ''),
('005', '', '0977788822', '', ''),
('006', '', '0977788822', '', ''),
('007', '', '0977788822', '', ''),
('008', '', '0977788822', '', ''),
('009', '', '0977788822', '', ''),
('010', '', '0977788822', '', ''),
('1', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_employment`
--

CREATE TABLE `tbl_employee_employment` (
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `departmentTypeID` int(100) NOT NULL COMMENT 'tbl_department_type',
  `positionTypeID` int(100) NOT NULL COMMENT 'tbl_position_type',
  `employmentOriginalDateHIred` date NOT NULL,
  `employmentStartDate` date NOT NULL,
  `employmentTypeID` int(100) NOT NULL COMMENT 'tbl_employment_type',
  `employmentPaymentGroup` int(100) NOT NULL COMMENT 'weekly = 1, semi = 2, monthly = 3',
  `employmentPaymentType` int(100) NOT NULL COMMENT 'remittance = 1, bank = 2',
  `employmentStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee_employment`
--

INSERT INTO `tbl_employee_employment` (`employeeID`, `departmentTypeID`, `positionTypeID`, `employmentOriginalDateHIred`, `employmentStartDate`, `employmentTypeID`, `employmentPaymentGroup`, `employmentPaymentType`, `employmentStatus`) VALUES
('', 0, 0, '0000-00-00', '2018-02-03', 1, 0, 0, 'Y'),
('001', 1, 1, '2018-12-13', '2018-12-17', 1, 1, 1, 'Y'),
('002', 2, 1, '2018-12-13', '2018-12-17', 1, 1, 1, 'Y'),
('003', 0, 0, '0000-00-00', '2018-02-03', 1, 0, 0, 'Y'),
('004', 0, 0, '0000-00-00', '2018-02-03', 1, 0, 0, 'Y'),
('005', 0, 0, '0000-00-00', '2018-02-03', 1, 0, 0, 'Y'),
('006', 1, 1, '2018-02-02', '2018-02-03', 1, 1, 1, 'Y'),
('007', 1, 1, '2018-02-02', '2018-02-03', 1, 1, 1, 'Y'),
('008', 1, 1, '2018-02-02', '2018-02-03', 1, 1, 1, 'Y'),
('009', 1, 1, '2018-02-02', '2018-02-03', 1, 1, 1, 'Y'),
('010', 1, 1, '2018-02-02', '2018-02-03', 1, 1, 1, 'Y'),
('1', 1, 1, '0000-00-00', '0000-00-00', 1, 1, 1, 'Y'),
('2011111211', 0, 0, '0000-00-00', '2018-02-03', 1, 0, 0, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_gov`
--

CREATE TABLE `tbl_employee_gov` (
  `employeeGovID` int(11) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `governmentType` int(100) NOT NULL COMMENT 'HDMF = 1, TIN = 2, SSS = 3, PHILHEALTH = 4',
  `governmentNumber` varchar(100) NOT NULL,
  `governmentStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee_gov`
--

INSERT INTO `tbl_employee_gov` (`employeeGovID`, `employeeID`, `governmentType`, `governmentNumber`, `governmentStatus`) VALUES
(1, '10', 1, '111111', 'Y'),
(2, '10', 1, '222222', 'Y'),
(3, '10', 1, '333333', 'Y'),
(4, '001', 4, '777777', 'Y'),
(5, '002', 4, '777777', 'Y'),
(6, '002', 1, '8888', 'Y'),
(7, '006', 1, '111111', 'Y'),
(8, '006', 1, '222222', 'Y'),
(9, '006', 1, '333333', 'Y'),
(10, '007', 1, '111111', 'Y'),
(11, '007', 1, '222222', 'Y'),
(12, '007', 1, '333333', 'Y'),
(13, '008', 1, '111111', 'Y'),
(14, '008', 1, '222222', 'Y'),
(15, '008', 1, '333333', 'Y'),
(16, '009', 1, '111111', 'Y'),
(17, '009', 1, '222222', 'Y'),
(18, '009', 1, '333333', 'Y'),
(19, '010', 1, '111111', 'Y'),
(20, '010', 1, '222222', 'Y'),
(21, '010', 1, '333333', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_leave`
--

CREATE TABLE `tbl_employee_leave` (
  `leaveID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `leaveTypeID` int(100) NOT NULL COMMENT 'tbl_leave_type',
  `employeeLeaveApplyDate` date NOT NULL,
  `employeeNumDaysLeave` int(100) NOT NULL,
  `employeeLeaveStartDate` date NOT NULL,
  `employeeLeaveEndDate` date NOT NULL,
  `employeeLeaveRemarks` text NOT NULL,
  `employeeLeaveStatus` int(100) NOT NULL COMMENT 'Approved = 1, Declined = 2',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee_leave`
--

INSERT INTO `tbl_employee_leave` (`leaveID`, `employeeID`, `leaveTypeID`, `employeeLeaveApplyDate`, `employeeNumDaysLeave`, `employeeLeaveStartDate`, `employeeLeaveEndDate`, `employeeLeaveRemarks`, `employeeLeaveStatus`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, '001', 2, '2018-12-20', 5, '2018-12-25', '2018-12-30', 'Family Vacationsssss', 2, 1, '2018-12-15 23:50:14.000000', 1, '2019-01-20 16:05:27.000000'),
(2, '001', 1, '2018-11-20', 3, '2018-12-25', '2018-12-30', 'Family Vacation', 1, 0, '2018-12-15 23:52:51.000000', 0, '0000-00-00 00:00:00.000000'),
(8, '001', 2, '2018-12-20', 2, '2018-12-20', '2018-12-20', 'Family Vacation', 1, 0, '2018-12-15 23:56:44.000000', 0, '0000-00-00 00:00:00.000000'),
(9, '002', 2, '2018-09-20', 1, '2018-12-25', '2018-12-30', 'Family Vacation', 1, 0, '2018-12-16 00:11:04.000000', 0, '0000-00-00 00:00:00.000000'),
(10, '002', 2, '2018-08-20', 1, '2018-12-25', '2018-12-30', 'Family Vacation', 1, 1, '2018-12-16 00:11:45.000000', 0, '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_loan`
--

CREATE TABLE `tbl_employee_loan` (
  `employeeLoanID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `loanTypeID` int(100) NOT NULL COMMENT 'tbl_loan_type',
  `employeePrincipalAmount` int(100) NOT NULL,
  `employeeLoanAmount` int(100) NOT NULL,
  `employeeLoanRemainingBalance` int(100) NOT NULL,
  `employeeLoanTerms` int(100) NOT NULL COMMENT 'months',
  `employeeLoanScheme` int(100) NOT NULL COMMENT 'weekly = 1, semi = 2, monthly = 3',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_payroll_record`
--

CREATE TABLE `tbl_employee_payroll_record` (
  `employeePayrollRecordID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `employeeGrossIncome` int(100) NOT NULL,
  `employeeDaysWorked` int(100) NOT NULL,
  `employeeNetIncome` int(100) NOT NULL,
  `employeeDaysDeduction` int(100) NOT NULL COMMENT 'Absent',
  `employeeLateDeduction` int(100) NOT NULL,
  `employeeOvertimeTotalAmount` int(100) NOT NULL,
  `employeeHolidayTotalAmount` int(100) NOT NULL,
  `employeeTaxTotalAmount` int(100) NOT NULL,
  `employeeSSSLoan` int(100) NOT NULL,
  `employeePagibigLoan` int(100) NOT NULL,
  `employeeOtherDeductionTotal` int(100) NOT NULL,
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_termination`
--

CREATE TABLE `tbl_employee_termination` (
  `employeeTerminationID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `terminationDate` date NOT NULL,
  `TerminationReason` text NOT NULL,
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee_termination`
--

INSERT INTO `tbl_employee_termination` (`employeeTerminationID`, `employeeID`, `terminationDate`, `TerminationReason`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, '002', '2019-01-16', 'Testing', 1, '2019-01-16 08:00:00.000000', 3, '2019-01-16 06:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employment_type`
--

CREATE TABLE `tbl_employment_type` (
  `employmentTypeID` int(100) NOT NULL,
  `employmentTypeName` text NOT NULL,
  `employmentTypeStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employment_type`
--

INSERT INTO `tbl_employment_type` (`employmentTypeID`, `employmentTypeName`, `employmentTypeStatus`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, 'Partime 2', 'Y', 1, '2018-12-16 04:40:47.000000', 1, '2018-12-16 04:46:42.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_holiday_type`
--

CREATE TABLE `tbl_holiday_type` (
  `holidayTypeID` int(100) NOT NULL,
  `holidayName` text NOT NULL,
  `holidayDate` date NOT NULL,
  `holidayDay` int(100) NOT NULL COMMENT 'Monday = 1, Tuesday = 2, Wednesday = 3, Thursday = 4, Friday = 5',
  `holidayType` int(100) NOT NULL COMMENT 'Regular = 1, Special = 2',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_holiday_type`
--

INSERT INTO `tbl_holiday_type` (`holidayTypeID`, `holidayName`, `holidayDate`, `holidayDay`, `holidayType`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, 'christmass', '2018-12-25', 2, 1, 1, '2018-12-16 01:29:09.000000', 1, '2018-12-16 01:36:54.000000'),
(2, 'christmas', '2018-12-25', 2, 1, 1, '2018-12-16 01:42:42.000000', 0, '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_leave_type`
--

CREATE TABLE `tbl_leave_type` (
  `leaveTypeID` int(100) NOT NULL,
  `leaveTypeName` text NOT NULL,
  `leaveTypeStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_leave_type`
--

INSERT INTO `tbl_leave_type` (`leaveTypeID`, `leaveTypeName`, `leaveTypeStatus`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, 'Leave', 'N', 1, '2018-12-16 13:58:54.000000', 1, '2018-12-16 14:01:01.000000'),
(2, 'Leave 2', 'Y', 1, '2018-12-16 13:59:02.000000', 0, '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_loan_type`
--

CREATE TABLE `tbl_loan_type` (
  `loanTypeID` int(100) NOT NULL,
  `loanTypeName` text NOT NULL,
  `loanTypeStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_loan_type`
--

INSERT INTO `tbl_loan_type` (`loanTypeID`, `loanTypeName`, `loanTypeStatus`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, 'Loan 2', 'Y', 1, '2018-12-16 15:25:41.000000', 1, '2018-12-16 15:26:46.000000'),
(2, 'Loan 2', 'N', 1, '2018-12-16 15:25:49.000000', 0, '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pay_run`
--

CREATE TABLE `tbl_pay_run` (
  `payRunID` int(100) NOT NULL,
  `userID` int(100) NOT NULL,
  `employmentPaymentGroup` int(100) NOT NULL,
  `payRunDate` date NOT NULL,
  `payRunFrom` date NOT NULL,
  `payRunTo` date NOT NULL,
  `payRunStatus` enum('Y','N') NOT NULL COMMENT 'Y = Open, N = Closed'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_pay_run`
--

INSERT INTO `tbl_pay_run` (`payRunID`, `userID`, `employmentPaymentGroup`, `payRunDate`, `payRunFrom`, `payRunTo`, `payRunStatus`) VALUES
(1, 1, 1, '2018-08-18', '2018-08-13', '2018-08-18', 'Y'),
(2, 1, 1, '2018-08-04', '2018-07-29', '2018-08-04', 'Y'),
(3, 1, 1, '2018-08-11', '2018-08-05', '2018-08-11', 'Y'),
(4, 1, 1, '2018-08-25', '2018-08-19', '2018-08-25', 'Y'),
(5, 1, 2, '2018-08-15', '2018-08-01', '2018-08-15', 'Y'),
(6, 1, 2, '2018-08-31', '2018-08-16', '2018-08-31', 'Y'),
(9, 1, 3, '2018-08-31', '2018-08-01', '2018-08-31', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_position_type`
--

CREATE TABLE `tbl_position_type` (
  `positionTypeID` int(100) NOT NULL,
  `departmentTypeID` int(100) NOT NULL COMMENT 'tbl_department_type',
  `positionTypeName` text NOT NULL,
  `positionTypeSalary` int(100) NOT NULL,
  `positionTypeStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_position_type`
--

INSERT INTO `tbl_position_type` (`positionTypeID`, `departmentTypeID`, `positionTypeName`, `positionTypeSalary`, `positionTypeStatus`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, 1, 'Software Developer 2', 10000, 'Y', 1, '2018-12-13 06:21:06.000000', 1, '2018-12-16 03:23:36.000000'),
(2, 1, 'Software Developersssss', 10000, 'Y', 1, '2018-12-13 06:21:06.000000', 1, '2018-12-16 03:23:06.000000'),
(3, 2, 'Information Technology', 40000, 'Y', 1, '2018-12-13 06:21:06.000000', 0, '0000-00-00 00:00:00.000000'),
(4, 1, 'Software Developer', 1000, 'N', 1, '2018-12-16 03:13:02.000000', 0, '0000-00-00 00:00:00.000000'),
(5, 1, 'Software Developer 1', 1000, 'N', 1, '2018-12-16 03:14:22.000000', 0, '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reset_password`
--

CREATE TABLE `tbl_reset_password` (
  `resetPasswordID` int(100) NOT NULL,
  `userName` varchar(100) NOT NULL,
  `resetPasswordStatus` int(100) NOT NULL,
  `resetPasswordDateTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_reset_password`
--

INSERT INTO `tbl_reset_password` (`resetPasswordID`, `userName`, `resetPasswordStatus`, `resetPasswordDateTime`, `updatedBy`, `updatedTime`) VALUES
(1, 'patricia.otero', 2, '2018-12-15 07:08:20.000000', 1, '2019-01-14 02:38:20.000000'),
(2, 'patricia.otero', 1, '2018-12-15 16:19:49.000000', 0, '0000-00-00 00:00:00.000000'),
(3, 'patricia.otero', 1, '2018-12-15 16:20:54.000000', 0, '0000-00-00 00:00:00.000000'),
(4, 'patricia.otero', 1, '2018-12-15 16:21:20.000000', 0, '0000-00-00 00:00:00.000000'),
(5, 'patricia.otero', 1, '2018-12-15 16:31:45.000000', 0, '0000-00-00 00:00:00.000000'),
(6, 'patricia.otero', 1, '2018-12-15 16:33:40.000000', 0, '0000-00-00 00:00:00.000000'),
(7, 'patricia.otero', 1, '2019-01-14 02:06:07.000000', 0, '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_salary_settings`
--

CREATE TABLE `tbl_salary_settings` (
  `salarySettingID` int(100) NOT NULL,
  `salarySettingName` text NOT NULL,
  `salarySettingValueRate` int(100) NOT NULL,
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_salary_settings`
--

INSERT INTO `tbl_salary_settings` (`salarySettingID`, `salarySettingName`, `salarySettingValueRate`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, 'Loan 2', 20, 1, '2018-12-16 16:33:28.000000', 1, '2018-12-16 16:35:04.000000'),
(2, 'Loan 1', 30, 1, '2018-12-16 16:33:44.000000', 0, '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `userID` int(100) NOT NULL,
  `employeeID` varchar(100) NOT NULL COMMENT 'tbl_employee',
  `userName` varchar(100) NOT NULL,
  `userPassword` varchar(100) NOT NULL,
  `userLevel` int(100) NOT NULL COMMENT 'Admin = 1, HR Manager = 2, HR Assistant = 3',
  `userStatus` enum('Y','N') NOT NULL COMMENT 'Y = active, N = inactive',
  `createdBy` int(100) NOT NULL,
  `createdTime` datetime(6) NOT NULL,
  `updatedBy` int(100) NOT NULL,
  `updatedTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`userID`, `employeeID`, `userName`, `userPassword`, `userLevel`, `userStatus`, `createdBy`, `createdTime`, `updatedBy`, `updatedTime`) VALUES
(1, '001', 'patricia.otero', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, 'Y', 1, '2018-12-13 08:16:11.000000', 0, '0000-00-00 00:00:00.000000'),
(2, '002', 'davecar.grave', '8CB2237D0679CA88DB6464EAC60DA96345513964', 1, 'Y', 1, '2018-12-13 08:16:11.000000', 1, '2018-12-13 08:16:11.000000'),
(3, '011', 'eireenaaaaaa1111.estudillo', '8cb2237d0679ca88db6464eac60da96345513964', 0, 'Y', 1, '2018-12-17 23:15:21.000000', 0, '0000-00-00 00:00:00.000000'),
(4, '012', 'eireenaaaaaa1111.estudillo', '8cb2237d0679ca88db6464eac60da96345513964', 0, 'Y', 1, '2018-12-17 23:16:22.000000', 0, '0000-00-00 00:00:00.000000'),
(5, '2011111211', 'eireenaaaaaa1111.estudillo', '8cb2237d0679ca88db6464eac60da96345513964', 0, 'Y', 1, '2018-12-18 03:20:43.000000', 0, '0000-00-00 00:00:00.000000'),
(6, '003', 'eireenaaaaaa1111.estudillo', '8cb2237d0679ca88db6464eac60da96345513964', 0, 'Y', 1, '2018-12-18 03:23:59.000000', 0, '0000-00-00 00:00:00.000000'),
(7, '003', 'eireenaaaaaa1111.estudillo', '8cb2237d0679ca88db6464eac60da96345513964', 0, 'Y', 1, '2018-12-18 03:24:29.000000', 0, '0000-00-00 00:00:00.000000'),
(8, '004', 'eireenaaaaaa1111.estudillo', '8cb2237d0679ca88db6464eac60da96345513964', 0, 'Y', 1, '2018-12-18 03:24:52.000000', 0, '0000-00-00 00:00:00.000000'),
(9, '004', 'eireenaaaaaa1111.estudillo', '8cb2237d0679ca88db6464eac60da96345513964', 0, 'Y', 1, '2018-12-18 03:33:16.000000', 0, '0000-00-00 00:00:00.000000'),
(10, '004', 'eireena.estudillo', '8cb2237d0679ca88db6464eac60da96345513964', 0, 'Y', 1, '2018-12-18 03:35:50.000000', 0, '0000-00-00 00:00:00.000000'),
(11, '005', 'eireenaa.estudillo', '8cb2237d0679ca88db6464eac60da96345513964', 0, 'Y', 1, '2018-12-18 03:42:16.000000', 0, '0000-00-00 00:00:00.000000'),
(12, '006', 'eireenaa.estudilloo', '8cb2237d0679ca88db6464eac60da96345513964', 0, 'Y', 1, '2018-12-18 03:58:36.000000', 0, '0000-00-00 00:00:00.000000'),
(13, '007', 'eireenaaa.estudilloo', '8cb2237d0679ca88db6464eac60da96345513964', 0, 'Y', 1, '2018-12-18 04:58:49.000000', 0, '0000-00-00 00:00:00.000000'),
(14, '008', 'eireenaaaa.estudilloo', '8cb2237d0679ca88db6464eac60da96345513964', 0, 'Y', 1, '2018-12-18 05:01:47.000000', 0, '0000-00-00 00:00:00.000000'),
(15, '009', 'eireenaaaaa.estudilloo', '8cb2237d0679ca88db6464eac60da96345513964', 0, 'Y', 1, '2018-12-18 05:02:39.000000', 0, '0000-00-00 00:00:00.000000'),
(16, '010', 'eireenaaaaaa.estudilloo', '8cb2237d0679ca88db6464eac60da96345513964', 0, 'Y', 1, '2018-12-18 05:04:26.000000', 0, '0000-00-00 00:00:00.000000'),
(17, '1', '1.1', '8cb2237d0679ca88db6464eac60da96345513964', 1, 'Y', 1, '2019-01-20 21:58:41.000000', 0, '0000-00-00 00:00:00.000000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_allowance_type`
--
ALTER TABLE `tbl_allowance_type`
  ADD PRIMARY KEY (`allowanceTypeID`);

--
-- Indexes for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  ADD PRIMARY KEY (`attendanceID`);

--
-- Indexes for table `tbl_audit_logs`
--
ALTER TABLE `tbl_audit_logs`
  ADD PRIMARY KEY (`auditLogsID`);

--
-- Indexes for table `tbl_company_settings`
--
ALTER TABLE `tbl_company_settings`
  ADD PRIMARY KEY (`companySettingsID`);

--
-- Indexes for table `tbl_deduction_type`
--
ALTER TABLE `tbl_deduction_type`
  ADD PRIMARY KEY (`deductionTypeID`);

--
-- Indexes for table `tbl_department_type`
--
ALTER TABLE `tbl_department_type`
  ADD PRIMARY KEY (`departmentTypeID`);

--
-- Indexes for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  ADD PRIMARY KEY (`employeeID`);

--
-- Indexes for table `tbl_employee_allowance`
--
ALTER TABLE `tbl_employee_allowance`
  ADD PRIMARY KEY (`employeeAllowanceID`),
  ADD UNIQUE KEY `employeeID` (`employeeID`);

--
-- Indexes for table `tbl_employee_basic_adj`
--
ALTER TABLE `tbl_employee_basic_adj`
  ADD PRIMARY KEY (`employeeBasicSalaryAdjID`),
  ADD UNIQUE KEY `employeeID` (`employeeID`);

--
-- Indexes for table `tbl_employee_deduction`
--
ALTER TABLE `tbl_employee_deduction`
  ADD PRIMARY KEY (`employeeDeductionID`);

--
-- Indexes for table `tbl_employee_educational`
--
ALTER TABLE `tbl_employee_educational`
  ADD PRIMARY KEY (`educationalID`);

--
-- Indexes for table `tbl_employee_employment`
--
ALTER TABLE `tbl_employee_employment`
  ADD PRIMARY KEY (`employeeID`);

--
-- Indexes for table `tbl_employee_gov`
--
ALTER TABLE `tbl_employee_gov`
  ADD PRIMARY KEY (`employeeGovID`);

--
-- Indexes for table `tbl_employee_leave`
--
ALTER TABLE `tbl_employee_leave`
  ADD PRIMARY KEY (`leaveID`);

--
-- Indexes for table `tbl_employee_loan`
--
ALTER TABLE `tbl_employee_loan`
  ADD PRIMARY KEY (`employeeLoanID`);

--
-- Indexes for table `tbl_employee_payroll_record`
--
ALTER TABLE `tbl_employee_payroll_record`
  ADD PRIMARY KEY (`employeePayrollRecordID`),
  ADD UNIQUE KEY `employeeID` (`employeeID`);

--
-- Indexes for table `tbl_employee_termination`
--
ALTER TABLE `tbl_employee_termination`
  ADD PRIMARY KEY (`employeeTerminationID`);

--
-- Indexes for table `tbl_employment_type`
--
ALTER TABLE `tbl_employment_type`
  ADD PRIMARY KEY (`employmentTypeID`);

--
-- Indexes for table `tbl_holiday_type`
--
ALTER TABLE `tbl_holiday_type`
  ADD PRIMARY KEY (`holidayTypeID`);

--
-- Indexes for table `tbl_leave_type`
--
ALTER TABLE `tbl_leave_type`
  ADD PRIMARY KEY (`leaveTypeID`);

--
-- Indexes for table `tbl_loan_type`
--
ALTER TABLE `tbl_loan_type`
  ADD PRIMARY KEY (`loanTypeID`);

--
-- Indexes for table `tbl_pay_run`
--
ALTER TABLE `tbl_pay_run`
  ADD PRIMARY KEY (`payRunID`);

--
-- Indexes for table `tbl_position_type`
--
ALTER TABLE `tbl_position_type`
  ADD PRIMARY KEY (`positionTypeID`);

--
-- Indexes for table `tbl_reset_password`
--
ALTER TABLE `tbl_reset_password`
  ADD PRIMARY KEY (`resetPasswordID`);

--
-- Indexes for table `tbl_salary_settings`
--
ALTER TABLE `tbl_salary_settings`
  ADD PRIMARY KEY (`salarySettingID`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_allowance_type`
--
ALTER TABLE `tbl_allowance_type`
  MODIFY `allowanceTypeID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  MODIFY `attendanceID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tbl_audit_logs`
--
ALTER TABLE `tbl_audit_logs`
  MODIFY `auditLogsID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_company_settings`
--
ALTER TABLE `tbl_company_settings`
  MODIFY `companySettingsID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_deduction_type`
--
ALTER TABLE `tbl_deduction_type`
  MODIFY `deductionTypeID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_department_type`
--
ALTER TABLE `tbl_department_type`
  MODIFY `departmentTypeID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_employee_allowance`
--
ALTER TABLE `tbl_employee_allowance`
  MODIFY `employeeAllowanceID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_employee_basic_adj`
--
ALTER TABLE `tbl_employee_basic_adj`
  MODIFY `employeeBasicSalaryAdjID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_employee_deduction`
--
ALTER TABLE `tbl_employee_deduction`
  MODIFY `employeeDeductionID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_employee_educational`
--
ALTER TABLE `tbl_employee_educational`
  MODIFY `educationalID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_employee_gov`
--
ALTER TABLE `tbl_employee_gov`
  MODIFY `employeeGovID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tbl_employee_leave`
--
ALTER TABLE `tbl_employee_leave`
  MODIFY `leaveID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_employee_loan`
--
ALTER TABLE `tbl_employee_loan`
  MODIFY `employeeLoanID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_employee_payroll_record`
--
ALTER TABLE `tbl_employee_payroll_record`
  MODIFY `employeePayrollRecordID` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_employee_termination`
--
ALTER TABLE `tbl_employee_termination`
  MODIFY `employeeTerminationID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_employment_type`
--
ALTER TABLE `tbl_employment_type`
  MODIFY `employmentTypeID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_holiday_type`
--
ALTER TABLE `tbl_holiday_type`
  MODIFY `holidayTypeID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_leave_type`
--
ALTER TABLE `tbl_leave_type`
  MODIFY `leaveTypeID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_loan_type`
--
ALTER TABLE `tbl_loan_type`
  MODIFY `loanTypeID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_pay_run`
--
ALTER TABLE `tbl_pay_run`
  MODIFY `payRunID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_position_type`
--
ALTER TABLE `tbl_position_type`
  MODIFY `positionTypeID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_reset_password`
--
ALTER TABLE `tbl_reset_password`
  MODIFY `resetPasswordID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_salary_settings`
--
ALTER TABLE `tbl_salary_settings`
  MODIFY `salarySettingID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `userID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
