<?php
session_start();
include_once 'db_connection.php';
use \Firebase\JWT\JWT;
date_default_timezone_set('Asia/Manila');
$currentTimeDate = date("Y-m-d H:i:s");
$currentDate = date("Y-m-d");
$message = "";
$returndata = array();
$list = array();
$token = (isset($_POST['jwt'])) ? verifyToken(($_POST['jwt'])) : "";

//CONSTANT
define("WORKDAYS", "26");
define("PHILHEALTH", "100");
define("HDMF", "100");

function verifyToken($jwt){

  include_once 'jwt/core.php';
  include_once 'jwt/libs/php-jwt-master/src/BeforeValidException.php';
  include_once 'jwt/libs/php-jwt-master/src/ExpiredException.php';
  include_once 'jwt/libs/php-jwt-master/src/SignatureInvalidException.php';
  include_once 'jwt/libs/php-jwt-master/src/JWT.php';

  if($jwt){
       // if decode succeed, show user details
       try {
           // decode jwt
           $decoded = JWT::decode($jwt, $key, array('HS256'));
           $data = $decoded->data;
           $GLOBALS['currentUser'] = $data->id;

           // set response code
           http_response_code(200);
           // show user details
            $status = "success";
       }
       // if decode fails, it means jwt is invalid
      catch (Exception $e){
          // set response code
          http_response_code(401);
          // tell the user access denied  & show error message
          $status = "denied";
      }
   }
   // show error message if jwt is empty
   else{
       // set response code
       http_response_code(401);
       // tell the user access denied
       $status = "denied";
   }
   return $status;
}

function getOvertime($timeIn, $timeOut){
  $overtime = 0;
  $timeIn = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $timeIn);
  $timeOut = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $timeOut);
  sscanf($timeIn, "%d:%d:%d", $hoursIn, $minutesIn, $secondsIn);
  sscanf($timeOut, "%d:%d:%d", $hoursOut, $minutesOut, $secondsOut);

    $timeInSeconds = $hoursIn * 3600 + $minutesIn * 60 + $secondsIn;
    $timeOutSeconds = $hoursOut * 3600 + $minutesOut * 60 + $secondsOut;
    $timeWorked = ($timeOutSeconds - $timeInSeconds) / 3600;
    //1HR = 3600 Seconds
    if($timeWorked >= 10){
      $overtime = $timeWorked - 9;
    }
  return $overtime;
}

function getLate($officeTime, $timeIn){
  $late = "";
  $officeTime = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $officeTime);
  $timeIn = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $timeIn);
  sscanf($officeTime, "%d:%d:%d", $hoursOfficeTime, $minutesOfficeTime, $secondsOfficeTime);
  sscanf($timeIn, "%d:%d:%d", $hoursIn, $minutesIn, $secondsIn);
  $officeTimeSeconds = $hoursOfficeTime * 3600 + $minutesOfficeTime * 60 + $secondsOfficeTime;
  $timeInSeconds = $hoursIn * 3600 + $minutesIn * 60 + $secondsIn;
  $late = ($timeInSeconds - $officeTimeSeconds) / 3600;
  return $late;
}

function getDay($data){
  $specificDay = strtotime($data);
  $specificDay = date('l', $specificDay);
  if($specificDay == "Monday"){
    $specificDay = 1;
  }else if ($specificDay == "Tuesday") {
    $specificDay = 2;
  }else if ($specificDay == "Wednesday") {
    $specificDay = 3;
  }else if($specificDay == "Thursday"){
    $specificDay = 4;
  }else if($specificDay == "Friday"){
    $specificDay = 5;
  }else if($specificDay == "Saturday"){
    $specificDay = 6;
  }else if($specificDay == "Sunday"){
    $specificDay = 7;
  }
  return $specificDay;
}

function getWeek($data){
    return ceil( date( 'j', strtotime( $data ) ) / 7 );
}

function getOfficeTime($data){
  $result = $data;
  if($result <= 5){
    $result = "08:00:00.000000";
  }else {
    $result = "07:00:00.000000";
  }
  return $result;
}

function createJsonData($arrName, $returndata){
  $arr = array(
  	$arrName => $returndata
  );
  echo json_encode($arr);
}

function showAllData($table,$name){
  $result = array();
  $conn = $GLOBALS['conn'];
  foreach ($name as $row) {
    $sql = "SELECT $row FROM $table";
  }
  if ($resultSql=mysqli_query($conn,$sql)){
    $result['data'] = $resultSql;
    $result['status'] = "SUCCESS";
  }else{
    //IF NO DATA FOUND
    $result['status'] = "ERROR";
    $result['message'] = "ERROR: 500 Status Codes";
  }

  return $result;
}

function selectData($table,$name,$where){
  $result = array();
  $conn = $GLOBALS['conn'];
  foreach ($name as $row) {
    $sql = "SELECT $row FROM $table $where";
  }
  if ($resultSql=mysqli_query($conn,$sql)){
    $result['data'] = $resultSql;
    $result['status'] = "SUCCESS";
  }else{
    //IF NO DATA FOUND
    $result['status'] = "ERROR";
    $result['message'] = "ERROR: 500 Status Codes";
  }

  return $result;
}

function insertAllData($table,$field,$data,$msg){
  $result = array();
  $conn = $GLOBALS['conn'];
  $field_values= implode(',',$field);
  $sql='INSERT INTO '.$table.' ('.$field_values.') VALUES ("' . implode('", "', $data) . '")';
  if ($conn->query($sql) === TRUE) {
    $result['status'] = "SUCCESS";
    $result['message'] = $msg;
  } else {
    $result['status'] = "ERROR";
    $result['message'] = "ERROR: 500 Status Codes";
  }

  return $result;
}

function updateData($table,$field,$data,$idName,$id,$msg){
    $result = array();
    $conn = $GLOBALS['conn'];
    $set = '';
    $x = 1;

    foreach($field as $name => $value) {
        $set .= "{$value} = \"{$data[$name]}\"";
        if($x < count($field)) {
            $set .= ',';
        }
        $x++;
    }

    $sql = "UPDATE {$table} SET {$set} WHERE $idName = '{$id}'; ";
    if ($conn->query($sql) === TRUE) {
      $result['status'] = "SUCCESS";
      $result['message'] = $msg;
    } else {
      $result['status'] = "ERROR";
      $result['message'] = "ERROR: 500 Status Codes";
    }

    return $result;
}

function auditLogs($auditLogsType, $auditLogsDescription){
    $table = "tbl_audit_logs";
    $field = array("auditLogsDate", "userID", "auditLogsType", "auditLogsDescription");
    $currentTimeDate = date("Y-m-d H:i:s");
    $data = array($currentTimeDate, $currentUser, $auditLogsType, $auditLogsDescription);
    insertAllData($table,$field,$data, "");
}

function stringEndsWith($string, $subString, $caseSensitive = true) {

    if ($caseSensitive === false) {
        $string		= mb_strtolower($string);
        $subString  = mb_strtolower($subString);
    }

    $strlen 			= strlen($string);
    $subStringLength 	= strlen($subString);

    if ($subStringLength > $strlen) {
        return false;
    }
    return substr_compare($string, $subString, $strlen - $subStringLength, $subStringLength) === 0;
}

function removeLastString($txt){
  $str= preg_replace('/\W\w+\s*(\W*)$/', '$1', $txt);
  return $str;
}

function checkIfEmpty($data){
  if($data == ""){
    $data = "";
  }
  return $data;
}

function countDays($year, $month, $ignore) {
    // ignore parameter: 0 is sunday, ..., 6 is saturday.
    $count = 0;
    $counter = mktime(0, 0, 0, $month, 1, $year);
    while (date("n", $counter) == $month) {
        if (in_array(date("w", $counter), $ignore) == false) {
            $count++;
        }
        $counter = strtotime("+1 day", $counter);
    }
    return $count;
}

function getLeave($data){
  echo $data;
}

function getMins($data){
  $timesplit=explode(':',$data);
  $min=($timesplit[0]*60)+($timesplit[1])+($timesplit[2]>30?1:0);
  return $min.' Min';  // 62 Min
}
