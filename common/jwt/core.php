<?php
// show error reporting
error_reporting(E_ALL);

// set your default time-zone
date_default_timezone_set('Asia/Manila');

// variables used for jwt
$key = "gigsnation2018";
$iss = "http://codingsyntax.com";
$aud = "http://codingsyntax.com";
$iat = time(); //Issued at: time when the token was generated
$nbf = $iat; // Not before
$expire = $nbf + 1 * 3600;// Expire
?>
