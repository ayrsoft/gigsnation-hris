<?php
/** Example (MySQLi Object-oriented) **/

/** Insert Data **/
$sql = "INSERT INTO MyGuests (firstname, lastname, email)
VALUES ('John', 'Doe', 'john@example.com')";

if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

/** Select Data **/
$sql = "SELECT id, firstname, lastname FROM MyGuests";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
        echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";
    }
} else {
    echo "0 results";
}

/** Delete Data **/
$sql = "DELETE FROM MyGuests WHERE id=3";

if ($conn->query($sql) === TRUE) {
    echo "Record deleted successfully";
} else {
    echo "Error deleting record: " . $conn->error;
}

/** Update Data **/
$sql = "UPDATE MyGuests SET lastname='Doe' WHERE id=2";

if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully";
} else {
    echo "Error updating record: " . $conn->error;
}

/** GET THE LAST ID OF THE DATA AFTER INSERTED **/
$sql = "INSERT INTO MyGuests (firstname, lastname, email) VALUES ('John', 'Doe', 'john@example.com')";

if ($conn->query($sql) === TRUE) {
    $last_id = $conn->insert_id;
    echo "New record created successfully. Last inserted ID is: " . $last_id;
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

$tblusers = "tbl_users";
$tbladdress = "tbl_users_address";
$tbleducation = "tbl_users_educational";
$tblcontact = "tbl_users_emergency_contact";
$column = "$tblusers.*,";
// $column. = "$tbladdress.address, $tbladdress.city, $tbladdress.provinceState, $tbladdress.country, $tbladdress.zipCode, $tbladdress.addressType";
// $column. = "$tbleducation.educationLevel, $tbleducation.startDate, $tbleducation.endDate, $tbleducation.majorSpecialization,";
$column. = "$tblcontact.contactName, $tblcontact.emergencyContactNum, $tblcontact.relationship, $tblcontact.remarks";

SELECT tbl_users.*, tbl_users_address.address FROM tbl_users
LEFT JOIN tbl_users_address
ON tbl_users_address.userID = tbl_users.userID
LEFT JOIN tbl_users_educational
ON tbl_users_educational.userID = tbl_users.userID
LEFT JOIN tbl_users_emergency_contact
ON tbl_users_emergency_contact.userID = tbl_users.userID
LEFT JOIN tbl_users_id_number
ON tbl_users_id_number.userID = tbl_users.userID
LEFT JOIN tbl_users_employment_profile
ON tbl_users_employment_profile.userID = tbl_users.userID
LEFT JOIN tbl_employee_salary
ON tbl_employee_salary.userID = tbl_users.userID


SELECT * FROM table1
LEFT JOIN table2
ON table2.email = table1.email
LEFT JOIN table3
ON table3.email = table1.email

name, employe num, email, mobile, join date, position, employment type, employment status
tbl_users,  tbl_users_employment_profile, tbl_position
