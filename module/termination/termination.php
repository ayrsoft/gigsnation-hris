<?php
include_once '../../common/common.php';
if($token != "success"){
  $status = "ERROR";
  $message = "Access Denied!";
}else{
  if($_POST['page'] == "listTermination"){
    $table1 = "tbl_employee_termination";
    $table2 = "tbl_users";
    $employeeID = $_POST['employeeID'];
		// SELECT
    $sql = "SELECT $table1.*, createdUser.*, updatedUser.* FROM $table1, (SELECT userName as createdUser FROM $table2 RIGHT JOIN $table1 ON $table2.userID = $table1.createdBy) as createdUser, (SELECT userName as updatedUser FROM $table2 RIGHT JOIN $table1 ON $table2.userID = $table1.updatedBY) as updatedUser ";
		$sql .= "WHERE $table1.employeeID ='".$employeeID."'";
		$sql .=" GROUP BY employeeTerminationID";

    if ($resultSql=mysqli_query($conn,$sql)){
			while ($row=mysqli_fetch_array($resultSql)) {
					$list[]= array(
              "employeeTerminationID" => $row['employeeTerminationID'],
							"terminationDate" => $row['terminationDate'],
							"terminationReason" => $row['terminationReason'],
							"createdBy" => $row['createdUser'],
							"createdTime" => $row['createdTime'],
							"updatedBy" => $row['updatedUser'],
							"updatedTime" => $row['updatedTime']
						);

      }
			$status = "SUCCESS";
		}else{
			//IF NO DATA FOUND
			$status = "ERROR";
		 	$message = "ERROR: " . $sql . "<br>" . $conn->error;
		}
  }else{//IF PROPER PARAMETER NOT PASSED return 404
		$status= "ERROR";
	}
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
  "message" => $message,
	"terminationListInfo" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
