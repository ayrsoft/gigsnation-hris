<?php
include_once '../../common/common.php';
if($token != "success"){
  $returndata['status'] = "ERROR";
  $returndata['message']  = "Access Denied!";
}else{
  if($_POST['page'] == "updateTermination"){

    $terminationDate = !EMPTY($_POST['terminationDate']) ? $_POST['terminationDate'] : "";
    $terminationReason = !EMPTY($_POST['terminationReason']) ? $_POST['terminationReason'] : "";
    $table1 = "tbl_employee_termination";
    $id1 = !EMPTY($_POST['employeeTerminationID']) ? $_POST['employeeTerminationID'] : "";
    $idName1 = "employeeTerminationID";
    $field1 = array("terminationDate","terminationReason","updatedBy","updatedTime");
    $data1 = array($terminationDate,$terminationReason,$currentUser,$currentTimeDate);
    $msg = "Termination successfully updated!";
    $result1 = updateData($table1,$field1,$data1,$idName1,$id1,$msg);
    $returndata = $result1;
  }
}

/********Compose Your Json Data Here*************/
createJsonData('terminationInfoUpdate', $returndata);
mysqli_close($conn);
