<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "updateAllowanceType"){

		$allowanceTypeName = !EMPTY($_POST['allowanceTypeName']) ? $_POST['allowanceTypeName'] : "";
    $allowanceTypeTaxable = !EMPTY($_POST['allowanceTypeTaxable']) ? $_POST['allowanceTypeTaxable'] : "";
    $allowanceTypeStatus = !EMPTY($_POST['allowanceTypeStatus']) ? $_POST['allowanceTypeStatus'] : "";

    $sql1="SELECT * FROM tbl_allowance_type WHERE allowanceTypeName='".$allowanceTypeName."' AND allowanceTypeStatus='".$allowanceTypeStatus."' ";
    $result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Allowance Type Already Exist";
  	}else{
      $table1 = "tbl_allowance_type";
      $id1 = $_POST['allowanceTypeID'];
      $idName1 = "allowanceTypeID";
      $field1 = array("allowanceTypeName", "allowanceTypeTaxable", "allowanceTypeStatus","updatedBy","updatedTime");
      $data1 = array($allowanceTypeName,$allowanceTypeTaxable,$allowanceTypeStatus,$currentUser,$currentTimeDate);
      $msg = "Allowance Type successfully updated!";
      $result1 = updateData($table1,$field1,$data1,$idName1,$id1,$msg);
      $returndata = $result1;
    }
  }
}

/********Compose Your Json Data Here*************/
createJsonData('allowanceTypeInfoUpdate', $returndata);
mysqli_close($conn);
