<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{

		$table = "tbl_allowance_type";
		$name = array("*");
		if($_POST['page'] == "listAllowanceType"){
			$result = showAllData($table,$name);
		}else if($_POST['page'] == "selectAllowanceType"){
			$where = "";
			$where .= "WHERE";
			$where .= (!EMPTY($_POST['allowanceTypeName']) ? " $table.allowanceTypeName ='".$_POST['allowanceTypeName']."' AND" : "");
			$where .= (!EMPTY($_POST['allowanceTypeTaxable']) ? " $table.allowanceTypeTaxable ='".$_POST['allowanceTypeTaxable']."' AND" : "");
			$where .= (!EMPTY($_POST['allowanceTypeStatus']) ? " $table.allowanceTypeStatus ='".$_POST['allowanceTypeStatus']."'" : "AND");

			$sqlWhere1 = stringEndsWith($where, "AND");
	    $sqlWhere2 = stringEndsWith($where, "WHEREAND");
	    if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
	      $where = removeLastString($where);
	    }

			$result = selectData($table,$name,$where);
		}
		while ($row=mysqli_fetch_array($result['data'])) {
				$list[]= array(
						"allowanceTypeID" => $row['allowanceTypeID'],
						"allowanceTypeName" => $row['allowanceTypeName'],
						"allowanceTypeTaxable" => $row['allowanceTypeTaxable'],
						"allowanceTypeStatus" => $row['allowanceTypeStatus'],
						"createdBy" => $row['createdBy'],
						"createdTime" => $row['createdTime'],
						"updatedBy" => $row['updatedBy'],
						"updatedTime" => $row['updatedTime']
					);
		}
		$status = $result['status'];
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"allowanceTypeList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
