<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "addAllowanceType"){

		$allowanceTypeName= !EMPTY($_POST['allowanceTypeName']) ? $_POST['allowanceTypeName'] : "";
    $allowanceTypeTaxable= !EMPTY($_POST['allowanceTypeTaxable']) ? $_POST['allowanceTypeTaxable'] : "";
    $allowanceTypeStatus = !EMPTY($_POST['allowanceTypeStatus']) ? $_POST['allowanceTypeStatus'] : "";

  	$sql1="SELECT * FROM tbl_allowance_type WHERE allowanceTypeName='".$allowanceTypeName."'";

  	$result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Allowance Type Already Exist";
  	}else{
  		$sql2 = "INSERT INTO tbl_allowance_type (allowanceTypeName, allowanceTypeTaxable, allowanceTypeStatus, createdBy, createdTime)";
  		$sql2 .= " VALUES('".$allowanceTypeName."',  '".$allowanceTypeTaxable."','".$allowanceTypeStatus."', '".$currentUser."', '".$currentTimeDate."')";

			echo $sql2;
  		if ($conn->query($sql2) === TRUE) {
  			$returndata['status'] = "SUCCESS";
        $returndata['message'] = "Allowance Type successfully added!";
  		}else{
  			 $returndata['status'] = "ERROR: " . $sql2 . "<br>" . $conn->error;
  		}
  	}

  }else{//IF PROPER PARAMETER NOT PASSED return 404
  	$returndata['status'] = "ERROR";
  }
}

/********Compose Your Json Data Here*************/
createJsonData('allowanceTypeInfo', $returndata);
mysqli_close($conn);
