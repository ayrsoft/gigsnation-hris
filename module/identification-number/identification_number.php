<?php
include_once '../../common/common.php';
if(isset($_POST['submit'])){
	$table = "tbl_identification_number";
	$name = array("*");
	$result = showAllData($table,$name);
	$departmentID = array();
	$departmentList = array();
	while ($row=mysqli_fetch_array($result['data'])) {
			$departmentList[]= array(
					"idNumberID" => $row['idNumberID'],
					"idNumberName" => $row['idNumberName']
				);
	}
	$status = $result['status'];
}else{//IF PROPER PARAMETER NOT PASSED return 404
	$status= "ERROR";
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"identificationNumberList" => $departmentList
);
echo  json_encode($arr);
mysqli_close($conn);
