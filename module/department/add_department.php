<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{
  if($_POST['page'] == "addDepartment"){

    $departmentTypeName = $_POST['departmentTypeName'];
    $departmentTypeStatus = $_POST['departmentTypeStatus'];

  	$sql1="SELECT * FROM tbl_department_type WHERE departmentTypeName='".$departmentTypeName."'";

  	$result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Department Name Already Exist";
  	}else{
  		$sql2 = "INSERT INTO tbl_department_type (departmentTypeName, departmentTypeStatus, createdBy, createdTime)";
  		$sql2 .= " VALUES('".$departmentTypeName."', '".$departmentTypeStatus."', '".$currentUser."', '".$currentTimeDate."')";
  		if ($conn->query($sql2) === TRUE) {
				$returndata['status'] = "SUCCESS";
  			$returndata['message'] = "Department name successfully added!";
  		}else{
  			 $returndata['status'] = "ERROR: " . $sql2 . "<br>" . $conn->error;
  		}
  	}

  }else{//IF PROPER PARAMETER NOT PASSED return 404
  	$returndata['status'] = "ERROR";
  }
}

/********Compose Your Json Data Here*************/
createJsonData('deparmentInfo', $returndata);
mysqli_close($conn);
