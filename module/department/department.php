<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{
	$table = "tbl_department_type";
	$name = array("*");
	if($_POST['page'] == "listDepartment"){
		$result = showAllData($table,$name);
	}else if($_POST['page'] == "searchHoliday"){
		$departmentTypeName = !EMPTY($_POST['departmentTypeName']) ? $_POST['departmentTypeName'] : "";
		$departmentTypeStatus = !EMPTY($_POST['departmentTypeStatus']) ? $_POST['departmentTypeStatus'] : "";
		$where = "";
		$where .= "WHERE";
		$where .= (!EMPTY($departmentTypeName) ? " $table.departmentTypeName ='".$departmentTypeName."' AND" : "");
		$where .= (!EMPTY($departmentTypeStatus) ? " $table.departmentTypeStatus ='".$departmentTypeStatus."'" : "AND");
		$sqlWhere1 = stringEndsWith($where, "AND");
		$sqlWhere2 = stringEndsWith($where, "WHEREAND");
		if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
			$where = removeLastString($where);
		}
		$result = selectData($table,$name,$where);
	}

	while ($row=mysqli_fetch_array($result['data'])) {
			$list[]= array(
				"departmentTypeID" => $row['departmentTypeID'],
				"departmentTypeName" =>  $row['departmentTypeName'],
				"departmentTypeStatus" =>  $row['departmentTypeStatus'],
				"createdBy" =>  $row['createdBy'],
				"createdTime" =>  $row['createdTime'],
				"updatedBy" =>  $row['updatedBy'],
				"updatedTime" =>  $row['updatedTime']
			);
	}
	$status = $result['status'];
}
/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"departmentList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
