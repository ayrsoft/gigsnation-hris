<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "basicPayUpdate"){
    $employeeID = !EMPTY($_POST['employeeID']) ? $_POST['employeeID'] : "";
    $employmentBasicSalary = !EMPTY($_POST['employmentBasicSalary']) ? $_POST['employmentBasicSalary'] : "";
    $employeeBasicSalaryAdjIDate = !EMPTY($_POST['employeeBasicSalaryAdjIDate']) ? $_POST['employeeBasicSalaryAdjIDate'] : "";
    $employeeBasicSalaryAdjReason = !EMPTY($_POST['employeeBasicSalaryAdjReason']) ? $_POST['employeeBasicSalaryAdjReason'] : "";
    $employeeBasicSalaryStatus  = "Y";
    $table1 = "tbl_employee_basic_adj";
    $id1 = !EMPTY($_POST['employeeBasicSalaryAdjID']) ? $_POST['employeeBasicSalaryAdjID'] : "";
    $idName1 = "employeeBasicSalaryAdjID";
    $field1 = array("employeeID","employmentBasicSalary","employeeBasicSalaryAdjIDate","employeeBasicSalaryAdjReason","employeeBasicSalaryStatus ","updatedBy","updatedTime");
    $data1 = array($employeeID,$employmentBasicSalary,$employeeBasicSalaryAdjIDate,$employeeBasicSalaryAdjReason,$employeeBasicSalaryStatus,$currentUser,$currentTimeDate);
    $msg = "Basic Pay successfully updated!";
    $result1 = updateData($table1,$field1,$data1,$idName1,$id1,$msg);
    $returndata = $result1;
  }
}

/********Compose Your Json Data Here*************/
createJsonData('basicPayUpdate', $returndata);
mysqli_close($conn);
