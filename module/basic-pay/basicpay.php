<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{
	if($_POST['page'] == "listbasicpay"){
		$table = "tbl_employee_basic_adj";
		$name = array("*");
		$employeeID = $_POST['employeeID'];

		$where 	= "WHERE";
		$where .= (!EMPTY($employeeID) ? " $table.employeeID ='".$employeeID."'" : "AND");

		$sqlWhere1 = stringEndsWith($where, "AND");
		$sqlWhere2 = stringEndsWith($where, "WHEREAND");
		if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
			$where = removeLastString($where);
		}

		$result = selectData($table,$name,$where);
		while ($row=mysqli_fetch_array($result['data'])) {
				$list[]= array(
						"employeeBasicSalaryAdjID" => $row['employeeBasicSalaryAdjID'],
						"employmentBasicSalary" => $row['employmentBasicSalary'],
						"employeeBasicSalaryAdjIDate" => $row['employeeBasicSalaryAdjIDate'],
						"employeeBasicSalaryAdjReason" => $row['employeeBasicSalaryAdjReason'],
						"employeeBasicSalaryStatus" => $row['employeeBasicSalaryStatus'],
						"createdBy" => $row['createdBy'],
						"createdTime" => $row['createdTime'],
						"updatedBy" => $row['updatedBy'],
						"updatedTime" => $row['updatedTime']
					);
		}
		$status = $result['status'];
	}
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"basicPayList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
