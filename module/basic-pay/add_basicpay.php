<?php
include_once '../../common/common.php';
if($token != "success"){
  $returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
  }else{
    if($_POST['page'] == "addBasicPay"){
      $employeeID = $_POST['employeeID'];
      $employmentBasicSalary = !EMPTY($_POST['employmentBasicSalary']) ? $_POST['employmentBasicSalary'] : "";
      $employeeBasicSalaryAdjIDate = !EMPTY($_POST['employeeBasicSalaryAdjIDate']) ? $_POST['employeeBasicSalaryAdjIDate'] : "";
      $employeeBasicSalaryAdjReason = !EMPTY($_POST['employeeBasicSalaryAdjReason']) ? $_POST['employeeBasicSalaryAdjReason'] : "";
      $employeeBasicSalaryStatus  = "Y";
      $table = "tbl_employee_basic_adj";
      $field = array("employeeID","employmentBasicSalary","employeeBasicSalaryAdjIDate","employeeBasicSalaryAdjReason","employeeBasicSalaryStatus ","createdBy","createdTime");
      $data = array($employeeID,$employmentBasicSalary,$employeeBasicSalaryAdjIDate,$employeeBasicSalaryAdjReason,$employeeBasicSalaryStatus,$currentUser,$currentTimeDate);
      $msg = "Basic Pay successfully Added!";
      $result = insertAllData($table,$field,$data,$msg);
      $returndata = $result;
    }
}

/********Compose Your Json Data Here*************/
createJsonData('basicPayListInfo', $returndata);
mysqli_close($conn);
