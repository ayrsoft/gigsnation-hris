<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{

		$table = "tbl_company_settings";
		$name = array("*");
		if($_POST['page'] == "listCompany"){
			$result = showAllData($table,$name);
		}
		while ($row=mysqli_fetch_array($result['data'])) {
				$list[]= array(
						"companySettingsID" => $row['companySettingsID'],
						"companyName" => $row['companyName'],
						"companyContactPerson" =>  $row['companyContactPerson'],
						"companyAddress" =>  $row['companyAddress'],
						"companyCountry" => $row['companyCountry'],
						"companyCity" => $row['companyCity'],
						"companyState" => $row['companyState'],
						"companyPostalCode" => $row['companyPostalCode'],
            "companyEmail" => $row['companyEmail'],
            "companyPhoneNumber" => $row['companyPhoneNumber'],
            "companyMobileNumber" => $row['companyMobileNumber'],
            "companyWebsite" => $row['companyWebsite'],
            "createdBy" => $row['createdBy'],
            "createdTime" => $row['createdTime'],
            "updatedBy" => $row['updatedBy'],
						"updatedTime" => $row['updatedTime']
					);
		}
		$status = $result['status'];
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"companyList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
