<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "updateCompany"){

    $companyName = !EMPTY($_POST['companyName']) ? $_POST['companyName'] : "";
    $companyContactPerson = !EMPTY($_POST['companyContactPerson']) ? $_POST['companyContactPerson'] : "";
    $companyAddress = !EMPTY($_POST['companyAddress']) ? $_POST['companyAddress'] : "";
    $companyCountry = !EMPTY($_POST['companyCountry']) ? $_POST['companyCountry'] : "";
    $companyCity = !EMPTY($_POST['companyCity']) ? $_POST['companyCity'] : "";
    $companyState = !EMPTY($_POST['companyState']) ? $_POST['companyState'] : "";
    $companyPostalCode = !EMPTY($_POST['companyPostalCode']) ? $_POST['companyPostalCode'] : "";
    $companyEmail = !EMPTY($_POST['companyEmail']) $_POST['companyEmail'] : "";
    $companyPhoneNumber = !EMPTY($_POST['companyPhoneNumber']) $_POST['companyPhoneNumber'] : "";
    $companyMobileNumber = !EMPTY($_POST['companyMobileNumber']) $_POST['companyMobileNumber'] : "";
    $companyFaxNumber = !EMPTY($_POST['companyFaxNumber']) $_POST['companyFaxNumber'] : "";
    $companyWebsite = !EMPTY($_POST['companyWebsite']) $_POST['companyWebsite'] : "";

    $table1 = "tbl_company_settings";
    $id1 = "1";
    $idName1 = "companySettingsID";
    $field1 = array("companyName", "companyContactPerson", "companyAddress", "companyCountry", "companyCity", "companyState", "companyPostalCode", "companyEmail", "companyPhoneNumber", "companyMobileNumber", "companyFaxNumber", "companyWebsite", "updatedBy","updatedTime");
    $data1 = array($companyName,$companyContactPerson,$companyAddress,$companyCountry,$companyCity,$companyState,$companyPostalCode,$companyEmail,$companyPhoneNumber,$companyMobileNumber,$companyFaxNumber,$companyWebsite,$currentUser,$currentTimeDate);
    $msg = "Company info Successfully updated!";
    $result1 = updateData($table1,$field1,$data1,$idName1,$id1,$msg);
    $returndata = $result1;
  }
}

/********Compose Your Json Data Here*************/
createJsonData('companyInfoUpdate', $returndata);
mysqli_close($conn);
