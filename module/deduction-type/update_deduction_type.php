<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "updateDeductionType"){

    $deductionTypeName= !EMPTY($_POST['deductionTypeName']) ? $_POST['deductionTypeName'] : "";
    $deductionTypeStatus = !EMPTY($_POST['deductionTypeStatus']) ? $_POST['deductionTypeStatus'] : "";

    $sql1="SELECT * FROM tbl_deduction_type WHERE deductionTypeName='".$deductionTypeName."' AND deductionTypeStatus='".$deductionTypeStatus."' ";
    $result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Deduction Type Already Exist";
  	}else{
      $table1 = "tbl_deduction_type";
      $id1 = !EMPTY($_POST['deductionTypeID']) ? $_POST['deductionTypeID'] : "";
      $idName1 = "deductionTypeID";
      $field1 = array("deductionTypeName", "deductionTypeStatus","updatedBy","updatedTime");
      $data1 = array($deductionTypeName,$deductionTypeStatus,$currentUser,$currentTimeDate);
      $msg = "Deduction Type successfully updated!";
      $result1 = updateData($table1,$field1,$data1,$idName1,$id1,$msg);
      $returndata = $result1;
    }
  }
}

/********Compose Your Json Data Here*************/
createJsonData('deductionTypeInfoUpdate', $returndata);
mysqli_close($conn);
