<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "addEmploymentType"){

    $deductionTypeName= $_POST['deductionTypeName'];
    $deductionTypeStatus = $_POST['deductionTypeStatus'];

  	$sql1="SELECT * FROM tbl_deduction_type WHERE deductionTypeName='".$deductionTypeName."'";

  	$result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Deduction Type Already Exist";
  	}else{
  		$sql2 = "INSERT INTO tbl_deduction_type (deductionTypeName, deductionTypeStatus, createdBy, createdTime)";
  		$sql2 .= " VALUES('".$deductionTypeName."', '".$deductionTypeStatus."', '".$currentUser."', '".$currentTimeDate."')";
  		if ($conn->query($sql2) === TRUE) {
  			$returndata['status'] = "SUCCESS";
        $returndata['message'] = "Deduction Type successfully added!";
  		}else{
  			 $returndata['status'] = "ERROR: " . $sql2 . "<br>" . $conn->error;
  		}
  	}

  }else{//IF PROPER PARAMETER NOT PASSED return 404
  	$returndata['status'] = "ERROR";
  }
}

/********Compose Your Json Data Here*************/
createJsonData('deductionTypeInfo', $returndata);
mysqli_close($conn);
