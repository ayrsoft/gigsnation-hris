<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{

		$table = "tbl_deduction_type";
		$name = array("*");
		if($_POST['page'] == "listDeductionType"){
			$result = showAllData($table,$name);
		}else if($_POST['page'] == "selectDeductionType"){
			$deductionTypeName = $_POST['deductionTypeName'];
			$deductionTypeStatus = $_POST['deductionTypeStatus'];
			$where	= "";
			$where .= "WHERE";
			$where .= (!EMPTY($deductionTypeName) ? " $table.deductionTypeName ='".$deductionTypeName."' AND" : "");
			$where .= (!EMPTY($deductionTypeStatus) ? " $table.deductionTypeStatus ='".$deductionTypeStatus."'" : "AND");

			$sqlWhere1 = stringEndsWith($where, "AND");
	    $sqlWhere2 = stringEndsWith($where, "WHEREAND");
	    if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
	      $where = removeLastString($where);
	    }

			$result = selectData($table,$name,$where);
		}
		while ($row=mysqli_fetch_array($result['data'])) {
				$list[]= array(
						"deductionTypeID" => $row['deductionTypeID'],
						"deductionTypeName" => $row['deductionTypeName'],
						"deductionTypeStatus" => $row['deductionTypeStatus'],
						"createdBy" => $row['createdBy'],
						"createdTime" => $row['createdTime'],
						"updatedBy" => $row['updatedBy'],
						"updatedTime" => $row['updatedTime']
					);
		}
		$status = $result['status'];
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"deductionTypeList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
