<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "updateSalarySetting"){

    $salarySettingName= !EMPTY($_POST['salarySettingName']) ? $_POST['salarySettingName'] : "";
    $salarySettingValueRate = !EMPTY($_POST['salarySettingValueRate']) ? $_POST['salarySettingValueRate'] : "";

    $sql1="SELECT * FROM tbl_salary_settings WHERE salarySettingName='".$salarySettingName."' AND salarySettingValueRate='".$salarySettingValueRate."' ";
    $result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Salary Setting Already Exist";
  	}else{
      $table1 = "tbl_salary_settings";
      $id1 = !EMPTY($_POST['salarySettingID']) ? $_POST['salarySettingID'] : "";
      $idName1 = "salarySettingID";
      $field1 = array("salarySettingName", "salarySettingValueRate","updatedBy","updatedTime");
      $data1 = array($salarySettingName,$salarySettingValueRate,$currentUser,$currentTimeDate);
      $msg = "Salary Setting successfully updated!";
      $result1 = updateData($table1,$field1,$data1,$idName1,$id1,$msg);
      $returndata = $result1;
    }
  }
}

/********Compose Your Json Data Here*************/
createJsonData('salarySettingInfoUpdate', $returndata);
mysqli_close($conn);
