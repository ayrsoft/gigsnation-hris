<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "addSalarySetting"){

    $salarySettingName= !EMPTY($_POST['salarySettingName']) ? $_POST['salarySettingName'] : "";
    $salarySettingValueRate = !EMPTY($_POST['salarySettingValueRate']) ? $_POST['salarySettingValueRate'] : "";

  	$sql1="SELECT * FROM tbl_salary_settings WHERE salarySettingName='".$salarySettingName."'";

  	$result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Salary Setting Already Exist";
  	}else{
  		$sql2 = "INSERT INTO tbl_salary_settings (salarySettingName, salarySettingValueRate, createdBy, createdTime)";
  		$sql2 .= " VALUES('".$salarySettingName."', '".$salarySettingValueRate."', '".$currentUser."', '".$currentTimeDate."')";
  		if ($conn->query($sql2) === TRUE) {
  			$returndata['status'] = "SUCCESS";
        $returndata['message'] = "Salary Setting successfully added!";
  		}else{
  			 $returndata['status'] = "ERROR: " . $sql2 . "<br>" . $conn->error;
  		}
  	}

  }else{//IF PROPER PARAMETER NOT PASSED return 404
  	$returndata['status'] = "ERROR";
  }
}

/********Compose Your Json Data Here*************/
createJsonData('salarySettingInfo', $returndata);
mysqli_close($conn);
