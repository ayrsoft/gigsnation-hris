<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{

		$table = "tbl_salary_settings";
		$name = array("*");
		if($_POST['page'] == "listSalarySetting"){
			$result = showAllData($table,$name);
			auditLogs("View", "View Salary Settings");
		}else if($_POST['page'] == "selectSalarySetting"){
			$salarySettingName = !EMPTY($_POST['salarySettingName']);
			$salarySettingValueRate = !EMPTY($_POST['salarySettingValueRate']);

			$where = "";
			$where .= "WHERE";
			$where .= (!EMPTY($salarySettingName) ? " $table.salarySettingName ='".$salarySettingName."' AND" : "");
			$where .= (!EMPTY($salarySettingValueRate) ? " $table.salarySettingValueRate ='".$salarySettingValueRate."'" : "AND");

			$sqlWhere1 = stringEndsWith($where, "AND");
	    $sqlWhere2 = stringEndsWith($where, "WHEREAND");
	    if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
	      $where = removeLastString($where);
	    }

			$result = selectData($table,$name,$where);
			auditLogs("Search", "Search Salary Settings");
		}
		while ($row=mysqli_fetch_array($result['data'])) {
				$list[]= array(
						"salarySettingID" => $row['salarySettingID'],
						"salarySettingName" => $row['salarySettingName'],
						"salarySettingValueRate" => $row['salarySettingValueRate'],
						"createdBy" => $row['createdBy'],
						"createdTime" => $row['createdTime'],
						"updatedBy" => $row['updatedBy'],
						"updatedTime" => $row['updatedTime']
					);
		}

		$status = $result['status'];
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"salarySettingList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
