<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
	$employeeID = $_POST['employeeID'];
	$table1 = "tbl_attendance";
	$table2 = "tbl_holiday_type";
	$column = "*";
	$attendanceList = array();

	if($_POST['page'] == "listAttendance"){
		$sql = "SELECT $column FROM $table1 LEFT JOIN $table2 ON $table2.holidayTypeID = $table1.holidayTypeID WHERE $table1.employeeID = '".$employeeID."'";
		if ($resultSql=mysqli_query($conn,$sql)){
			while ($row=mysqli_fetch_array($resultSql)) {
			$attendanceList[]= array(
					"attendanceID" => $row['attendanceID'],
					"attendanceDate" =>  $row['attendanceDate'],
					"timeIn" =>  $row['timeIn'],
					"timeOut" => $row['timeOut'],
					"overtime" => $row['overtime'],
					"overtimeStatus" => $row['overtimeStatus'],
					"holidayTypeID" => $row['holidayTypeID'],
					"holidayName" => $row['holidayName'],
					"createdBy" => $row['createdBy'],
					"createdTime" => $row['createdTime'],
					"updatedBy" => $row['updatedBy'],
					"updatedTime" => $row['updatedTime']
				);
			}
		}
		$status = "SUCCESS";
	}else{
		$status = "ERROR";
	}
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"attendanceList" => $attendanceList
);
echo  json_encode($arr);
mysqli_close($conn);
