<?php
include_once '../../common/common.php';
require_once('../../vendor/import-excel/php-excel-reader/excel_reader2.php');
require_once('../../vendor/import-excel/SpreadsheetReader.php');

if (isset($_POST["import"]))
{

    $targetPath = 'uploads/'.$_FILES['file']['name'];
    move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);

    $Reader = new SpreadsheetReader($targetPath);

    $sheetCount = count($Reader->sheets());
    for($i=0;$i<$sheetCount;$i++)
    {

        $Reader->ChangeSheet($i);

        foreach ($Reader as $Row)
        {
              $userID = "";
              $timeIn = "";
              $timeOut = "";
              $overtime = getOvertime($Row[3], $Row[4]);
              $overtimeStatus = "2";
              $holidayID = "";
              $sql1 = "SELECT holidayID, holidayType FROM tbl_holidays WHERE holidayDate = '".$Row[2]."'";
              $result1=mysqli_query($conn,$sql1);
              $rowcount1=mysqli_num_rows($result1);
              if ($rowcount1 > 0) {
                  while ($row1=mysqli_fetch_array($result1)) {
                       $holidayID = $row1['holidayID'];
                  }
              }
              $sql2 = "SELECT userID FROM tbl_users WHERE empID = '".$Row[1]."'";
              $result2=mysqli_query($conn,$sql2);

              while ($row2=mysqli_fetch_array($result2)) {
                   $userID = $row2['userID'];;
              }
            if(isset($Row[2])) { $attendanceDate = mysqli_real_escape_string($conn,$Row[2]); }
            if(isset($Row[3])) { $timeIn = mysqli_real_escape_string($conn,$Row[3]); }
            if(isset($Row[4])) { $timeOut = mysqli_real_escape_string($conn,$Row[4]); }

            if (!empty($userID) || !empty($attendanceDate) || !empty($timeIn) || !empty($timeOut) || !empty($overtime) || !empty($overtimeStatus) || !empty($holidayID)) {
                $query = "insert into tbl_attendance(userID, attendanceDate, timeIn, timeout, overtime, overtimeStatus, holidayID)";
                $query .= "values('".$userID."','".$attendanceDate."','".$timeIn."','".$timeOut."','".$overtime."','".$overtimeStatus."','".$holidayID."')";
                $result = mysqli_query($conn, $query);

                if (!empty($result)) {
                    $returndata['status'] = "SUCCESS";
                    $returndata['message'] = "Excel Data Imported into the Database";
                } else {
                    $returndata['status'] = "ERROR";
                    $returndata['message'] = "Problem in Importing Excel Data";
                }
            }
         }

     }
}
createJsonData('attendanceInfo', $returndata);
mysqli_close($conn);
