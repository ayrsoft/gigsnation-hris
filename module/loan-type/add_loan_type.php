<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "addLoanType"){

    $loanTypeName= !EMPTY($_POST['loanTypeName']) ? $_POST['loanTypeName'] : "";
    $loanTypeStatus = !EMPTY($_POST['loanTypeStatus']) ? $_POST['loanTypeStatus'] : "";

  	$sql1="SELECT * FROM tbl_loan_type WHERE loanTypeName='".$loanTypeName."'";

  	$result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Loan Type Already Exist";
  	}else{
  		$sql2 = "INSERT INTO tbl_loan_type (loanTypeName, loanTypeStatus, createdBy, createdTime)";
  		$sql2 .= " VALUES('".$loanTypeName."', '".$loanTypeStatus."', '".$currentUser."', '".$currentTimeDate."')";
  		if ($conn->query($sql2) === TRUE) {
  			$returndata['status'] = "SUCCESS";
        $returndata['message'] = "Loan Type successfully added!";
  		}else{
  			 $returndata['status'] = "ERROR: " . $sql2 . "<br>" . $conn->error;
  		}
  	}

  }else{//IF PROPER PARAMETER NOT PASSED return 404
  	$returndata['status'] = "ERROR";
  }
}

/********Compose Your Json Data Here*************/
createJsonData('loanTypeInfo', $returndata);
mysqli_close($conn);
