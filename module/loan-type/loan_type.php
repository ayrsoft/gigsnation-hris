<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{

		$table = "tbl_loan_type";
		$name = array("*");
		if($_POST['page'] == "listLoanType"){
			$result = showAllData($table,$name);
		}else if($_POST['page'] == "selectLoanType"){
			$loanTypeName = !EMPTY($_POST['loanTypeName']) ? $_POST['loanTypeName'] : "";
			$loanTypeStatus = !EMPTY($_POST['loanTypeStatus']) ? $_POST['loanTypeStatus'] : "";

			$where = "";
			$where .= "WHERE";
			$where .= (!EMPTY($loanTypeName) ? " $table.loanTypeName ='".$loanTypeName."' AND" : "");
			$where .= (!EMPTY($loanTypeStatus) ? " $table.loanTypeStatus ='".$loanTypeStatus."'" : "AND");

			$sqlWhere1 = stringEndsWith($where, "AND");
	    $sqlWhere2 = stringEndsWith($where, "WHEREAND");
	    if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
	      $where = removeLastString($where);
	    }

			$result = selectData($table,$name,$where);
		}
		while ($row=mysqli_fetch_array($result['data'])) {
				$list[]= array(
						"loanTypeID" => $row['loanTypeID'],
						"loanTypeName" => $row['loanTypeName'],
						"loanTypeStatus" => $row['loanTypeStatus'],
						"createdBy" => $row['createdBy'],
						"createdTime" => $row['createdTime'],
						"updatedBy" => $row['updatedBy'],
						"updatedTime" => $row['updatedTime']
					);
		}
		$status = $result['status'];
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"loanTypeList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
