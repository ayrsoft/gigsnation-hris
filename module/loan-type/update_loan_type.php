<?php
include_once '../../common/common.php';
if($token != "success"){
  $returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "updateLoanType"){

    $loanTypeName = !EMPTY($_POST['loanTypeName']) ? $_POST['loanTypeName'] : "";
    $loanTypeStatus = !EMPTY($_POST['loanTypeStatus']) ? $_POST['loanTypeStatus'] : "";

    $sql1="SELECT * FROM tbl_loan_type WHERE loanTypeName='".$loanTypeName."' AND loanTypeStatus='".$loanTypeStatus."' ";
    $result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Loan Type Already Exist";
  	}else{
      $table1 = "tbl_loan_type";
      $id1 = isset($_POST['loanTypeID']);
      $idName1 = "loanTypeID";
      $field1 = array("loanTypeName", "loanTypeStatus","updatedBy","updatedTime");
      $data1 = array($loanTypeName,$loanTypeStatus,$currentUser,$currentTimeDate);
      $msg = "Loan Type successfully updated!";
      $result1 = updateData($table1,$field1,$data1,$idName1,$id1,$msg);
      $returndata = $result1;
    }
  }
}

/********Compose Your Json Data Here*************/
createJsonData('loanTypeInfoUpdate', $returndata);
mysqli_close($conn);
