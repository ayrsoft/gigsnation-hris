<?php
include_once '../../common/common.php';
$generalInfo = array();
$educationInfo = array();
$addressInfo = array();
$employmentInfo = array();
$idNumberInfo = array();

if($token != "success"){
  $status= "ERROR";
  $message = "Access Denied!";
}else if($_POST['page'] == "employeeProfile"){
  $employeeID = $_POST['employeeID'];
	$table1 = "tbl_employee";
	$table2 = "tbl_employee_employment";
	$table3 = "tbl_position_type";
	$table4 = "tbl_employment_type";
  $table5 = "tbl_department_type";
  $table7 = "tbl_employee_educational";
  $table8 = "tbl_employee_gov";
  $table10 = "tbl_employee_address";
  $table11 = "tbl_employee_emergency_contact";

	$column = "*";

	$sql = "SELECT $column FROM $table1 LEFT JOIN $table2 ON $table2.employeeID = $table1.employeeID";
  $sql .= " LEFT JOIN $table3 ON $table3.positionTypeID = $table2.positionTypeID";
  $sql .= " LEFT JOIN $table4 ON $table4.employmentTypeID = $table2.employmentTypeID";
  $sql .= " LEFT JOIN $table5 ON $table5.departmentTypeID = $table2.departmentTypeID";
  $sql .= " LEFT JOIN $table11 ON $table11.employeeID = $table1.employeeID";
  $sql .= " WHERE $table1.employeeID = $employeeID";

  $sql1 = "SELECT $column FROM $table7 WHERE employeeID = $employeeID";
  $sql2 = "SELECT $column FROM $table8 WHERE employeeID = $employeeID";
  $sql3 = "SELECT $column FROM $table10 WHERE employeeID = $employeeID";



  if ($resultSql1=mysqli_query($conn,$sql1)){
    while ($row1=mysqli_fetch_array($resultSql1)) {
      $educationInfo[]= array(
        "educationalID" => $row1['educationalID'],
        "educationalLevelName" => $row1['educationalLevelName'],
        "educationalStartDate" => $row1['educationalStartDate'],
        "educationalEndDate" => $row1['educationalEndDate'],
        "educationalMajorSpecialization" => $row1['educationalMajorSpecialization']
      );
    }
  }
  if ($resultSql2=mysqli_query($conn,$sql2)){
    while ($row2=mysqli_fetch_array($resultSql2)) {
      $idNumberInfo[]= array(
        "employeeGovID" => $row2['employeeGovID'],
        "governmentType" => $row2['governmentType'],
        "governmentNumber" => $row2['governmentNumber'],
        "governmentStatus" => $row2['governmentStatus']
      );
    }
  }
  if ($resultSql3=mysqli_query($conn,$sql3)){
    while ($row3=mysqli_fetch_array($resultSql3)) {
      $addressInfo[]= array(
        "employeeAddress" => $row3['employeeAddress'],
        "employeeCity" => $row3['employeeCity'],
        "employeeProvince" => $row3['employeeProvince'],
        "employeeCountry" => $row3['employeeCountry'],
        "employeeZipcode" => $row3['employeeZipcode'],
        "employeeAddressType" => $row3['employeeAddressType']
      );
    }
  }
	if ($resultSql=mysqli_query($conn,$sql)){
		while ($row=mysqli_fetch_array($resultSql)) {
				$generalInfo[]= array(
						"employeeID" => $employeeID, //TABLE tbl_employee
						"firstName" => $row['firstName'],
						"lastName" => $row['lastName'],
						"middleName" => $row['middleName'],
						"extName" => $row['extName'],
						"email" => $row['email'],
            "contactNumber" => $row['contactNumber'],
            "birthday" => $row['birthday'],
            "gender" => $row['gender'],
            "civilStatus" => $row['civilStatus'],
            "nationality" => $row['nationality'],
            "emergencyContactName" => $row['emergencyContactName'], //TABLE tbl_employee_emergency_contact
            "emergencyContactNum" => $row['emergencyContactNum'],
            "emergencyRelationship" => $row['emergencyRelationship'],
            "emergencyRemarks" => $row['emergencyRemarks'],

					);
        $employmentInfo[]=array(
          "departmentTypeID" => $row['departmentTypeID'], //TABLE tbl_employee_employment
          "departmentTypeName" => $row['departmentTypeName'], //TABLE tbl_department_type
          "positionTypeID" => $row['positionTypeID'], //TABLE tbl_employee_employment
          "positionTypeName" => $row['positionTypeName'], //TABLE tbl_position_type
          "positionTypeSalary" => $row['positionTypeSalary'],
          "employmentOriginalDateHIred" => $row['employmentOriginalDateHIred'],
          "employmentStartDate" => $row['employmentStartDate'],
          "employmentTypeID" => $row['employmentTypeID'],
          "employmentTypeName" => $row['employmentTypeName'], //TABLE tbl_employment_type
          "employmentPaymentType" => $row['employmentPaymentType'], //TABLE tbl_employee_employment
          "employmentStatus" => $row['employmentStatus'],
          "employmentPaymentGroup" => $row['employmentPaymentGroup']
        );
		}
		$status = "SUCCESS";
	}else{
	 	$status = "ERROR: " . $sql . "<br>" . $conn->error;
	}
}else{
  $status = "ERROR";
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
  "message" => $message,
  "employeeListInfo" => $generalInfo,
  "addressListInfo" => $addressInfo,
  "educationListInfo" => $educationInfo,
  "govNumberListInfo" => $idNumberInfo,
  "employmentListInfo" => $employmentInfo,
);
echo  json_encode($arr);
mysqli_close($conn);
