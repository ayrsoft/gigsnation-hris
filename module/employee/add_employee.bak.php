<?php
include_once '../../common/common.php';
if(isset($_POST['submit'])){

  $currentUserID = $_POST['currentUserID'];

  $empID = $_POST['empID'];
  $firstName = $_POST['firstName'];
  $lastName = $_POST['lastName'];
  $middleName = $_POST['middleName'];
  $extName = $_POST['extName'];
  $birthday = $_POST['birthday'];
  $gender = $_POST['gender'];
  $civilStatus = $_POST['civilStatus'];
  $nationality= $_POST['nationality'];
  $email = $_POST['email'];
  $contactNumber = $_POST['contactNumber'];

  $address1 = $_POST['address1'];
  $city1 = $_POST['city1'];
  $provinceState1 = $_POST['provinceState1'];
  $country1 = $_POST['country1'];
  $zipCode1 = $_POST['zipCode1'];
  $addressType1 = $_POST['addressType1'];

  if($_POST['residentailAddress'] == "true"){
    $address2 = $_POST['address2'];
    $city2 = $_POST['city2'];
    $provinceState2 = $_POST['provinceState2'];
    $country2 = $_POST['country2'];
    $zipCode2 = $_POST['zipCode2'];
    $addressType2 = $_POST['addressType2'];
  }

  $contactName = $_POST['contactName'];
  $emergencyContactNum = $_POST['emergencyContactNum'];
  $relationship = $_POST['relationship'];
  $remarks = $_POST['remarks'];

  $departmentID = $_POST['departmentID'];
  $positionID = $_POST['positionID'];
  $employmentOriginalDateHired = $_POST['employmentOriginalDateHired'];
  $employmentStartDate = $_POST['employmentStartDate'];
  $employmentStatus = $_POST['employmentStatus'];
  $employmentTypeID = $_POST['employmentTypeID'];
  $paymentType = $_POST['paymentType'];

  $employeeMonthlySalary = $_POST['employeeMonthlySalary'];

  $fnameExpolode = explode(" ", $firstName);
  $userName = strtolower($fnameExpolode[0].".".$lastName);
  $userPassword = sha1("12345");

  $sql1 = "INSERT INTO tbl_users (empID, userName, userPassword, firstName, lastName, middleName, extName, birthday, gender, civilStatus, nationality, email, contactNumber)";
  $sql1 .= " VALUES('".$empID."', '".$userName."', '".$userPassword."', '".$firstName."', '".$lastName."', '".$middleName."', '".$extName."', '".$birthday."',  '".$gender."', '".$civilStatus."', '".$nationality."', '".$email."', '".$contactNumber."')";


  if ($conn->query($sql1) === TRUE) {
     $lastID = $conn->insert_id;

     $sql2 = "INSERT INTO tbl_users_address (userID, address, city, provinceState, country, zipCode, addressType)";
     $sql2 .= " VALUES('".$lastID."', '".$address1."', '".$city1."', '".$provinceState1."', '".$country1."', '".$zipCode1."', '".$addressType1."')";
     mysqli_query($conn, $sql2);

     if($_POST['residentailAddress'] == "true"){
       $sql3 = "INSERT INTO tbl_users_address (userID, address, city, provinceState, country, zipCode, addressType)";
       $sql3 .= " VALUES('".$lastID."', '".$address2."', '".$city2."', '".$provinceState2."', '".$country2."', '".$zipCode2."', '".$addressType2."')";
       mysqli_query($conn, $sql3);
     }

     $sql4 = "INSERT INTO tbl_users_emergency_contact (userID, contactName, emergencyContactNum, relationship, remarks)";
     $sql4 .=" VALUES('".$lastID."', '".$contactName."', '".$emergencyContactNum."', '".$relationship."', '".$remarks."')";
     // mysqli_query($conn, $sql4);
     $userIDNumber = $_POST['userIDNumber'];
     $userIDType = $_POST['userIDType'];
     $deductionStatus = $_POST['deductionStatus'];

     $sql5 = 'INSERT INTO tbl_users_id_number (userID, idNumber, idType, deductionStatus) VALUES';
      foreach( $userIDNumber as $row => $key ){
          $sql5 .= "( '".$lastID."', '".$userIDNumber[$row]."', '".$userIDType[$row]."', '".$deductionStatus[$row]."' ),";
      }
     $sql5 = rtrim( $sql5, ',');
     mysqli_query($conn, $sql5);

     $educationLevel = $_POST['educationLevel'];
     $startDate = $_POST['startDate'];
     $endDate = $_POST['endDate'];
     $majorSpecialization = $_POST['majorSpecialization'];

     $sql6 = 'INSERT INTO tbl_users_educational (userID, educationLevel, startDate, endDate, majorSpecialization) VALUES';
      foreach( $educationLevel as $row => $key ){
          $sql6 .= "( '".$lastID."', '".$educationLevel[$row]."', '".$startDate[$row]."', '".$endDate[$row]."', '".$majorSpecialization[$row]."' ),";
      }
     $sql6 = rtrim( $sql6, ',');
     mysqli_query($conn, $sql6);
     $returndata['sql6'] =$sql6;

     $sql7 = "INSERT INTO tbl_users_employment_profile (userID, departmentID, positionID, employmentOriginalDateHired, employmentStartDate, employmentStatus, employmentTypeID, paymentType)";
     $sql7 .=" VALUES('".$lastID."', '".$departmentID."', '".$positionID."', '".$employmentOriginalDateHired."', '".$employmentStartDate."', '".$employmentStatus."', '".$employmentTypeID."', '".$paymentType."')";
     mysqli_query($conn, $sql7);


     $positionSalaryID = "1"; //tbl_poistion_salary
     $sql8 = "INSERT INTO tbl_employee_salary (userID, employeeMonthlySalary, positionSalaryID, salaryStatus, updatedBy, updatedAt)";
     $sql8 .=" VALUES('".$lastID."', '".$employeeMonthlySalary."', '".$positionSalaryID."', '1', '".$currentUserID."', '".$currentTimeDate."')";
     mysqli_query($conn, $sql8);

     $returndata['status'] = "SUCCESS";
   }else{
     $returndata['status'] = "ERROR: " . $sql1 . "<br>" . $conn->error;
   }

}else{//IF PROPER PARAMETER NOT PASSED return 404
	$returndata['status'] = "ERROR";
}

/********Compose Your Json Data Here*************/
createJsonData('employeeInfo', $returndata);
mysqli_close($conn);
