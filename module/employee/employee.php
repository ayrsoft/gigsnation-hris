<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{
	if($_POST['page'] == "listEmployee" || $_POST['page'] == "selectEmployee"){
	$table1 = "tbl_employee";
	$table2 = "tbl_employee_employment";
	$table3 = "tbl_position_type";
	$employeeID = !EMPTY($_POST['employeeID']) ? $_POST['employeeID'] : "";
		$firstName = !EMPTY($_POST['firstName']) ? $_POST['firstName'] : "";
		$lastName = !EMPTY($_POST['lastName']) ? $_POST['lastName'] : "";
		$positionTypeID = !EMPTY($_POST['positionTypeID']) ?  $_POST['positionTypeID']: "";
		$employmentStatus = !EMPTY($_POST['employmentStatus']) ?  $_POST['employmentStatus']: "";

		$column = "$table1.employeeID, $table1.firstName, $table1.lastName, $table1.middleName, $table1.extName, $table1.email, $table1.contactNumber, ";
		$column .= "$table2.employmentStartDate, $table2.employmentStatus, ";
		$column .= "$table3.positionTypeName";
		$sql = "SELECT $column FROM $table1 LEFT JOIN $table2 ON $table2.employeeID = $table1.employeeID LEFT JOIN $table3 ON $table3.positionTypeID = $table2.positionTypeID ";
		$sql .= "WHERE";
		$sql .= (!EMPTY($employeeID) ? " $table1.employeeID ='".$employeeID."' AND" : "");
		$sql .= (!EMPTY($firstName) ? " $table1.firstName ='".$firstName."' AND" : "");
		$sql .= (!EMPTY($lastName) ? " $table1.lastName ='".$lastName."' AND" : "");
		// $sql .= (!EMPTY($employmentStatus) ? " $table2.employmentStatus ='".$employmentStatus."' AND" : "");
		$sql .= (!EMPTY($positionTypeID) ? " $table2.positionTypeID ='".$positionTypeID."'" : "AND");

		$sqlWhere1 = stringEndsWith($sql, "AND");
    $sqlWhere2 = stringEndsWith($sql, "WHEREAND");
    if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
      $sql = removeLastString($sql);
    }

		if ($resultSql=mysqli_query($conn,$sql)){
			while ($row=mysqli_fetch_array($resultSql)) {
					$list[]= array(
							"employeeID" => $row['employeeID'],
							"firstName" => $row['firstName'],
							"lastName" => $row['lastName'],
							"middleName" => $row['middleName'],
							"extName" => $row['extName'],
							"email" => $row['email'],
							"contactNumber" => $row['contactNumber'],
							"positionTypeName" => $row['positionTypeName'],
							"employmentStartDate" => $row['employmentStartDate'],
							"employmentStatus" => $row['employmentStatus'],
						);
			}
			$status = "SUCCESS";

		}else{
			//IF NO DATA FOUND
			$status = "ERROR";
		 	$message = "ERROR: " . $sql . "<br>" . $conn->error;
		}
	}else{//IF PROPER PARAMETER NOT PASSED return 404
		$status= "ERROR";
	}
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"employeeList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
