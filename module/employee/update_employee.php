<?php
include_once '../../common/common.php';
if($token != "success"){
  $returndata['status']= "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
    if($_POST['page'] == "updateEmployee"){
    $employeeID = !EMPTY($_POST['employeeID']) ? $_POST['employeeID'] : "";
    $firstName = !EMPTY($_POST['firstName']) ? $_POST['firstName'] : "";
    $lastName = !EMPTY($_POST['lastName']) ? $_POST['lastName'] : "";
    $middleName = !EMPTY($_POST['middleName']) ? $_POST['middleName'] : "";
    $extName = !EMPTY($_POST['extName']) ? $_POST['extName'] : "";
    $birthday = !EMPTY($_POST['birthday']) ? $_POST['birthday'] : "";
    $gender = !EMPTY($_POST['gender']) ? $_POST['gender'] : "";
    $civilStatus = !EMPTY($_POST['civilStatus']) ? $_POST['civilStatus'] : "";
    $nationality= !EMPTY($_POST['nationality']) ? $_POST['nationality'] : "";
    $email = !EMPTY($_POST['email']) ? $_POST['email'] : "";
    $contactNumber = !EMPTY($_POST['contactNumber']) ? $_POST['contactNumber'] : "";
    $userLevel = !EMPTY($_POST['userLevel']) ? $_POST['userLevel'] : "";

    $address1 = !EMPTY($_POST['address1']) ? $_POST['address1'] : "";
    $city1 = !EMPTY($_POST['city1']) ? $_POST['city1'] : "";
    $provinceState1 = !EMPTY($_POST['provinceState1']) ? $_POST['provinceState1'] : "";
    $country1 = !EMPTY($_POST['country1']) ? $_POST['country1'] : "";
    $zipCode1 = !EMPTY($_POST['zipCode1']) ? $_POST['zipCode1'] : "";
    $addressType1 = !EMPTY($_POST['employeeAddressType1']) ? $_POST['employeeAddressType1'] : "";

    if(isset($_POST['residentailAddress']) == "true"){
      $address2 = !EMPTY($_POST['address2']) ? $_POST['address2'] : "";
      $city2 = !EMPTY($_POST['city2']) ? $_POST['city2'] : "";
      $provinceState2 = !EMPTY($_POST['provinceState2']) ? $_POST['provinceState2'] : "";
      $country2 = !EMPTY($_POST['country2']) ? $_POST['country2'] : "";
      $zipCode2 = !EMPTY($_POST['zipCode2']) ? $_POST['zipCode2'] : "";
      $addressType2 = !EMPTY($_POST['employeeAddressType2']) ? $_POST['employeeAddressType2'] : "";
    }

    $emergencyContactName = !EMPTY($_POST['emergencyContactName']) ? $_POST['emergencyContactName'] : "";
    $emergencyContactNum = !EMPTY($_POST['emergencyContactNum']) ? $_POST['emergencyContactNum'] : "";
    $emergencyRelationship = !EMPTY($_POST['emergencyRelationship']) ? $_POST['emergencyRelationship'] : "";
    $emergencyRemarks = !EMPTY($_POST['emergencyRemarks']) ? $_POST['emergencyRemarks'] : "";

    $departmentTypeID = !EMPTY($_POST['departmentTypeID']) ? $_POST['departmentTypeID'] : "";
    $positionTypeID = !EMPTY($_POST['positionTypeID']) ? $_POST['positionTypeID'] : "";
    $employmentOriginalDateHIred = !EMPTY($_POST['employmentOriginalDateHIred']) ? $_POST['employmentOriginalDateHIred'] : "";
    $employmentStartDate = !EMPTY($_POST['employmentStartDate']) ? $_POST['employmentStartDate'] : "";
    $employmentTypeID = !EMPTY($_POST['employmentTypeID']) ? $_POST['employmentTypeID'] : "";
    $employmentPaymentGroup = !EMPTY($_POST['employmentPaymentGroup']) ? $_POST['employmentPaymentGroup'] : "";
    $employmentPaymentType = !EMPTY($_POST['employmentPaymentType']) ? $_POST['employmentPaymentType'] : "";
    $employmentStatus = !EMPTY($_POST['employmentStatus']) ? $_POST['employmentStatus'] : "";

    //TABLE tbl_employee_basic_adj
    $employmentBasicSalary = !EMPTY($_POST['employmentBasicSalary']) ? $_POST['employmentBasicSalary'] : "";
    $employeeBasicSalaryAdjIDate = $currentDate;
    $employeeBasicSalaryAdjReason = "Initial Salary";
    $employeeBasicSalaryStatus = "Y";

    $educationalLevelName = !EMPTY($_POST['educationalLevelName']) ? $_POST['educationalLevelName'] : "";
    $educationalStartDate = !EMPTY($_POST['educationalStartDate']) ? $_POST['educationalStartDate'] : "";
    $educationalEndDate = !EMPTY($_POST['educationalEndDate']) ? $_POST['educationalEndDate'] : "";
    $educationalMajorSpecialization = !EMPTY($_POST['educationalMajorSpecialization']) ?  $_POST['educationalMajorSpecialization']: "";

    $fnameExpolode = explode(" ", $firstName);
    $userName = strtolower($fnameExpolode[0].".".$lastName);
    $userPassword = sha1("12345");

    $id = $_POST['employeeID'];
    $idName = "employeeID";

    $sql1="SELECT userName FROM tbl_users WHERE userName='".$userName."' AND employeeID='".$employeeID."' AND userStatus='".$employmentStatus."'";
    $result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Employee Already Existed!";
  	}else{
      $table8 = "tbl_users";
      $field8 = array("employeeID","userName","userPassword","userLevel","userStatus","createdBy","createdTime");
      $data8 = array($employeeID,$userName,$userPassword,$userLevel,$employmentStatus,$currentUser,$currentTimeDate);
      $result8 = updateData($table8,$field8,$data8,$idName,$id,"");

      $table1 = "tbl_employee";
      $field1 = array("firstName","lastName","middleName","extName","birthday","gender","civilStatus","nationality","email","contactNumber");
      $data1 = array($firstName,$lastName,$middleName,$extName,$birthday,$gender,$civilStatus,$nationality,$email,$contactNumber);
      $result1 = updateData($table1,$field1,$data1,$idName,$id,"");

      $table2 = "tbl_employee_address";
      $field2 = array("employeeID","employeeAddress","employeeCity","employeeProvince","employeeCountry","employeeZipcode","employeeAddressType");
      $data2  = array($employeeID,$address1,$city1,$provinceState1,$country1,$zipCode1,$addressType1);
      $result2 = updateData($table2,$field2,$data2,$idName,$id,"");

      if(!EMPTY($_POST['residentailAddress']) == "true"){
        $data3 = array($employeeID,$address2,$city2,$provinceState2,$country2,$zipCode2,$addressType2);
        $result3 = updateData($table2,$field2,$data3,$idName,$id,"");
      }

      $table4 = "tbl_employee_emergency_contact";
      $field4 = array("employeeID","emergencyContactName","emergencyContactNum","emergencyRelationship","emergencyRemarks");
      $data4  = array($employeeID,$emergencyContactName,$emergencyContactNum,$emergencyRelationship,$emergencyRemarks);
      $result4 = updateData($table4,$field4,$data4,$idName,$id,"");

      $table5 = "tbl_employee_employment";
      $field5 = array("employeeID","departmentTypeID","positionTypeID","employmentOriginalDateHIred","employmentStartDate","employmentTypeID","employmentPaymentGroup","employmentPaymentType","employmentStatus");
      $data5  = array($employeeID,$departmentTypeID,$positionTypeID,$employmentOriginalDateHIred,$employmentStartDate,$employmentTypeID,$employmentPaymentGroup,$employmentPaymentType,$employmentStatus);
      $result5 = updateData($table5,$field5,$data5,$idName,$id,"");

      if($employmentStatus == "N"){
        $terminationReason = !EMPTY($_POST['terminationReason']) ? $_POST['terminationReason'] : "";
        $table6 = " tbl_employee_termination";
        $field6 = array("employeeID","	terminationDate","terminationReason","createdBy","createdTime");
        $data6 = array($employeeID,$currentDate,$terminationReason,$currentUser,$currentTimeDate);
        $result = insertAllData($table6,$field6,$data6,"");
      }

       foreach( $educationalLevelName as $row => $key ){
         $sql6 = 'UPDATE tbl_employee_educational SET ';
         $sql6 .="employeeID=".$employeeID.",";
         $sql6 .="educationalLevelName=".$educationalLevelName[$row].",";
         $sql6 .="educationalStartDate=".$educationalStartDate[$row].",";
         $sql6 .="educationalEndDate=".$educationalEndDate[$row].",";
         $sql6 .="educationalMajorSpecialization=".$educationalMajorSpecialization[$row].",";
         $sql6 = rtrim( $sql6, ',');
         $sql6 .=" WHERE educationalID='".$educationalID[$row]."'; ";
         mysqli_query($conn, $sql6);
       }

      $employeeGovID = !EMPTY($_POST['employeeGovID']) ? $_POST['employeeGovID'] : "";
      $governmentType = !EMPTY($_POST['governmentType']) ? $_POST['governmentType'] : "";
      $governmentNumber = !EMPTY($_POST['governmentNumber']) ? $_POST['governmentNumber'] : "";
      $governmentStatus = !EMPTY($_POST['governmentStatus']) ? $_POST['governmentStatus'] : "";

       foreach( $governmentType as $row => $key ){
          $sql7 = 'UPDATE tbl_employee_gov SET ';
          $sql7 .="employeeID=".$employeeID.",";
          $sql7 .="governmentType=".$governmentType[$row].",";
          $sql7 .="governmentNumber='".$governmentNumber[$row]."',";
          $sql7 .="governmentStatus=".$governmentStatus[$row].",";
          $sql7 = rtrim( $sql7, ',');
          $sql7 .=" WHERE employeeGovID='".$employeeGovID[$row]."'; ";
          mysqli_query($conn, $sql7);
       }

      $returndata['status'] = "SUCCESS";
      $returndata['message'] = "Employee Info Updated!";

    }
  }
}

/********Compose Your Json Data Here*************/
createJsonData('employeeInfoUpdate', $returndata);
mysqli_close($conn);
