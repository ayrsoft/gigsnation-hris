<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "addEmploymentType"){

    $employmentTypeName= $_POST['employmentTypeName'];
    $employmentTypeStatus = $_POST['employmentTypeStatus'];

  	$sql1="SELECT * FROM tbl_employment_type WHERE employmentTypeName='".$employmentTypeName."'";

  	$result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Employment Type Already Exist";
  	}else{
  		$sql2 = "INSERT INTO tbl_employment_type (employmentTypeName, employmentTypeStatus, createdBy, createdTime)";
  		$sql2 .= " VALUES('".$employmentTypeName."', '".$employmentTypeStatus."', '".$currentUser."', '".$currentTimeDate."')";
  		if ($conn->query($sql2) === TRUE) {
  			$returndata['status'] = "SUCCESS";
        $returndata['message'] = "Employment Type successfully added!";
  		}else{
  			 $returndata['status'] = "ERROR: " . $sql2 . "<br>" . $conn->error;
  		}
  	}

  }else{//IF PROPER PARAMETER NOT PASSED return 404
  	$returndata['status'] = "ERROR";
  }
}

/********Compose Your Json Data Here*************/
createJsonData('employmentTypeInfo', $returndata);
mysqli_close($conn);
