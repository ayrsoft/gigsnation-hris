<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{

		$table = "tbl_employment_type";
		$name = array("*");
		if($_POST['page'] == "listEmploymentType"){
			$result = showAllData($table,$name);
		}else if($_POST['page'] == "selectEmploymentType"){
			$employmentTypeName = !EMPTY($_POST['employmentTypeName']) ? $_POST['employmentTypeName'] : "";
			$employmentTypeStatus = !EMPTY($_POST['employmentTypeStatus']) ? $_POST['employmentTypeStatus'] : "";
			$where = "";
			$where .= "WHERE";
			$where .= (!EMPTY($employmentTypeName) ? " $table.employmentTypeName ='".$employmentTypeName."' AND" : "");
			$where .= (!EMPTY($employmentTypeStatus) ? " $table.employmentTypeStatus ='".$employmentTypeStatus."'" : "AND");

			$sqlWhere1 = stringEndsWith($where, "AND");
	    $sqlWhere2 = stringEndsWith($where, "WHEREAND");
	    if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
	      $where = removeLastString($where);
	    }

			$result = selectData($table,$name,$where);
		}
		while ($row=mysqli_fetch_array($result['data'])) {
				$list[]= array(
						"employmentTypeID" => $row['employmentTypeID'],
						"employmentTypeName" => $row['employmentTypeName'],
						"employmentTypeStatus" => $row['employmentTypeStatus'],
						"createdBy" => $row['createdBy'],
						"createdTime" => $row['createdTime'],
						"updatedBy" => $row['updatedBy'],
						"updatedTime" => $row['updatedTime']
					);
		}
		$status = $result['status'];
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"employmentTypeList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
