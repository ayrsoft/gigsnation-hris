<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "updateEmploymentType"){

    $employmentTypeName= !EMPTY($_POST['employmentTypeName']) ? $_POST['employmentTypeName'] : "";
    $employmentTypeStatus = !EMPTY($_POST['employmentTypeStatus']) $_POST['employmentTypeStatus'] : "";

    $sql1="SELECT * FROM tbl_employment_type WHERE employmentTypeName='".$employmentTypeName."' AND employmentTypeStatus='".$employmentTypeStatus."' ";
    $result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Employment Type Already Exist";
  	}else{
      $table1 = "tbl_employment_type";
      $id1 = !EMPTY($_POST['employmentTypeID']) ? $_POST['employmentTypeID'] : "";
      $idName1 = "employmentTypeID";
      $field1 = array("employmentTypeName", "employmentTypeStatus","updatedBy","updatedTime");
      $data1 = array($employmentTypeName,$employmentTypeStatus,$currentUser,$currentTimeDate);
      $msg = "Employment Type successfully updated!";
      $result1 = updateData($table1,$field1,$data1,$idName1,$id1,$msg);
      $returndata = $result1;
    }
  }
}

/********Compose Your Json Data Here*************/
createJsonData('employmentTypeInfoUpdate', $returndata);
mysqli_close($conn);
