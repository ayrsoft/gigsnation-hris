<?php
include_once '../../common/common.php';
if($token != "success"){
  $returndata['status'] = "ERROR";
  $returndata['message']  = "Access Denied!";
}else{
  if($_POST['page'] == "leaveUpdate"){
    $employeeLeaveStatus = !EMPTY($_POST['employeeLeaveStatus']) ? $_POST['employeeLeaveStatus'] : "";
    $employeeID = $_POST['employeeID'];
    $leaveTypeID = !EMPTY($_POST['leaveTypeID']) ? $_POST['leaveTypeID'] : "";
    $employeeLeaveApplyDate = !EMPTY($_POST['employeeLeaveApplyDate']) ? $_POST['employeeLeaveApplyDate'] : "";
    $employeeLeaveStartDate = !EMPTY($_POST['employeeLeaveStartDate']) ? $_POST['employeeLeaveStartDate'] : "";
    $employeeLeaveEndDate = !EMPTY($_POST['employeeLeaveEndDate']) ? $_POST['employeeLeaveEndDate'] : "";
    $employeeLeaveRemarks = !EMPTY($_POST['employeeLeaveRemarks']) ? $_POST['employeeLeaveRemarks'] : "";
    $table1 = "tbl_employee_leave";
    $id1 = !EMPTY($_POST['leaveID']) ? $_POST['leaveID'] : "";
    $idName1 = "leaveID";
    $field1 = array("employeeID","leaveTypeID","employeeLeaveApplyDate","employeeLeaveStartDate","employeeLeaveEndDate","employeeLeaveRemarks","employeeLeaveStatus","updatedBy","updatedTime");
    $data1 = array($employeeID,$leaveTypeID,$employeeLeaveApplyDate,$employeeLeaveStartDate,$employeeLeaveEndDate,$employeeLeaveRemarks,$employeeLeaveStatus,$currentUser,$currentTimeDate);
    $msg = "Leave successfully updated!";
    $result1 = updateData($table1,$field1,$data1,$idName1,$id1,$msg);
    $returndata = $result1;
  }
}

/********Compose Your Json Data Here*************/
createJsonData('leaveInfoUpdate', $returndata);
mysqli_close($conn);
