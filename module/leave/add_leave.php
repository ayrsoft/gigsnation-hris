<?php
include_once '../../common/common.php';
if($token != "success"){
  $returndata['status'] = "ERROR";
  $returndata['message']  = "Access Denied!";
  }else{
    if($_POST['page'] == "addLeave"){
      $employeeID = !EMPTY($_POST['employeeID']) ? $_POST['employeeID'] : "";
      $leaveTypeID = !EMPTY($_POST['leaveTypeID']) ? $_POST['leaveTypeID'] : "";
      $employeeLeaveApplyDate = !EMPTY($_POST['employeeLeaveApplyDate']) ? $_POST['employeeLeaveApplyDate'] : "";
      $employeeLeaveStartDate = !EMPTY($_POST['employeeLeaveStartDate']) ? $_POST['employeeLeaveStartDate'] : "";
      $employeeLeaveEndDate = !EMPTY($_POST['employeeLeaveEndDate']) ? $_POST['employeeLeaveEndDate'] : "";
      $employeeLeaveRemarks = !EMPTY($_POST['employeeLeaveRemarks']) ? $_POST['employeeLeaveRemarks'] : "";
      $table = "tbl_employee_leave";
      $field = array("employeeID","leaveTypeID","employeeLeaveApplyDate","employeeLeaveStartDate","employeeLeaveEndDate","employeeLeaveRemarks","employeeLeaveStatus","createdBy","createdTime");
      $data = array($employeeID,$leaveTypeID,$employeeLeaveApplyDate,$employeeLeaveStartDate,$employeeLeaveEndDate,$employeeLeaveRemarks,1,$currentUser,$currentTimeDate);
      $msg = "Request Successfully Sent!";
      $result = insertAllData($table,$field,$data,$msg);
      $returndata = $result;
    }
}
/********Compose Your Json Data Here*************/
createJsonData('leaveListInfo', $returndata);
mysqli_close($conn);
