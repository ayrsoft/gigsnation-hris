<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{
	if($_POST['page'] == "listLeave"){
	$table1 = "tbl_employee_leave";
	$table2 = "tbl_leave_type";
	$table3 = "tbl_employee";
	$employeeID = isset($_POST['employeeID']);
  $firstName = isset($_POST['firstName']);
  $lastName = isset($_POST['lastName']);
  $leaveTypeID = isset($_POST['leaveTypeID']);
  $employeeLeaveStatus = isset($_POST['employeeLeaveStatus']);
  $employeeLeaveStartDate = isset($_POST['employeeLeaveStartDate']);
  $employeeLeaveEndDate = isset($_POST['employeeLeaveEndDate']);

		$column = "$table1.*, ";
		$column .= "$table2.leaveTypeID, $table2.leaveTypeName, ";
    $column .= "$table3.firstName, $table3.lastName, $table3.middleName, $table3.extName";

		$sql = "SELECT $column FROM $table1 LEFT JOIN $table2 ON $table2.leaveTypeID = $table1.leaveTypeID LEFT JOIN $table3 ON $table3.employeeID = $table1.employeeID ";
		$sql .= "WHERE";
		$sql .= (!EMPTY($employeeID) ? " $table1.employeeID ='".$employeeID."' AND" : "");
    $sql .= (!EMPTY($firstName) ? " $table3.firstName ='".$firstName."' AND" : "");
    $sql .= (!EMPTY($lastName) ? " $table3.lastName ='".$lastName."' AND" : "");
    $sql .= (!EMPTY($employeeLeaveStatus) ? " $table1.employeeLeaveStatus ='".$employeeLeaveStatus."' AND" : "");
    $sql .= (!EMPTY($employeeLeaveStartDate) ? " $table1.employeeLeaveStartDate ='".$employeeLeaveStartDate."' AND" : "");
		$sql .= (!EMPTY($employeeLeaveEndDate) ? " $table1.employeeLeaveEndDate ='".$employeeLeaveEndDate."' AND": "");
		$sql .= (!EMPTY($leaveTypeID) ? " $table1.leaveTypeID ='".$leaveTypeID."'" : "AND");

    $sqlWhere1 = stringEndsWith($sql, "AND");
    $sqlWhere2 = stringEndsWith($sql, "WHEREAND");
    if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
      $sql = removeLastString($sql);
    }

		if ($resultSql=mysqli_query($conn,$sql)){
			while ($row=mysqli_fetch_array($resultSql)) {
					$list[]= array(
              "leaveID" => $row['leaveID'],
							"employeeID" => $row['employeeID'],
							"firstName" => $row['firstName'],
							"lastName" => $row['lastName'],
							"middleName" => $row['middleName'],
							"extName" => $row['extName'],
							"leaveTypeID" => $row['leaveTypeID'],
							"leaveTypeName" => $row['leaveTypeName'],
							"employeeLeaveApplyDate" => $row['employeeLeaveApplyDate'],
							"employeeLeaveStartDate" => $row['employeeLeaveStartDate'],
							"employeeLeaveEndDate" => $row['employeeLeaveEndDate'],
              "employeeLeaveRemarks" => $row['employeeLeaveRemarks'],
              "employeeLeaveStatus" => $row['employeeLeaveStatus'],
              "createdBy" => $row['createdBy'],
              "createdTime" => $row['createdTime'],
              "updatedBy" => $row['updatedBy'],
              "updatedTime" => $row['updatedTime']
						);
			}
			$status = "SUCCESS";
		}else{
			//IF NO DATA FOUND
			$status = "ERROR";
		 	$message = "ERROR: " . $sql . "<br>" . $conn->error;
		}
	}else{//IF PROPER PARAMETER NOT PASSED return 404
		$status= "ERROR";
	}
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
  "message" => $message,
	"leaveListInfo" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
