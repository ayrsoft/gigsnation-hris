<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{
	if($_POST['page'] == "listDeduction"){
		$table1 = "tbl_employee_deduction";
		$table2 = "tbl_deduction_type";
		$name = array("*");
		$employeeID = $_POST['employeeID'];

		$sql = "SELECT * FROM $table1 LEFT JOIN $table2 ON $table2.deductionTypeID = $table1.deductionTypeID";
		$sql .= " WHERE";
		$sql .= (!EMPTY($employeeID) ? " $table1.employeeID='".$employeeID."' AND" : "");

		$sqlWhere1 = stringEndsWith($sql, "AND");
    $sqlWhere2 = stringEndsWith($sql, "WHEREAND");
    if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
      $sql = removeLastString($sql);
    }

		if ($resultSql=mysqli_query($conn,$sql)){
			while ($row=mysqli_fetch_array($resultSql)) {
					$list[]= array(
							"employeeDeductionID" => $row['employeeDeductionID'],
							"deductionTypeID" => $row['deductionTypeID'],
							"deductionTypeName" => $row['deductionTypeName'],
							"deductionAmount" => $row['deductionAmount'],
							"deductionValidFrom" => $row['deductionValidFrom'],
							"deductionValidTo" => $row['deductionValidTo'],
							"deductionStatus" => $row['deductionStatus'],
							"createdBy" => $row['createdBy'],
							"createdTime" => $row['createdTime'],
							"updatedBy" => $row['updatedBy'],
							"updatedTime" => $row['updatedTime']
						);
			}
			$status = "SUCCESS";
		}else{
			$status = "ERROR";
			$message = "ERROR: " . $sql . "<br>" . $conn->error;
		}
	}
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"deductionList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
