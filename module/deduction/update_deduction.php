<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "updateDeduction"){
    $employeeID = $_POST['employeeID'];
    $deductionTypeID = !EMPTY($_POST['deductionTypeID']) ? $_POST['deductionTypeID'] : "";
    $deductionAmount = !EMPTY($_POST['deductionAmount']) ? $_POST['deductionAmount'] : "";
    $deductionValidFrom = !EMPTY($_POST['deductionValidFrom']) ? $_POST['deductionValidFrom'] : "";
    $deductionValidTo = !EMPTY($_POST['deductionValidTo']) ? $_POST['deductionValidTo'] : "";
    $deductionStatus  = !EMPTY($_POST['deductionStatus']) ? $_POST['deductionStatus'] : "";
    $table1 = "tbl_employee_deduction";
    $id1 = !EMPTY($_POST['employeeDeductionID']) ? $_POST['employeeDeductionID'] : "";
    $idName1 = "employeeDeductionID";
    $field1 = array("employeeID","deductionTypeID","deductionAmount","deductionValidFrom","deductionValidTo","deductionStatus","updatedBy","updatedTime");
    $data1 = array($employeeID,$deductionTypeID,$deductionAmount,$deductionValidFrom,$deductionValidTo,$deductionStatus,$currentUser,$currentTimeDate);
    $msg = "Deduction successfully updated!";
    $result1 = updateData($table1,$field1,$data1,$idName1,$id1,$msg);
    $returndata = $result1;
  }
}

/********Compose Your Json Data Here*************/
createJsonData('basicPayUpdate', $returndata);
mysqli_close($conn);
