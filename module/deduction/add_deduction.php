<?php
include_once '../../common/common.php';
if($token != "success"){
  $returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
  }else{
    if($_POST['page'] == "addDeduction"){
      $employeeID = $_POST['employeeID'];
      $deductionTypeID = !EMPTY($_POST['deductionTypeID']) ? $_POST['deductionTypeID'] : "";
      $deductionAmount = !EMPTY($_POST['deductionAmount']) ? $_POST['deductionAmount'] : "";
      $deductionValidFrom = !EMPTY($_POST['deductionValidFrom']) ? $_POST['deductionValidFrom'] : "";
      $deductionValidTo = !EMPTY($_POST['deductionValidTo']) ? $_POST['deductionValidTo'] : "";
      $deductionStatus  = !EMPTY($_POST['deductionStatus']) ? $_POST['deductionStatus'] : "";
      $table = "tbl_employee_deduction";
      $field = array("employeeID","deductionTypeID","deductionAmount","deductionValidFrom","deductionValidTo","deductionStatus","createdBy","createdTime");
      $data = array($employeeID,$deductionTypeID,$deductionAmount,$deductionValidFrom,$deductionValidTo,$deductionStatus,$currentUser,$currentTimeDate);
      $msg = "Deduction Already Added!";
      $result = insertAllData($table,$field,$data,$msg);
      $returndata = $result;
    }
}

/********Compose Your Json Data Here*************/
createJsonData('basicPayListInfo', $returndata);
mysqli_close($conn);
