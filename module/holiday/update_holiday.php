<?php
include_once '../../common/common.php';
if($token != "success"){
  $returndata['status']= "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "holidayUpdate"){

    $holidayName = !EMPTY($_POST['holidayName']) ? $_POST['holidayName'] : "";
    $holidayDate = !EMPTY($_POST['holidayDate']) ? $_POST['holidayDate'] : "";
    $holidayType = !EMPTY($_POST['holidayType']) ? $_POST['holidayType'] : "";
    $holidayDay = getDay(!EMPTY($_POST['holidayDate']) ? $_POST['holidayDate'] : "");

    $sql1="SELECT * FROM tbl_holiday_type WHERE holidayDate='".$holidayDate."' AND holidayName='".$holidayName."' ";
    $result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Holiday Schedule Already Exist";
  	}else{
      $table1 = "tbl_holiday_type";
      $id1 = !EMPTY($_POST['holidayTypeID']);
      $idName1 = "holidayTypeID";
      $field1 = array("holidayName", "holidayDate", "holidayDay", "holidayType", "updatedBy","updatedTime");
      $data1 = array($holidayName,$holidayDate,$holidayDay,$holidayType,$currentUser,$currentTimeDate);
      $msg = "Holiday already updated!";
      $result1 = updateData($table1,$field1,$data1,$idName1,$id1,$msg);
      $returndata = $result1;
    }
  }
}

/********Compose Your Json Data Here*************/
createJsonData('holidayInfoUpdate', $returndata);
mysqli_close($conn);
