<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{

		$table = "tbl_holiday_type";
		$name = array("*");
		if($_POST['page'] == "listHoliday"){
			$result = showAllData($table,$name);
		}else if($_POST['page'] == "searchHoliday"){
			$holidayName = !EMPTY($_POST['holidayName']) ? $_POST['holidayName'] : "";
			$holidayType = !EMPTY($_POST['holidayType']) ? $_POST['holidayType'] : "";
			$holidayDay = !EMPTY($_POST['holidayDay']) ? $_POST['holidayDay'] : "";
			$where = " WHERE";
			$where .= (!EMPTY($holidayName) ? " $table.holidayName ='".$holidayName."' AND" : "");
			$where .= (!EMPTY($holidayType) ? " $table.holidayType ='".$holidayType."' AND" : "");
			$where .= (!EMPTY($holidayDay) ? " $table.holidayDay ='".$holidayDay."'" : "AND");

			$sqlWhere1 = stringEndsWith($where, "AND");
	    $sqlWhere2 = stringEndsWith($where, "WHEREAND");
	    if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
	      $where = removeLastString($where);
	    }

			$result = selectData($table,$name,$where);
		}
		while ($row=mysqli_fetch_array($result['data'])) {
				$list[]= array(
						"holidayTypeID" => $row['holidayTypeID'],
						"holidayName" => $row['holidayName'],
						"holidayDate" =>  $row['holidayDate'],
						"holidayDay" =>  $row['holidayDay'],
						"holidayType" => $row['holidayType'],
						"createdBy" => $row['createdBy'],
						"createdTime" => $row['createdTime'],
						"updatedBy" => $row['updatedBy'],
						"updatedTime" => $row['updatedTime']
					);
		}
		$status = $result['status'];
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"holidayList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
