<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status']= "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "addHoliday"){

    $holidayName = !EMPTY($_POST['holidayName']) ? $_POST['holidayName'] : "";
    $holidayDate = !EMPTY($_POST['holidayDate']) ? $_POST['holidayDate'] : "";
    $holidayDay = getDay(!EMPTY($_POST['holidayDate']) ? $_POST['holidayDate'] : "");
  	$holidayType = !EMPTY($_POST['holidayType']) ? $_POST['holidayType'] : "";

  	$sql1="SELECT * FROM tbl_holiday_type WHERE holidayDate='".$holidayDate."' AND holidayName='".$holidayName."' ";

  	$result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Holiday Schedule Already Exist";
  	}else{
  		$sql2 = "INSERT INTO tbl_holiday_type (holidayName, holidayDate, holidayDay, holidayType, createdBy, createdTime)";
  		$sql2 .= " VALUES('".$holidayName."', '".$holidayDate."', '".$holidayDay."', '".$holidayType."', '".$currentUser."', '".$currentTimeDate."')";
  		if ($conn->query($sql2) === TRUE) {
  			$returndata['status'] = "SUCCESS";
        $returndata['message'] = "Holiday name successfully added!";
  		}else{
  			 $returndata['status'] = "ERROR: " . $sql2 . "<br>" . $conn->error;
  		}
  	}

  }else{//IF PROPER PARAMETER NOT PASSED return 404
  	$returndata['status'] = "ERROR";
  }
}

/********Compose Your Json Data Here*************/
createJsonData('holidayInfo', $returndata);
mysqli_close($conn);
