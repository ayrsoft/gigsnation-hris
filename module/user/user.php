<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{
	if($_POST['page'] == "listUser" || $_POST['page'] == "selectUser"){
	$table1 = "tbl_users";
	$table2 = "tbl_employee";

		$column = "$table1.*, ";
		$column .= "$table2.firstName, $table2.lastName, $table2.middleName, $table2.extName, $table2.email, $table2.contactNumber";
		$sql = "SELECT $column FROM $table1 LEFT JOIN $table2 ON $table2.employeeID = $table1.employeeID";
		$sql .= " WHERE";
		$sql .= (!EMPTY($_POST['firstName']) ? " $table2.firstName ='".$_POST['firstName']."' AND" : "");
    $sql .= (!EMPTY($_POST['lastName']) ? " $table2.lastName ='".$_POST['lastName']."' AND" : "");
		$sql .= (!EMPTY($_POST['userName']) ? " $table1.userName ='".$_POST['userName']."' AND" : "");
		$sql .= (!EMPTY($_POST['userLevel']) ? " $table1.userLevel ='".$_POST['userLevel']."'" : "AND");

		$sqlWhere1 = stringEndsWith($sql, "AND");
    $sqlWhere2 = stringEndsWith($sql, "WHEREAND");
    if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
      $sql = removeLastString($sql);
    }

		if ($resultSql=mysqli_query($conn,$sql)){
			while ($row=mysqli_fetch_array($resultSql)) {
					$list[]= array(
							"firstName" => $row['firstName'],
							"lastName" => $row['lastName'],
							"middleName" => $row['middleName'],
							"extName" => $row['extName'],
							"email" => $row['email'],
							"contactNumber" => $row['contactNumber'],
              "userLevel" => $row['userLevel'],
              "userStatus" => $row['userStatus']
						);
			}
			$status = "SUCCESS";
		}else{
			//IF NO DATA FOUND
		 	$status = "ERROR: " . $sql . "<br>" . $conn->error;
		}
	}else{//IF PROPER PARAMETER NOT PASSED return 404
		$status= "ERROR";
	}
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"userList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
