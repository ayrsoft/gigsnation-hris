<?php
include_once '../../common/common.php';

		$table1 = "tbl_attendance";
		$table2 = "tbl_employee";
		$table3 = "tbl_employee_employment";
    $table4 = "tbl_employee_basic_adj";
		$table5 = "tbl_employee_gov";
    $table6 = "tbl_holiday_type";

		$dateFrom = date('Y-m-01');
		$dateTo = date('Y-m-t');

		if( isset($_POST['month']) && isset($_POST['year']) ){
			$dateFrom = date($_POST['year'].'-'.$_POST['month'].'-01');
			$dateTo = date($_POST['year'].'-'.$_POST['month'].'-t');
			// $absent = countDays($_POST['year'], $_POST['month'], array(0, 6));
		}
		$column = "$table1.*, $table2.firstName, $table2.lastName, $table2.middleName, $table2.extName, count($table1.attendanceID) as 'daysOFWork', $table4.employmentBasicSalary, ";
		$column .= "SEC_TO_TIME(SUM(TIME_TO_SEC(IF(WEEKDAY(attendanceDate), IF(TIMEDIFF(timeIn,'08:00:00.000000') > '00:00:00.000000', TIMEDIFF(timeIn,'08:00:00.000000'),'00:00:00.000000'), IF(TIMEDIFF(timeIn,'07:00:00.000000') > '00:00:00.000000', TIMEDIFF(timeIn,'07:00:00.000000'),'00:00:00.000000'))))) as late, ";
    $column .= "IF($table1.overtimeStatus = '2', SUM($table1.overtime), '') as overtime, ";
    $column .= "GROUP_CONCAT(DISTINCT($table5.governmentType) SEPARATOR ', ') as employeeGov, ";
		$column .= "SUM(IF($table6.holidayType = '1', 1, 0)) as regularHoliday, ";
		$column .= "SUM(IF($table6.holidayType = '2', 1, 0)) as specialHoliday ";
		// $column .= "COUNT($table1.holidayTypeID) as holidayCount";

		$sql = "SELECT $column FROM $table1 LEFT JOIN $table2 ON $table2.employeeID = $table1.employeeID";
		$sql .= " LEFT JOIN $table3 on $table2.employeeID = $table3.employeeID";
    $sql .= " LEFT JOIN $table4 on $table2.employeeID = $table4.employeeID";
    $sql .= " LEFT JOIN $table5 on $table1.employeeID = $table5.employeeID";
		$sql .=" LEFT JOIN $table6 on $table1.holidayTypeID = $table6.holidayTypeID";
		$sql .= " WHERE $table3.employmentStatus = 'Y' AND $table4.employeeBasicSalaryStatus = 'Y' AND $table5.governmentStatus = 'Y'";
		// $sql .= " attendanceDate between '".$dateFrom."' and '".$dateTo."' AND";
		// $sql .= (!EMPTY($_POST['firstName']) ? " $table2.firstName ='".$_POST['firstName']."' AND" : "");
		// $sql .= (!EMPTY($_POST['lastName']) ? " $table2.lastName ='".$_POST['lastName']."' AND" : "");
		// $sql .= (!EMPTY($_POST['employeeID']) ? " $table1.employeeID ='".$_POST['employeeID']."'" : "AND");
		$sqlWhere1 = stringEndsWith($sql, "AND");
    $sqlWhere2 = stringEndsWith($sql, "WHEREAND");
    if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
      $sql = removeLastString($sql);
    }
		$sql .= " GROUP BY employeeID";
		// echo $sql;

		if ($resultSql=mysqli_query($conn,$sql)){
			while ($row=mysqli_fetch_array($resultSql)) {
          $basicSalary = $row['employmentBasicSalary'];
          $dailyRate = $basicSalary / 26;
          $hrRate = $dailyRate / 8;
          $minRate = $hrRate / 60;

					//TAX COMPUTATION
					$annualSalary = $basicSalary * 12;
					//TAX COMPUTATION
					$bracket2 = (($annualSalary-250000)*.2)/12;
					$bracket3 = (($annualSalary-400000)*.25+30000)/12;
					$bracket4 = (($annualSalary-800000)*.3+130000)/12;
					$bracket5 = (($annualSalary-2000000)*.32+490000)/12;
					$bracket6 = (($annualSalary-8000000)*.35+2410000)/12;
					$tax = 0;
					if($annualSalary<=250000){
						$tax=0;
					}else if(($annualSalary>250000)AND($annualSalary<400000)){
						$tax=$bracket2;
					}else if(($annualSalary>400000)AND($annualSalary<800000)){
						$tax=$bracket3;
					}else if(($annualSalary>800000)AND($annualSalary<2000000)){
						$tax=$bracket4;
					}else if(($annualSalary>2000000)AND($annualSalary<8000000)){
						$tax=$bracket5;
					}else if($annualSalary>8000000){
						$tax=$bracket6;
					}else{
						$tax=0;
					}

					//HDMF = 1, TIN = 2, SSS = 3, PHILHEALTH = 4
					$govType = explode(",",$row['employeeGov']);
					$countGov = count($govType);
					$philhealth = in_array("4",$govType);
					$sss = in_array("3",$govType);
					$tin = in_array("2",$govType);
					$hdmf = in_array("1",$govType);

          //GET LATE DEDUCTION
          $late = $row['late'];
          $getLateInMins = getMins($late);
          $totalLateDeduction = $getLateInMins * $minRate;

          //GET THE TOTAL ABSENT\
					$workdays = $row['daysOFWork'] / $countGov;
          $numberAbsence = WORKDAYS - $workdays;
          $totalAbsentDeduction = $numberAbsence * $dailyRate;

          //GET OVERTIME RATE
          $totalOvertimeRate = getMins($row['overtime']) * $minRate;

					//HOLIDAYS
					$regularHoliday = $row['regularHoliday'] / $countGov;
					$specialHoliday = $row['specialHoliday'] / $countGov;
					$regularHolidayTotalRate = $regularHoliday * $dailyRate;
					$specialHolidayTotalRate = ($dailyRate * $specialHoliday ) * .30;
          // $sss = ($govType[2] == 2) ? $row['employeeGov'] : "";

					$list[]= array(
							"employeeID" => $row['employeeID'],
							"firstName" => $row['firstName'],
							"lastName" => $row['lastName'],
							"middleName" => $row['middleName'],
              "extName" => $row['extName'],
							"employmentBasicSalary" => $basicSalary,
              "dailyRate" => $dailyRate,
              "hrsRate" => $hrRate,
              "minRate" => $minRate,
              "daysOFWork" => $workdays,
							"totalAbsent" => $numberAbsence,
              "totalAbsentDeduction" => $totalAbsentDeduction,
              "totalLate" => $late,
							"totalLateInMins" => $getLateInMins,
              "totalLateDeduction" => $totalLateDeduction,
              "overtime" => $row['overtime'],
              "totalOvertimeRate" => $totalOvertimeRate,
							"government" => $row['employeeGov'],
              "countGov" => $countGov,
              "HDMF" => ($hdmf != false) ? HDMF : "N/A",
              "TAX" => ($tin != false) ? $tax : "N/A",
              "SSS" => ($sss != false) ? "100" : "N/A",
              "PHILHEALTH" => ($philhealth != false) ? "100" : "N/A",
							"regularHoliday" => $regularHoliday,
							"regularHolidayTotalRate" => $regularHolidayTotalRate,
							"specialHoliday" => $specialHoliday,
							"specialHolidayTotalRate" => $specialHolidayTotalRate,
							// "holidayCount" => $row['holidayCount']
						);
			}
			$status = "SUCCESS";
		}else{
			//IF NO DATA FOUND
		 	$status = "ERROR: " . $sql . "<br>" . $conn->error;
		}


/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"timeAttendanceList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
