<?php
include_once '../../common/common.php';
if(isset($_POST['submit'])){
  $dateFrom = !EMPTY($_POST['dateFrom']) ? $_POST['dateFrom'] : "";
  $dateTo = !EMPTY($_POST['dateTo']) ? $_POST['dateTo'] : "";
	$table1 = "tbl_attendance";
  $table2 = "tbl_holidays";
	$table3 = "tbl_users";
  $table4 = "tbl_employee_salary";
  $table5 = "tbl_position_salary";

	$date1=date_create($dateFrom);
	$date2=date_create($dateTo);
	$dateDiff=date_diff($date1,$date2);
	$dateNumberDiff = $dateDiff->format("%a");

	$condition = "attendanceDate between '".$dateFrom."' and '".$dateTo."'";
	$sql1 = "SELECT * FROM $table1 WHERE $condition";
		if ($resultSql1=mysqli_query($conn,$sql1)){
				while ($row1=mysqli_fetch_array($resultSql1)) {
					$day = getDay($row1['attendanceDate']);
					$getTimeOffice = getOfficeTime($day);
					$currentTimeIn = new DateTime($row1['timeIn']);
					$officeTimeIn  = new DateTime($getTimeOffice);
					if ($currentTimeIn  > $officeTimeIn) {
						 $late = getLate($getTimeOffice, $row1['timeIn']);
					}  else {
						 $late = 0;
					}
					$sqlUpdate = "UPDATE $table1 SET attendanceLate='$late' WHERE attendanceID='".$row1["attendanceID"]."'";
					$conn->query($sqlUpdate);

				}
		}

	$list = array();
	$salaryDeductions = array();
	$salaryAddition = array();

	$column = "*, COUNT($table1.userID) AS noPresent, SUM(attendanceLate) as late, SUM(Case When overtimeStatus = 1 THEN overtime ELSE 0 End) as totalOvertime, ";
	$column .= "SUM(if($table2.holidayType = '1', 1, 0)) as holidayRegular, SUM(if($table2.holidayType = '2', 1, 0)) as holidaySpecial";

	$sql = "SELECT $column FROM $table1 LEFT JOIN $table2 ON $table2.holidayID = $table1.holidayID LEFT JOIN $table3 ON $table3.userID = $table1.userID";
  $sql .= " LEFT JOIN $table4 ON $table4.userID = $table1.userID AND salaryStatus = 1";
  $sql .= " LEFT JOIN $table5 ON $table5.positionSalaryID = $table4.positionSalaryID";
  $sql .= " WHERE attendanceDate between '".$dateFrom."' and '".$dateTo."'";
  $sql .= " GROUP BY $table1.userID";
  if ($resultSql=mysqli_query($conn,$sql)){
		while ($row=mysqli_fetch_array($resultSql)) {
        if($row['employeeMonthlySalary'] != 0){
          $monthlySalary = $row['employeeMonthlySalary'];
        }else{
          $monthlySalary = $row['positionMonthlySalary'];
        }
				$annualSalary = $monthlySalary * 12;
				//TAX COMPUTATION
				$bracket2 = (($annualSalary-250000)*.2)/12;
				$bracket3 = (($annualSalary-400000)*.25+30000)/12;
				$bracket4 = (($annualSalary-800000)*.3+130000)/12;
				$bracket5 = (($annualSalary-2000000)*.32+490000)/12;
				$bracket6 = (($annualSalary-8000000)*.35+2410000)/12;
				$tax = 0;
				if($annualSalary<=250000){
					$tax=0;
				}else if(($annualSalary>250000)AND($annualSalary<400000)){
					$tax=$bracket2;
				}else if(($annualSalary>400000)AND($annualSalary<800000)){
					$tax=$bracket3;
				}else if(($annualSalary>800000)AND($annualSalary<2000000)){
					$tax=$bracket4;
				}else if(($annualSalary>2000000)AND($annualSalary<8000000)){
					$tax=$bracket5;
				}else if($annualSalary>8000000){
					$tax=$bracket6;
				}else{
					$tax=0;
				}

				//COMPUTATION - DEDUCTION LATE
				$dailyRate = $monthlySalary / 26;
				$hrRate = $dailyRate / 8;
				$lateTotalDeduction = $hrRate * $row['late'];

				//COMPUTATION - ADDITIONAL
				$overTimeRate = $hrRate * 1.3;
				$overTimeTotalRate = $overTimeRate * $row['totalOvertime'];
				$numberAbsence = $dateNumberDiff - $row['noPresent'];

				//CHECK IF BI WEEKLY or SEMI MONTHLY PAYROLL
				if($dateNumberDiff > 7){
					$payslipType = "Semi-Monthly";
					$basicSalary = $monthlySalary / 13;
					$numberAbsenceDeduction = $numberAbsence * $dailyRate;
				}else{
					$payslipType = "Bi-Weekly";
					$basicSalary = $monthlySalary / 4;
					$numberAbsenceDeduction = 0;
				}
				$regularHoliday = $dailyRate * 1;
				$specialHoliday = $dailyRate * .3;
				$holidayTotalPay = $regularHoliday + $specialHoliday;

				$weekNumber = getWeek($dateFrom);
				$totalSalary = ($basicSalary + $overTimeTotalRate) - ($lateTotalDeduction + $numberAbsenceDeduction + $tax);
				$totalDeduction = 0;
				$netSalary = $totalSalary - $totalDeduction;

				$list[]= array(
						"userID" => $row['userID'],
            "empID" => $row['empID'],
						"firstName" => $row['firstName'],
						"lastName" => $row['lastName'],
						"middleName" => $row['middleName'],
						"extName" => $row['extName'],
						"salaryType" => $payslipType,
						"holidayRegular" => $row['holidayRegular'],
						"holidaySpecial" => $row['holidaySpecial'],
						"employeeMonthlySalary" => $monthlySalary,
            "employeeBasicSalary" => $basicSalary,
						"noOfDays" => $row['noPresent'],
						"noOfAbsences" => $numberAbsence,
            "totalLate" => $row['late'],
            "totalOvertime" => $row['totalOvertime'],
						"dailyRate" => $dailyRate,
						"hrRate" => $hrRate,
						"lateTotalDeduction" => $lateTotalDeduction,
						"absenceTotalDeduction" => $numberAbsenceDeduction,
						"overTimeTotalRate" => $overTimeTotalRate,
						"holidayTotalPay" => $holidayTotalPay,
						"tax" => $tax,
						"totalSalary" => $totalSalary,
						"totalDeduction" => $totalDeduction,
						"netSalary" => $netSalary
					);

		}
		$status = "SUCCESS";
	}else{
		//IF NO DATA FOUND
	 	$status = "ERROR: " . $sql . "<br>" . $conn->error;
	}
}else{//IF PROPER PARAMETER NOT PASSED return 404
	$status= "ERROR";
}
/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"payslipList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
