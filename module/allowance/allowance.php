<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{
	if($_POST['page'] == "listAllowance"){
		$table1 = "tbl_employee_allowance";
		$table2 = "tbl_allowance_type";
		$name = array("*");

		$sql = "SELECT * FROM $table1 LEFT JOIN $table2 ON $table2.allowanceTypeID = $table1.allowanceTypeID";
		$sql .= " WHERE";
		$sql .= (!EMPTY($_POST['employeeID']) ? " $table1.employeeID ='".$_POST['employeeID']."'" : "AND");

		$sqlWhere1 = stringEndsWith($sql, "AND");
		$sqlWhere2 = stringEndsWith($sql, "WHEREAND");
		if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
			$sql = removeLastString($sql);
		}

		if ($resultSql=mysqli_query($conn,$sql)){
			while ($row=mysqli_fetch_array($resultSql)) {
					$list[]= array(
							"employeeAllowanceID" => $row['employeeAllowanceID'],
							"allowanceTypeID" => $row['allowanceTypeID'],
							"employeeAllowanceAmount" => $row['employeeAllowanceAmount'],
							"employeeAllowanceStatus" => $row['employeeAllowanceStatus'],
							"createdBy" => $row['createdBy'],
							"createdTime" => $row['createdTime'],
							"updatedBy" => $row['updatedBy'],
							"updatedTime" => $row['updatedTime']
						);
			}
			$status = "SUCCESS";
		}else{
			$status = "ERROR";
			$message = "ERROR: " . $sql . "<br>" . $conn->error;
		}
	}
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"allowanceList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
