<?php
include_once '../../common/common.php';
if($token != "success"){
  $returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
  }else{
    if($_POST['page'] == "addAllowance"){
      $employeeID = (!EMPTY($_POST['employeeID']) ? $_POST['employeeID'] : "");
      $allowanceTypeID = (!EMPTY($_POST['allowanceTypeID']) ? $_POST['allowanceTypeID'] : "");
      $employeeAllowanceAmount = (!EMPTY($_POST['employeeAllowanceAmount']) ? $_POST['employeeAllowanceAmount'] : "");
      $employeeAllowanceStatus = (!EMPTY($_POST['employeeAllowanceStatus']) ? $_POST['employeeAllowanceStatus'] : "");
      $table = "tbl_employee_allowance";
      $field = array("employeeID","allowanceTypeID","employeeAllowanceAmount","employeeAllowanceStatus","createdBy","createdTime");
      $data = array($employeeID,$allowanceTypeID,$employeeAllowanceAmount,$employeeAllowanceStatus,$currentUser,$currentTimeDate);
      $msg = "Allowance Already Added!";
      $result = insertAllData($table,$field,$data,$msg);
      $returndata = $result;
    }
}

/********Compose Your Json Data Here*************/
createJsonData('allowanceListInfo', $returndata);
mysqli_close($conn);
