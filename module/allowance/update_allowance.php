<?php
include_once '../../common/common.php';
if($token != "success"){
  $returndata['status'] = "ERROR";
  $returndata['message']  = "Access Denied!";
}else{
  if($_POST['page'] == "updateAllowance"){
    $employeeID = $_POST['employeeID'];
    $allowanceTypeID = !EMPTY($_POST['allowanceTypeID']) ? $_POST['allowanceTypeID'] : "";
    $employeeAllowanceAmount = !EMPTY($_POST['employeeAllowanceAmount']) ? $_POST['employeeAllowanceAmount'] : "";
    $employeeAllowanceStatus = !EMPTY($_POST['employeeAllowanceStatus']) ? $_POST['employeeAllowanceStatus'] : "";
    $table1 = "tbl_employee_allowance";
    $id1 = !EMPTY($_POST['employeeAllowanceID']) ? $_POST['employeeAllowanceID'] : "";
    $idName1 = "employeeAllowanceID";
    $field1 = array("employeeID","allowanceTypeID","employeeAllowanceAmount","employeeAllowanceStatus","updatedBy","updatedTime");
    $data1 = array($employeeID,$allowanceTypeID,$employeeAllowanceAmount,$employeeAllowanceStatus,$currentUser,$currentTimeDate);
    $msg = "Allowance successfully updated!";
    $result1 = updateData($table1,$field1,$data1,$idName1,$id1,$msg);
    $returndata = $result1;
  }
}

/********Compose Your Json Data Here*************/
createJsonData('allowanceUpdate', $returndata);
mysqli_close($conn);
