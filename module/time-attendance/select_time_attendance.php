<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{

		if($_POST['page'] == "listTimeAttendance"){
		$table1 = "tbl_attendance";
		$column = array("*, IF(WEEKDAY(attendanceDate), IF(TIMEDIFF(timeIn,'08:00:00.000000') > '00:00:00.000000', TIMEDIFF(timeIn,'8:00:00.000000'),'00:00:00.000000'), if(TIMEDIFF(timeIn,'07:00:00.000000') > '00:00:00.000000', TIMEDIFF(timeIn,'07:00:00.000000'),'00:00:00.000000')) as late, TIMEDIFF(IF(WEEKDAY(attendanceDate), TIMEDIFF(timeOut, '08:00:00.000000'), TIMEDIFF(timeOut, '07:00:00.000000')), '01:00:00.000000') as 'totalWorkHrs'");
		$where = "";
		$where .= "WHERE";
		$where .= (!EMPTY($_POST['employeeID']) ? " $table1.employeeID ='".$_POST['employeeID']."'" : "AND");

		$sqlWhere1 = stringEndsWith($where, "AND");
		$sqlWhere2 = stringEndsWith($where, "WHEREAND");
		if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
			$where = removeLastString($where);
		}
		$result = selectData($table1,$column,$where);
		while ($row=mysqli_fetch_array($result['data'])) {
				$list[]= array(
						"attendanceDate" => $row['attendanceDate'],
						"timeIn" => $row['timeIn'],
						"timeOut" => $row['timeOut'],
						"totalWorkHrs" => $row['totalWorkHrs'],
						"late" => $row['late'],
						"overtime" => $row['overtime'],
						"overtimeStatus " => $row['overtimeStatus'],
						"createdBy" => $row['createdBy'],
						"createdTime" => $row['createdTime'],
						"updatedBy" => $row['updatedBy'],
						"updatedTime" => $row['updatedTime']
					);
		}
		$status = $result['status'];
	}
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"timeAttendanceList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
