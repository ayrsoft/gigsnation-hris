<?php
include_once '../../common/common.php';
echo $currentUserID;
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{
	if($_POST['page'] == "listTimeAttendance"){
		$table1 = "tbl_attendance";
		$table2 = "tbl_employee";
		$table3 = "tbl_employee_leave";
		$dateFrom = date('Y-m-01');
		$dateTo = date('Y-m-t');

		if( isset($_POST['month']) && isset($_POST['year']) ){
			$dateFrom = date($_POST['year'].'-'.$_POST['month'].'-01');
			$dateTo = date($_POST['year'].'-'.$_POST['month'].'-t');
			// $absent = countDays($_POST['year'], $_POST['month'], array(0, 6));
		}
		$column = "$table1.*, $table2.firstName, $table2.lastName, $table2.middleName, $table2.extName, count('attendanceID') as 'daysOFWork', ";
		$column .= "SEC_TO_TIME(SUM(TIME_TO_SEC(IF(WEEKDAY(attendanceDate), IF(TIMEDIFF(timeIn,'08:00:00.000000') > '00:00:00.000000', TIMEDIFF(timeIn,'08:00:00.000000'),'00:00:00.000000'), IF(TIMEDIFF(timeIn,'07:00:00.000000') > '00:00:00.000000', TIMEDIFF(timeIn,'07:00:00.000000'),'00:00:00.000000'))))) as late";

		$sql = "SELECT $column FROM $table1 LEFT JOIN $table2 ON $table2.employeeID = $table1.employeeID";
		$sql .= " WHERE";
		$sql .= " attendanceDate between '".$dateFrom."' and '".$dateTo."' AND";
		$sql .= (!EMPTY($_POST['firstName']) ? " $table2.firstName ='".$_POST['firstName']."' AND" : "");
		$sql .= (!EMPTY($_POST['lastName']) ? " $table2.lastName ='".$_POST['lastName']."' AND" : "");
		$sql .= (!EMPTY($_POST['employeeID']) ? " $table1.employeeID ='".$_POST['employeeID']."'" : "AND");
		$sqlWhere1 = stringEndsWith($sql, "AND");
    $sqlWhere2 = stringEndsWith($sql, "WHEREAND");
    if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
      $sql = removeLastString($sql);
    }
		$sql .= " GROUP BY employeeID";

		if ($resultSql=mysqli_query($conn,$sql)){
			while ($row=mysqli_fetch_array($resultSql)) {
					$list[]= array(
							"employeeID" => $row['employeeID'],
							"firstName" => $row['firstName'],
							"lastName" => $row['lastName'],
							"middleName" => $row['middleName'],
							"extName" => $row['extName'],
							"daysOFWork" => $row['daysOFWork'],
							"totalAbsent" => WORKDAYS - $row['daysOFWork'],
							"totalLate" => $row['late']
						);
			}
			$status = "SUCCESS";
		}else{
			//IF NO DATA FOUND
		 	$status = "ERROR: " . $sql . "<br>" . $conn->error;
		}
	}else{//IF PROPER PARAMETER NOT PASSED return 404
		$status= "ERROR";
	}
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"timeAttendanceList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
