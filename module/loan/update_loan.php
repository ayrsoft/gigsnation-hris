<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status'] = "ERROR";
	$returndata['message']  = "Access Denied!";
}else{
  if($_POST['page'] == "updateLoan"){
    $employeeID = $_POST['employeeID'];
    $loanTypeID = !EMPTY($_POST['loanTypeID']) ? $_POST['loanTypeID'] : "";
    $employeePrincipalAmount = !EMPTY($_POST['employeePrincipalAmount']) ? $_POST['employeePrincipalAmount'] : "";
    $employeeLoanAmount = !EMPTY($_POST['employeeLoanAmount']) ? $_POST['employeeLoanAmount'] : "";
    $employeeLoanRemainingBalance = !EMPTY($_POST['employeeLoanRemainingBalance']) ? $_POST['employeeLoanRemainingBalance'] : "";
    $employeeLoanTerms = !EMPTY($_POST['employeeLoanTerms']) ? $_POST['employeeLoanTerms'] : "";
    $employeeLoanScheme = !EMPTY($_POST['employeeLoanScheme']) ? $_POST['employeeLoanScheme'] : "";
    $employeeLoanStatus = !EMPTY($_POST['employeeLoanStatus']) ? $_POST['employeeLoanStatus'] : "";
    $table1 = "tbl_employee_loan";
    $id1 = !EMPTY($_POST['employeeLoanID']) ? $_POST['employeeLoanID'] : "";
    $idName1 = "employeeLoanID";
    $field1 = array("employeeID","loanTypeID","employeePrincipalAmount","employeeLoanAmount","employeeLoanRemainingBalance","employeeLoanTerms","employeeLoanScheme","employeeLoanStatus","updatedBy","updatedTime");
    $data1 = array($employeeID,$loanTypeID,$employeePrincipalAmount,$employeeLoanAmount,$employeeLoanRemainingBalance,$employeeLoanTerms,$employeeLoanScheme,$employeeLoanStatus,$currentUser,$currentTimeDate);
    $msg = "Loan successfully updated!";
    $result1 = updateData($table1,$field1,$data1,$idName1,$id1,$msg);
    $returndata = $result1;
  }
}

/********Compose Your Json Data Here*************/
createJsonData('loanUpdate', $returndata);
mysqli_close($conn);
