<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{
	if($_POST['page'] == "listLoan"){
		$table1 = "tbl_employee_loan";
		$table2 = "tbl_loan_type";
		$name = array("*");
		$employeeID = !EMPTY($_POST['employeeID']) ? $_POST['employeeID'] : "";

		$sql = "SELECT * FROM $table1 LEFT JOIN $table2 ON $table2.loanTypeID = $table1.loanTypeID";
		$sql .= " WHERE";
		$sql .= (!EMPTY($employeeID) ? " $table1.employeeID='".$employeeID."' AND" : "");

		$sqlWhere1 = stringEndsWith($sql, "AND");
    $sqlWhere2 = stringEndsWith($sql, "WHEREAND");
    if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
      $sql = removeLastString($sql);
    }

		if ($resultSql=mysqli_query($conn,$sql)){
			while ($row=mysqli_fetch_array($resultSql)) {
					$list[]= array(
							"employeeLoanID" => $row['employeeLoanID'],
							"loanTypeID" => $row['loanTypeID'],
							"loanTypeName" => $row['loanTypeName'],
							"employeePrincipalAmount" => $row['employeePrincipalAmount'],
							"employeeLoanAmount" => $row['employeeLoanAmount'],
							"employeeLoanRemainingBalance" => $row['employeeLoanRemainingBalance'],
							"employeeLoanTerms" => $row['employeeLoanTerms'],
							"employeeLoanScheme" => $row['employeeLoanScheme'],
							"employeeLoanStatus" => $row['employeeLoanStatus'],
							"createdBy" => $row['createdBy'],
							"createdTime" => $row['createdTime'],
							"updatedBy" => $row['updatedBy'],
							"updatedTime" => $row['updatedTime']
						);
			}
			$status = "SUCCESS";
		}else{
			$status = "ERROR";
			$message = "ERROR: " . $sql . "<br>" . $conn->error;
		}
	}
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"loanList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
