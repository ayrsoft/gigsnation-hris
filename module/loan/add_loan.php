<?php
include_once '../../common/common.php';
if($token != "success"){
  $returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
  }else{
    if($_POST['page'] == "addLoan"){
      $employeeID = !EMPTY($_POST['employeeID']) ? $_POST['employeeID'] : "";
      $loanTypeID = !EMPTY($_POST['loanTypeID']) ? $_POST['loanTypeID'] : "";
      $employeePrincipalAmount = !EMPTY($_POST['employeePrincipalAmount']) ? $_POST['employeePrincipalAmount'] : "";
      $employeeLoanAmount = !EMPTY($_POST['employeeLoanAmount']) ? $_POST['employeeLoanAmount'] : "";
      $employeeLoanRemainingBalance = !EMPTY($_POST['employeeLoanRemainingBalance']) ? $_POST['employeeLoanRemainingBalance'] : "";
      $employeeLoanTerms = !EMPTY($_POST['employeeLoanTerms']) ? $_POST['employeeLoanTerms'] : "";
      $employeeLoanScheme = !EMPTY($_POST['employeeLoanScheme']) ? $_POST['employeeLoanScheme'] : "";
      $employeeLoanStatus = !EMPTY($_POST['employeeLoanStatus']) ? $_POST['employeeLoanStatus'] : "";
      $table = "tbl_employee_loan";
      $field = array("employeeID","loanTypeID","employeePrincipalAmount","employeeLoanAmount","employeeLoanRemainingBalance","employeeLoanTerms","employeeLoanScheme","employeeLoanStatus","createdBy","createdTime");
      $data = array($employeeID,$loanTypeID,$employeePrincipalAmount,$employeeLoanAmount,$employeeLoanRemainingBalance,$employeeLoanTerms,$employeeLoanScheme,$employeeLoanStatus,$currentUser,$currentTimeDate);
      $msg = "Loan Successfully Added!";
      $result = insertAllData($table,$field,$data,$msg);
      $returndata = $result;
    }
}

/********Compose Your Json Data Here*************/
createJsonData('loanListInfo', $returndata);
mysqli_close($conn);
