<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{
	if($_POST['page'] == "listPosition" || $_POST['page'] == "searchPosition"){
		$table1 = "tbl_position_type";
		$table2 = "tbl_department_type";
		$departmentTypeID = !EMPTY($_POST['departmentTypeID']);
		$positionTypeName = !EMPTY($_POST['positionTypeName']);
		$positionTypeStatus = !EMPTY($_POST['positionTypeStatus']);

		$sql = "SELECT * FROM $table1 LEFT JOIN $table2 ON $table2.departmentTypeID = $table1.departmentTypeID";

		$sql .= " WHERE";
		$sql .= (!EMPTY($departmentTypeID) ? " $table1.departmentTypeID='".$departmentTypeID."' AND" : "");
		$sql .= (!EMPTY($positionTypeName) ? " $table1.positionTypeName ='".$positionTypeName."' AND" : "");
    $sql .= (!EMPTY($positionTypeStatus) ? " $table1.positionTypeStatus ='".$positionTypeStatus."'" : "AND");

		$sqlWhere1 = stringEndsWith($sql, "AND");
    $sqlWhere2 = stringEndsWith($sql, "WHEREAND");
    if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
      $sql = removeLastString($sql);
    }

		if ($resultSql=mysqli_query($conn,$sql)){
			while ($row=mysqli_fetch_array($resultSql)) {
					$list[]= array(
							"positionTypeID" => $row['positionTypeID'],
							"departmentTypeID" => $row['departmentTypeID'],
							"positionTypeName" => $row['positionTypeName'],
							"positionTypeSalary" => $row['positionTypeSalary'],
							"positionTypeStatus" => $row['positionTypeStatus'],
							"createdBy" => $row['createdBy'],
							"createdTime" => $row['createdTime'],
							"updatedBy" => $row['updatedBy'],
							"updatedTime" => $row['updatedTime']
						);
			}
			$status = "SUCCESS";
		}else{
			//IF NO DATA FOUND
			$status = "ERROR";
		 	$message = "ERROR: " . $sql . "<br>" . $conn->error;
		}
	}else{//IF PROPER PARAMETER NOT PASSED return 404
		$status= "ERROR";
	}
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message"=> $message,
	"positionList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
