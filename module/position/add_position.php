<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status']= "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
	if($_POST['page'] == "addPosition"){

		$departmentTypeID = !EMPTY($_POST['departmentTypeID']) ? $_POST['departmentTypeID'] : "";
    $positionTypeName = !EMPTY($_POST['positionTypeName']) ? $_POST['positionTypeName'] : "";
		$positionTypeSalary = !EMPTY($_POST['positionTypeSalary']) ? $_POST['positionTypeSalary'] : "";
  	$positionTypeStatus = !EMPTY($_POST['positionTypeStatus']) ? $_POST['positionTypeStatus'] : "";

  	$sql1="SELECT * FROM tbl_position_type WHERE positionTypeName='".$positionTypeName."'";

  	$result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Position Name Already Exist";
  	}else{
  		$sql2 = "INSERT INTO tbl_position_type (departmentTypeID, positionTypeName, positionTypeSalary, positionTypeStatus, createdBy, createdTime)";
  		$sql2 .= " VALUES('".$departmentTypeID."', '".$positionTypeName."', '".$positionTypeSalary."', '".$positionTypeStatus."', '".$currentUser."', '".$currentTimeDate."')";
  		if ($conn->query($sql2) === TRUE) {
				$returndata['status'] = "SUCCESS";
				$returndata['message'] = "Position name successfully added!";
  		}else{
  			 $returndata['status'] = "ERROR: " . $sql2 . "<br>" . $conn->error;
  		}
  	}

	}else{//IF PROPER PARAMETER NOT PASSED return 404
		$returndata['status'] = "ERROR";
	}
}


/********Compose Your Json Data Here*************/
createJsonData('positionInfo', $returndata);
mysqli_close($conn);
