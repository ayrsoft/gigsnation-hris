<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status']= "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "updatePosition"){

    $departmentTypeID = !EMPTY($_POST['departmentTypeID']) ? $_POST['departmentTypeID'] : "";
    $positionTypeName = !EMPTY($_POST['positionTypeName']) ? $_POST['positionTypeName'] : "";
		$positionTypeSalary = !EMPTY($_POST['positionTypeSalary']) ? $_POST['positionTypeSalary'] : "";
  	$positionTypeStatus = !EMPTY($_POST['positionTypeStatus']) ? $_POST['positionTypeStatus'] : "";

    $sql1="SELECT * FROM tbl_position_type WHERE positionTypeName='".$positionTypeName."'";
    $result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Position Name Already Exist";
  	}else{
      $table1 = "tbl_position_type";
      $id1 = !EMPTY($_POST['positionTypeID']) ? $_POST['positionTypeID'] : "";
      $idName1 = "positionTypeID";
      $field1 = array("departmentTypeID", "positionTypeName", "positionTypeSalary", "positionTypeStatus", "updatedBy","updatedTime");
      $data1 = array($departmentTypeID,$positionTypeName,$positionTypeSalary,$positionTypeStatus,$currentUser,$currentTimeDate);
      $msg = "Position already updated!";
      $result1 = updateData($table1,$field1,$data1,$idName1,$id1,$msg);
      $returndata = $result1;
    }
  }
}

/********Compose Your Json Data Here*************/
createJsonData('holidayInfoUpdate', $returndata);
mysqli_close($conn);
