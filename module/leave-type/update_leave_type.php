<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "updateLeaveType"){

		$leaveTypeName = !EMPTY($_POST['leaveTypeName']) ? $_POST['leaveTypeName'] : "";
		$leaveTypeStatus = !EMPTY($_POST['leaveTypeStatus']) ? $_POST['leaveTypeStatus'] : "";

    $sql1="SELECT * FROM tbl_leave_type WHERE leaveTypeName='".$leaveTypeName."' AND leaveTypeStatus='".$leaveTypeStatus."' ";
    $result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Leave Type Already Exist";
  	}else{
      $table1 = "tbl_leave_type";
      $id1 = isset($_POST['leaveTypeID']);
      $idName1 = "leaveTypeID";
      $field1 = array("leaveTypeName", "leaveTypeStatus","updatedBy","updatedTime");
      $data1 = array($leaveTypeName,$leaveTypeStatus,$currentUser,$currentTimeDate);
      $msg = "Leave Type successfully updated!";
      $result1 = updateData($table1,$field1,$data1,$idName1,$id1,$msg);
      $returndata = $result1;
    }
  }
}

/********Compose Your Json Data Here*************/
createJsonData('leaveTypeInfoUpdate', $returndata);
mysqli_close($conn);
