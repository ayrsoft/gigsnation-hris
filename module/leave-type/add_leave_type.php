<?php
include_once '../../common/common.php';
if($token != "success"){
	$returndata['status'] = "ERROR";
  $returndata['message'] = "Access Denied!";
}else{
  if($_POST['page'] == "addLeaveType"){

    $leaveTypeName= !EMPTY($_POST['leaveTypeName']) ? $_POST['leaveTypeName'] : "";
    $leaveTypeStatus = !EMPTY($_POST['leaveTypeStatus']) ? $_POST['leaveTypeStatus'] : "";

  	$sql1="SELECT * FROM tbl_leave_type WHERE leaveTypeName='".$leaveTypeName."'";

  	$result=mysqli_query($conn,$sql1);
  	$rowcount=mysqli_num_rows($result);
    if ($rowcount > 0){
  		$returndata['status'] = "Leave Type Already Exist";
  	}else{
  		$sql2 = "INSERT INTO tbl_leave_type (leaveTypeName, leaveTypeStatus, createdBy, createdTime)";
  		$sql2 .= " VALUES('".$leaveTypeName."', '".$leaveTypeStatus."', '".$_SESSION['userId']."', '".$currentTimeDate."')";
  		if ($conn->query($sql2) === TRUE) {
  			$returndata['status'] = "SUCCESS";
        $returndata['message'] = "Leave Type successfully added!";
  		}else{
  			 $returndata['status'] = "ERROR: " . $sql2 . "<br>" . $conn->error;
  		}
  	}

  }else{//IF PROPER PARAMETER NOT PASSED return 404
  	$returndata['status'] = "ERROR";
  }
}

/********Compose Your Json Data Here*************/
createJsonData('leaveTypeInfo', $returndata);
mysqli_close($conn);
