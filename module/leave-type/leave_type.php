<?php
include_once '../../common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{

		$table = "tbl_leave_type";
		$name = array("*");
		if($_POST['page'] == "listLeaveType"){
			$result = showAllData($table,$name);
		}else if($_POST['page'] == "selectLeaveType"){
			$leaveTypeName = !EMPTY($_POST['leaveTypeName']) ? $_POST['leaveTypeName'] : "";
			$leaveTypeStatus = !EMPTY($_POST['leaveTypeStatus']) ? $_POST['leaveTypeStatus'] : "";
			$where = "";
			$where = "WHERE";
			$where .= (!EMPTY($leaveTypeName) ? " $table.leaveTypeName ='".$leaveTypeName."' AND" : "");
			$where .= (!EMPTY($leaveTypeStatus) ? " $table.leaveTypeStatus ='".$leaveTypeStatus."'" : "AND");

			$sqlWhere1 = stringEndsWith($where, "AND");
	    $sqlWhere2 = stringEndsWith($where, "WHEREAND");
	    if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
	      $where = removeLastString($where);
	    }

			$result = selectData($table,$name,$where);
		}
		while ($row=mysqli_fetch_array($result['data'])) {
				$list[]= array(
						"leaveTypeID" => $row['leaveTypeID'],
						"leaveTypeName" => $row['leaveTypeName'],
						"leaveTypeStatus" => $row['leaveTypeStatus'],
						"createdBy" => $row['createdBy'],
						"createdTime" => $row['createdTime'],
						"updatedBy" => $row['updatedBy'],
						"updatedTime" => $row['updatedTime']
					);
		}
		$status = $result['status'];
}

/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"leaveTypeList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
