<?php
include_once 'common/common.php';
if($token != "success"){
	$status= "ERROR";
  $message = "Access Denied!";
}else{
	if($_POST['page'] == "listAuditLogs" || $_POST['page'] == "selectAuditLogs"){
	$table1 = "tbl_audit_logs";
	$table2 = "tbl_users";
	$userName = $_POST['userName'];
	$auditLogsDate = $_POST['auditLogsDate'];
	$auditLogsType = $_POST['auditLogsType'];

		$column = "$table1.*, ";
		$column .= "$table2.userName";
		$list = array();
		$sql = "SELECT $column FROM $table1 LEFT JOIN $table2 ON $table2.userID = $table1.userID ";
		$sql .= "WHERE";
		$sql .= (!EMPTY($userName) ? " $table2.userName='".$userName."' AND" : "");
		$sql .= (!EMPTY($auditLogsDate) ? " $table1.auditLogsDate ='".$auditLogsDate."' AND" : "");
    $sql .= (!EMPTY($auditLogsType) ? " $table1.auditLogsType ='".$auditLogsType."'" : "AND");

		$sqlWhere1 = stringEndsWith($sql, "AND");
    $sqlWhere2 = stringEndsWith($sql, "WHEREAND");
    if($sqlWhere1 == TRUE OR $sqlWhere2 ==  TRUE){
      $sql = removeLastString($sql);
    }

		if ($resultSql=mysqli_query($conn,$sql)){
			while ($row=mysqli_fetch_array($resultSql)) {
					$list[]= array(
							"auditLogsID" => $row['auditLogsID'],
							"auditLogsDate" => $row['auditLogsDate'],
							"userName" => $row['userName'],
							"auditLogsType" => $row['auditLogsType'],
							"auditLogsDescription" => $row['auditLogsDescription']
						);
			}
			$status = "SUCCESS";
		}else{
			//IF NO DATA FOUND
		 	$status = "ERROR: " . $sql . "<br>" . $conn->error;
		}
	}else{//IF PROPER PARAMETER NOT PASSED return 404
		$status= "ERROR";
	}
}




/********Compose Your Json Data Here*************/
$arr = array(
	"status" => $status,
	"message" => $message,
	"auditLogsList" => $list
);
echo  json_encode($arr);
mysqli_close($conn);
